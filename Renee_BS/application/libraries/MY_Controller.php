<?php 
class MY_Controller
{
     var $CI;

	 public function __construct(){
          $this->CI =& get_instance();
	 //$this->lg =& get_instance();
       //$this->CheckSessions();
    }

    public function CheckSessions()
    {

        if(!isset($_SESSION['username'])) 
        { 
        return false;
        }
		else 
		{ 
		return true; 
		}
		
    }

    public function do_upload()
        {
           
               $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|PNG|png';
                $config['max_size']             = 10000;
                
               
                $this->CI->load->library('upload', $config);
          if ( ! $this->CI->upload->do_upload('userfile'))
                {
                       $error = array('error' => $this->CI->upload->display_errors());
                        return $error;
                       // $this->load->view('upload_form', $error);
                }
                else
                {
                        $data = array('upload_data' => $this->CI->upload->data());
                        return $this->CI->upload->data('file_name');
                        //$this->load->view('upload_success', $data);
                }
            
    

        }
}



?>