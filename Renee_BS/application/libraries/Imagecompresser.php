<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of image_compreser
 *
 * @author Harbhajan Singh
 */
   class Imagecompresser{
       
       var $CI;

	 public function __construct(){
          $this->CI =& get_instance();
	
          $this->CI->load->library(array('image_lib'));
    }
    public function compress_image($source_url, $destination_url, $quality,$thumbpath='uploads/thumbnails',$width=150,$height=150) {
        
        $info = getimagesize($source_url);
        if ($info['mime'] == 'image/jpeg')
            $image = imagecreatefromjpeg($source_url);
        elseif ($info['mime'] == 'image/gif')
            $image = imagecreatefromgif($source_url);
        elseif ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source_url);
        imagejpeg($image, $destination_url, $quality);
        $config['image_library'] = 'gd2';
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $width;
        $config['height'] = $height;
        $config['source_image'] = $destination_url;
        $config['new_image'] = $thumbpath;
        $this->CI->image_lib->initialize($config);
        $this->CI->image_lib->resize();
        $this->CI->image_lib->clear();
        return str_replace('uploads/', '', $destination_url);
    }
    }