<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question_Model extends CI_Model { 
	public function add($data){
		$query = $this->db->insert('questions',$data);	
		if($query){
			return true;
		}else{
			return false;
		}
	}
	
	public function show(){
		$this->db->select('*');
		$this->db->from('questions');
                $this->db->order_by('id','desc');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ){
			$row = $query->result();
			return $row;
		}
	}
	
	public function delete($delete_id){
		$this->db->where('id', $delete_id);
		$delete_data = $this->db->delete('questions');
		if($delete_data){
			return true;
		}
		else{
			return false;
		}
	}


}

