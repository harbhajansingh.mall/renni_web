<?php

class Sitemodel extends CI_Model{

	/*---------Submit Form Data---------*/
	public function submitform($memid){
		$data = array(
			"fname"=>$this->input->post('fname'),
			"lname"=>$this->input->post('lname'),
			"gender"=>$this->input->post('gen'),
			"qualid"=>implode(',', $this->input->post('qual')),
			"profid"=>$this->input->post('profession'),
			"rem"=>$this->input->post('rem'));
		if($memid == 'n'){
			$res = $this->db->insert('infodata',$data);
		}
		else{
			$this->db->where('id', $memid);
			$res = $this->db->update('infodata',$data);
		}
		return $res;
	}

	/*-----------Delete selected Row-------------*/
	public function deleteData($table,$memid){
		$res = $this->db->delete($table, array('id' => $memid));
		/*return $res;*/
	}

	/*-------------Edit Selected Row------------*/
	public function editdata($memid){
		$res = $this->db->update('infodata', array('id' => $memid));
		return $res;
	}

	/*-------------Select Data------------*/
	public function selectData($table,$memid){
		$query = $this->db->get_where($table, array('id' => $memid));
		return $query->row_array();
	}

	
	/*------------View All Content--------------*/
	public function viewContent($table){
		$query = $this->db->get($table);
		return $query->result();
	}	

	public function selectWhereData($table,$data){
		$query = $this->db->get_where($table, $data);
		return $query->result();

	}

	public function uploadPoll(){
		$this->db->trans_begin();
		$validtill = date("Y-m-d", strtotime($this->input->post('validtill')));
		$this->db->update('pollquestion', array("status"=>"N"));
		$dataquest = array(
			"question"=>$this->input->post('question'),
			"validtill"=>$validtill,
			"postedon"=>DT,
			"postedby"=>$this->session->userdata('username'),
			"status"=>"Y"
		);
		
		$res = $this->db->insert('pollquestion',$dataquest);
		$insid = $this->db->insert_id();
		$i = 1;
		while($i <= 4){
			$optval = 'option'.$i;
			$dataopt = array(
			"quest_id"=>$insid,
			"opt_id"=>$i,
			"opt_val"=>$this->input->post($optval),
			"postedon"=>DT,
			"postedby"=>$this->session->userdata('username')
		);

			$res1 = $this->db->insert('polloption',$dataopt);

			$i++;
		}
		
		
		
		/*$this->db->query('AN SQL QUERY...');
		$this->db->query('ANOTHER QUERY...');
		$this->db->query('AND YET ANOTHER QUERY...');*/

		if ($this->db->trans_status() === FALSE)
		{
		        $this->db->trans_rollback();
		}
		else
		{
		        $this->db->trans_commit();
		}

		return $res;
	}

	public function deletePollData($memid){

		$res = $this->db->delete('pollquestion', array('id' => $memid));

	}

	 
}
