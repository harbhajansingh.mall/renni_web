<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs_Model extends CI_Model {

    public function insertrec($data, $insid) {
        $res = $this->db->insert('faqs', $data);
        return $res;
    }

    function getmaxid() {
        $sql = $this->db->query("SELECT MAX(faqid) as id FROM faqs");
        return $sql->row_array();
    }

    public function showallrecords() {
        //$res=$this->db->query('SELECT f.*, GROUP_CONCAT(i.ins_name) ins_name FROM `faqs` f left join faq_insurance_mapping fi on f.faqid=fi.faqid left join insurances i on i.Ins_ID=fi.Ins_ID group by f.faqid');
        $res = $this->db->query('SELECT f.*, GROUP_CONCAT(`i`.`ins_name`) as ins_name FROM faqs f left join insurances i on FIND_IN_SET(`i`.`ins_id`, `f`.`Ins_Ids`) != 0 GROUP BY `f`.`faqid` order by f.faqid desc');
        return $res->result();
    }
    public function showallrecords_front() {
        //$res=$this->db->query('SELECT f.*, GROUP_CONCAT(i.ins_name) ins_name FROM `faqs` f left join faq_insurance_mapping fi on f.faqid=fi.faqid left join insurances i on i.Ins_ID=fi.Ins_ID group by f.faqid');
        $res = $this->db->query('SELECT f.*, GROUP_CONCAT(`i`.`ins_name`) as ins_name FROM faqs f left join insurances i on FIND_IN_SET(`i`.`ins_id`, `f`.`Ins_Ids`) != 0 where f.fstatus=1 and f.insurance=0 GROUP BY `f`.`faqid`');
        return $res->result();
    }
    public function showallrecords_front_home() {
        //$res=$this->db->query('SELECT f.*, GROUP_CONCAT(i.ins_name) ins_name FROM `faqs` f left join faq_insurance_mapping fi on f.faqid=fi.faqid left join insurances i on i.Ins_ID=fi.Ins_ID group by f.faqid');
        $res = $this->db->query('SELECT f.* FROM faqs f where f.fstatus=1 and insurance=0 GROUP BY `f`.`faqid` order by `f`.`faqid` desc limit 0,3');
        return $res->result();
    }
    

    public function getinsuranceparent() {
        $res = $this->db->query('SELECT i.* FROM faqs f inner join insurances i on `i`.`ins_id`=f.insurance GROUP BY `f`.`insurance`');
        return $res->result();
    }

    public function showdata_byinsuranceid($id) {
        $res = $this->db->query('SELECT f.* FROM faqs f WHERE f.insurance=' . $id);
        //$this->db->where('faqid', $id);
        //$res=$this->db->get('faqs');
        return $res->result();
    }

    public function deleterec($id) {

        $this->db->where('faqid', $id);
        $res = $this->db->delete('faqs');
        return $res;
    }

    public function showdata($id) {
        $res = $this->db->query('SELECT f.* FROM faqs f, insurances i WHERE f.faqid=' . $id);
        //$this->db->where('faqid', $id);
        //$res=$this->db->get('faqs');
        return $res->result();
    }

    public function updatedata($id, $data, $insid) {

        $this->db->trans_start();
        $this->db->where('faqid', $id);
        $res = $this->db->update('faqs', $data);

        if ($insid) {
            //---Delete Record/s(if available) against Faqid in faq insurance mapping table
            $this->db->where('faqid', $id);
            $this->db->delete('faq_insurance_mapping');
            for ($i = 0; $i < count($insid); $i++) {
                $data1 = array(
                    'faqid' => $id,
                    'Ins_ID' => $insid[$i]
                );
                //---Add updated record into faq insurance mapping table
                $res = $this->db->insert('faq_insurance_mapping', $data1);
            }
        }

        $this->db->trans_complete();
        return $res;
    }

    public function subinc_cat($data) {

        $this->db->select('ins_id,ins_name');
        $this->db->where('parent_id', $data);
        $query = $this->db->get('insurances');
        return $query->result();
    }

}
