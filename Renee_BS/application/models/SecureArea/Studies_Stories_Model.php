
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Studies_Stories_Model extends CI_Model {

    public function studies_stories_add($data) {
        $query = $this->db->insert('casestudies_stories', $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function studies_stories_update($data, $id) {

        $this->db->where('rlid', $id);
        $query = $this->db->update('casestudies_stories', $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function studies_stories_show() {
        $res = $this->db->query('SELECT c.*, GROUP_CONCAT(`i`.`ins_name`) as ins_name FROM casestudies_stories c left join insurances i on FIND_IN_SET(`i`.`ins_id`, `c`.`ins_ids`) != 0 GROUP BY `c`.`rlid` order by rlid desc');
        return $res->result();

        /* $this->db->select('*');
          $this->db->from('casestudies_stories');
          $query = $this->db->get();
          if ( $query->num_rows() > 0 ){
          $row = $query->result();
          return $row;
          } */
    }

    public function studies_stories_show_bychildid($childid){
        $res = $this->db->query('SELECT c.* FROM casestudies_stories c where FIND_IN_SET('.$childid.', `c`.`ins_ids`) != 0 GROUP BY `c`.`rlid` order by rlid desc');
        return $res->result();
    }
    public function studies_stories_readmore($id){
        $res = $this->db->query('SELECT c.* FROM casestudies_stories c where c.rlid='.$id);
        return $res->result();
    }

    public function studies_stories_delete($delete_id) {
        $this->db->where('rlid', $delete_id);
        $delete_data = $this->db->delete('casestudies_stories');
        if ($delete_data) {
            return true;
        } else {
            return false;
        }
    }

}
