<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lifestorymodel extends CI_Model 
{
 
		public function getdata($data,$insid)
 		 {
     
     // print_r($data);
     // echo "<br>";
     // print_r($insid);
     // return;
      $this->db->trans_start();
      $res=$this->db->insert('reallife_stories',$data); 
         

           if($insid)
           {
           	
             $a=$this->getmaxid();
             for ($i=0; $i <count($insid) ; $i++) { 
             
              $data1=array(
              'rlid'=>$a['id'],
              'Ins_ID'=>$insid[$i]);
          
           $res=$this->db->insert('realstory_insurance_mapping',$data1);
             # code...
             }
         }

           $this->db->trans_complete();
          
           if ($this->db->trans_status() === FALSE)
            {
             
             return $res;
              }
          else
          {
            return $res;
          }

    	}
    	function getmaxid()
      {
         $sql=$this->db->query("SELECT MAX(rlid) as id FROM reallife_stories");
          return $sql->row_array();
      }
   
	 
	
	public function getallrecord()
	{
	
		$res=$this->db->query('SELECT r.*, GROUP_CONCAT(i.Ins_Name)Ins_Name FROM `reallife_stories` r left join realstory_insurance_mapping ri on r.rlid=ri.rlid left join insurances i on i.Ins_ID=ri.Ins_ID group by r.rlid');
 	       //$query = $this->db->get();
		 //$res=$this->db->get('reallife_stories');
		return $res->result();

	}

    
	public function deleterec($id)
	{
		$this->db->where('rlid',$id);
		$res=$this->db->delete('reallife_stories');
		return $res;
	}
	public function showdataidwise($id)
	{ 
    $res=$this->db->query('SELECT r.*, GROUP_CONCAT(i.Ins_ID) Ins_ID FROM `reallife_stories` r left join realstory_insurance_mapping ri on r.rlid=ri.rlid left join insurances i on i.Ins_ID=ri.Ins_ID where r.rlid=' .$id . ' group by r.rlid');
		// $this->db->where('rlid',$id);
		// $res=$this->db->get('reallife_stories');
		 return $res->result();
	}
	public function updatedata($id,$data,$insid)
	{ 
  
  print_r($insid);
  echo "<br>" . $id;
 // return;
    $this->db->trans_start();
		$this->db->where('rlid',$id);
		$res=$this->db->update('reallife_stories',$data);
    if($insid)
           {
            $this->db->where('rlid', $id);
                $this->db->delete('realstory_insurance_mapping');
              for($i=0; $i<count($insid); $i++)
              {
           $data1=array(
          'rlid'=>$id ,
          'Ins_ID'=>$insid[$i]

                   );
               $res=$this->db->insert('realstory_insurance_mapping',$data1);
         }
       }
          $this->db->trans_complete();
		return $res;


	 }

}