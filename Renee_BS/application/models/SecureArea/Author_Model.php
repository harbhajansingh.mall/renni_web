<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Author_Model extends CI_Model { 
	public function author_add($data){
		$query = $this->db->insert('author_master',$data);	
		if($query){
			return true;
		}else{
			return false;
		}
	}
	
	public function author_show(){
		$this->db->select('*');
		$this->db->from('author_master');
                $this->db->order_by('author_id','desc');
		$query = $this->db->get();
		if ( $query->num_rows() > 0 ){
			$row = $query->result();
			return $row;
		}
	}
	
	public function author_delete($delete_id){
		$this->db->where('author_id', $delete_id);
		$delete_data = $this->db->delete('author_master');
		if($delete_data){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function authors_update($author_name,$id){
		$data=array('author_name'=>$author_name['name']);
		$this->db->where('author_id',$id);
		$update = $this->db->update('author_master',$data);
		if($update){
			return true;
		}else{
			return false;
		}
	}



}

