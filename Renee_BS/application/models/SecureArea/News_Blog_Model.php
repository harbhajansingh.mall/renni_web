
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class News_Blog_Model extends CI_Model {

    public function news_bloag_update($data, $id) {

        $this->db->where('nb_id', $id);
        $query = $this->db->update('news_blogs', $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function news_bloag_add($data) {
        $query = $this->db->insert('news_blogs', $data);
        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function news_blog_show($type) {
        $this->db->group_by('nb_id');
        $this->db->select('nb.*, GROUP_CONCAT(`i`.`ins_name`) as ins_name');
        $this->db->where('nb.type', $type);
        $this->db->from('news_blogs as nb');
        $this->db->join('insurances i', 'find_in_set(`i`.`ins_id`, `nb`.`Ins_Ids`) != 0', 'left outer');
       // $this->db->where('find_in_set(`i`.`ins_id`, `nb`.`Ins_Ids`) !=', 0);
        $this->db->order_by('nb.nb_id','desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->result();
            return $row;
        }
    }
    public function news_blog_show_all($id=0) {
        $this->db->group_by('nb_id');
        $this->db->select('nb.*, GROUP_CONCAT(`i`.`ins_name`) as ins_name');
        if($id!=0){
        $this->db->where('find_in_set('.$id.', `nb`.`Ins_Ids`) != 0');
        }
        $this->db->from('news_blogs as nb');
        $this->db->join('insurances i', 'find_in_set(`i`.`ins_id`, `nb`.`Ins_Ids`) != 0', 'left outer');
       // $this->db->where('find_in_set(`i`.`ins_id`, `nb`.`Ins_Ids`) !=', 0);
        $this->db->order_by('nb.nb_id','desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->result();
            return $row;
        }
    }
    
    public function blog_show_all($id=0) {
        $this->db->group_by('nb_id');
        $this->db->select('nb.*, GROUP_CONCAT(`i`.`ins_name`) as ins_name');
        if($id!=0){
        $this->db->where('find_in_set('.$id.', `nb`.`Ins_Ids`) != 0');
        }
        $this->db->where('nb.type','blog');
        $this->db->from('news_blogs as nb');
        $this->db->join('insurances i', 'find_in_set(`i`.`ins_id`, `nb`.`Ins_Ids`) != 0', 'left outer');
       // $this->db->where('find_in_set(`i`.`ins_id`, `nb`.`Ins_Ids`) !=', 0);
        $this->db->order_by('nb.nb_id','desc');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->result();
            return $row;
        }
    }
    
    

    public function news_blog_delete($delete_id) {
        $this->db->where('nb_id', $delete_id);
        $delete_data = $this->db->delete('news_blogs');
        if ($delete_data) {
            return true;
        } else {
            return false;
        }
    }

    public function insurance_all() {
        $this->db->select('*');
        $this->db->from('insurances');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->result();
            return $row;
        }
    }

}
