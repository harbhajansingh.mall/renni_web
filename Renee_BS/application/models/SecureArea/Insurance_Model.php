<?php

class Insurance_Model extends CI_model {

    public function __construct() {
        parent::__construct();
    }

    public function showAllRecords() {
        // 0 means only records which are having parent_id=0, i.e all parents
        // 1 means all records available in the table

        $this->db->select('c1.ins_id,c1.ins_name,c1.ins_subtitle,c1.image_url,c2.ins_name as parent_name');
        $this->db->from('insurances c1');
        //if($id<=0)
        // {
        // $this->db->join('insurances c2', 'c1.parent_id = c2.ins_id');
        //$this->db->where('c1.ins_id', $id,'left outer');
        //}

        $this->db->join('insurances c2', 'c1.parent_id = c2.ins_id', 'left outer');

$this->db->order_by('c1.ins_id','desc');

        $query = $this->db->get();

        return $query->result();
    }

    public function showParentInsurance() {

        $this->db->select('ins_id,ins_name');
        $this->db->where('parent_id', 0);
        $query = $this->db->get('insurances');
        return $query->result();
    }
    public function sub_inc_cat($data){

     $this->db->select('ins_id,ins_name');
            $this->db->where('parent_id',$data);
            $query= $this->db->get('insurances');
            return $query->result();
		
	}

    function AddUpdateRecords($data) {

        $ins = $this->input->post('insid');

        if ($ins > 0) {
            $this->db->where('ins_id', $ins);
            $res = $this->db->update('insurances', $data);
            echo "hello1";
        } else {
            echo "hello";
            $res = $this->db->insert('insurances', $data);
        }


        return $res;
    }

    function DeleteSelected($id, $image_url) {

        $this->db->where('Ins_ID', $id);
        unlink("uploads/" . $image_url);
        unlink("uploads/thumbnails/" . $image_url);
        $res = $this->db->delete('insurances');
        return $res;
    }

    function FillRecords($id) {
        $this->db->where('Ins_ID', $id);
        $query = $this->db->get('insurances');
        return $query->result();
    }

    function insurence_list() {
        $this->db->order_by('ins_id', 'ASC');
        $this->db->select('ins_id,ins_name');
        $this->db->where('parent_id', 0);
        $query = $this->db->get('insurances');
        $array[] = 'Select insurance type';
        foreach ($query->result() as $row) {

            $array[$row->ins_id] = $row->ins_name;
        }
        return $array;
    }

}

?>