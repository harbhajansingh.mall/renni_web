<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blogs extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->helper('form');
        //$this->load->helper('url');
        $this->load->model('securearea/News_Blog_Model');
    }

    public function index() {
        $child_id = $this->uri->segment(3);

        if ($child_id == '') {
            $data['result'] = $this->News_Blog_Model->news_blog_show_all();
        } else {
            $data['result'] = $this->News_Blog_Model->news_blog_show_all($child_id);
        }
        $data['pagename'] = "blogs";
        $data['pagetitle'] = "Blogs";
        $this->load->view('template', $data);
    }

    public function details() {
        $id = $this->uri->segment(4);
        if ($id == '') {
            $data['result'] = $this->News_Blog_Model->blog_show_all();
        } else {
            $data['result'] = $this->News_Blog_Model->blog_show_all($id);
        }
        $data['pagename'] = "blog_detail";
        $data['pagetitle'] = "Blog Detail";
        $this->load->view('template', $data);
    }

}
