<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//require_once(APPPATH.'controllers/securearea/commoncontroller.php'); 
//class InsuranceController extends CommonController
class Insurance extends CI_Controller {

    function __construct() {
        parent::__construct();

        //---Check for user session-----
        $this->load->library('MY_Controller');
        if (!$this->my_controller->CheckSessions())
            redirect('login/logout');
        //------------------------------
        //$this->load->library('image_lib');
        $this->load->library(array('image_lib'));
        $this->load->model('securearea/Insurance_Model');
    }

    //---Shows the records from Insurances Table
    public function Index() {
        $data['msg'] = "";

        $result = $this->Insurance_Model->showAllRecords();
        //echo "hhh";
        //print_r($result);

        $data['pagename'] = "Insurance_list";
        $data['title'] = "Insurance list";
        $data['result'] = $result;

        $data['msg'] = $this->session->flashdata('msg');
        $this->load->view('SecureArea/template', $data);
    }

    //--Open up Form for Add/Update
    function openAddForm() {

        $result = $this->Insurance_Model->showParentInsurance();
        $data['parentlist'] = $result;

        $data['msg'] = $this->session->flashdata('msg');
        $data['result'] = $this->session->flashdata('res');
        $data['pagename'] = "Insurance_Add_Edit";
        $this->load->view('SecureArea/template', $data);
    }

    //--Execute Once Submit button Pressed for Add/Update 
    function Add_Update_Record() {
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('subtitle', 'Sub Title', 'trim');
        $filename = "";

        if (($_FILES["userfile"]["type"] == "image/gif") || ($_FILES["userfile"]["type"] == "image/jpeg") || ($_FILES["userfile"]["type"] == "image/png") || ($_FILES["userfile"]["type"] == "image/pjpeg")) {
            $ext = pathinfo($_FILES["userfile"]["name"], PATHINFO_EXTENSION);
            $maxval = md5(time());
            $url = 'uploads/' . $maxval . '.' . $ext;
            $url_thumb = 'uploads/thumbnails/' . $maxval . '.' . $ext;
            $filename = $this->compress_image($_FILES["userfile"]["tmp_name"], $url, 50);

            //$filename1 = $this->compress_image($_FILES["userfile"]["tmp_name"], $url, 50); 
        }



        if ($this->form_validation->run() == FALSE) {
            $data['msg'] = '';
            $this->openAddForm();
            return;
        } elseif ($this->form_validation->run() == TRUE AND isset($imageUploded['error'])) {
            $this->session->set_flashdata('msg', $imageUploded['error']);
            //$data['error']= $imageUploded['error'];
            $this->openAddForm();
            return;
        }


        //--Reading Form Data---
        $id = $this->input->post('insid');
        $insdetails = array(
            'ins_name' => $this->input->post('title'),
            'ins_subtitle' => $this->input->post('subtitle'),
            'parent_id' => $this->input->post('parent_id'),
            'image_url' => $filename
        );
        //print_r($insdetails);
        //return;
        //$id=$this->input->post('insid');
        $this->load->model('Securearea/Insurance_Model');
        $res = $this->Insurance_Model->AddUpdateRecords($insdetails);

        if ($res) {
            if ($id <= 0) {
                $data['msg'] = "";
                $this->session->set_flashdata('msg', 'Record Added Sucessfully');
                redirect(site_url('securearea/Insurance'));
            } else {
                $data['msg'] = "";
                $this->session->set_flashdata('msg', 'Record Updated Sucessfully');
                redirect(site_url('securearea/Insurance'));
            }

            $data['pagename'] = "Insurance_Add_Edit";
            $this->load->view('SecureArea/template', $data);
        }
    }

    //--Filling Data for update
    function Update() {
        $id = $this->uri->segment(4);
        $this->load->model('SecureArea/Insurance_Model');
        $result = $this->Insurance_Model->FillRecords($id);
        $this->session->set_flashdata('res', $result);
        $this->openAddForm();
    }

    //---Delete Selected Record
    function DeleteRec() {
        $id = $this->uri->segment(4);
        $img_url = $this->uri->segment(5);
        $this->load->model('SecureArea/Insurance_Model');
        $res = $this->Insurance_Model->DeleteSelected($id, $img_url);
        if ($res == 1) {
            $this->session->set_flashdata('msg', 'Record Deleted Sucessfully');
            redirect('securearea/Insurance');
        }
    }

    public function compress_image($source_url, $destination_url, $quality) {
        $info = getimagesize($source_url);
        if ($info['mime'] == 'image/jpeg')
            $image = imagecreatefromjpeg($source_url);
        elseif ($info['mime'] == 'image/gif')
            $image = imagecreatefromgif($source_url);
        elseif ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source_url);
        imagejpeg($image, $destination_url, $quality);
        $config['image_library'] = 'gd2';
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = 150;
        $config['height'] = 150;
        $config['source_image'] = $destination_url;
        $config['new_image'] = 'uploads/thumbnails';
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
        $this->image_lib->clear();
        return str_replace('uploads/', '', $destination_url);
    }

}

?>