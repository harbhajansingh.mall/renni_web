<?php
 defined('BASEPATH') OR exit('No direct script access allowed');
class Ckeditorform extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        //$this->load->model('FileUploader');
        $this->load->model('LocalFileUploader');
        $this->load->helper('ckeditor');
    }
 
    public function index()
    {
        $this->load->view('ckeditor-form', [            
            // HTML for textarea, populate using your model's property
            'content_html' => '',
        ]);
    }
 
    public function save()
    {
        if ($this->input->post('content') !== false) {
            // TODO persist model for 'content' textarea HTML containing uploaded
            // file's img reference.
 
        }
 
        header('Location: /ckeditor-form/');
        exit();
    }
 
    /**
     * Output CKEditor Javascript callback function for image file uploaded
     * in $_FILES['upload']. The GET parameters must also contain the
     * CKEditorFuncNum parameter so the JavaScript callback will reference
     * the correct CKEditor instance.
     */
    public function upload()
    {
        $callback = 'null';
        $url = '';
        $get = [];
 
        // for form action, pull CKEditorFuncNum from GET string. e.g., 4 from
        // /ckeditor-form/upload?CKEditor=content&CKEditorFuncNum=4&langCode=en
        // Convert GET parameters to PHP variables
        $qry = $_SERVER['REQUEST_URI'];
        parse_str(substr($qry, strpos($qry, '?') + 1), $get);
 
        if (!isset($_POST) || !isset($get['CKEditorFuncNum'])) {
            $msg = 'CKEditor instance not defined. Cannot upload image.';
        } else {
            $callback = $get['CKEditorFuncNum'];
 
            try {
                $fileUploader = new LocalFileUploader();
               // echo $this->base_url;
                $url = $fileUploader->uploadFile($_FILES['upload'],base_url());
                $msg = "File uploaded successfully to: {$url}";
            } catch (Exception $e) {
                $url = '';
                $msg = $e->getMessage();
            }
        }
 
        // Callback function that inserts image into correct CKEditor instance
        $output = '<html><body><script type="text/javascript">' .
            'window.parent.CKEDITOR.tools.callFunction(' .
            $callback .
            ', "' .
            $url .
            '", "' .
            $msg .
            '");</script></body></html>';
 
        echo $output;
    }
 
   
    
}