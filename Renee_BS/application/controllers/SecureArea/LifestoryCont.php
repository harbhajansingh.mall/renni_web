<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class LifestoryCont extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		
		//---Check for user session-----
		$this->load->library('MY_Controller');
		if(!$this->my_controller->CheckSessions()) redirect('login/logout');

$this->load->library('MY_Controller');
    if(!$this->my_controller->do_upload()) redirect('SecureArea/LifestoryCont');

		// //-----------Image upload-------------------
  //   $this->load->library('Image_upload');
  //   if(!$this->Image_upload->do_upload()) redirect('SecureArea/LifestoryCont');
	}

	public function index()
	{
	$data['pagename']="lifestory_show";
	 $this->load->model('securearea/Lifestorymodel');
     $result=$this->Lifestorymodel->getallrecord();
       
	 $data['result']=$result;
      $this->load->view('securearea/template',$data);
	}

	public function add()
   { 

        $this->load->model('securearea/Insurance_Model');
        $result=$this->Insurance_Model->showinsid_name();
        $data['result']=$result;
        $data['msg']=$this->session->flashdata('msg');
   		$data['pagename']="lifestory_add";
		$this->load->view('securearea/template',$data);
   }
// public function do_upload()
// {
//                $config['upload_path']          = './uploads/';
//                 $config['allowed_types']        = 'gif|jpg|PNG|png';
//                 $config['max_size']             = 10000;
                
               
//                 $this->load->library('upload', $config);

//                 if ( ! $this->upload->do_upload('userfile'))
//                 {
//                        $error = array('error' => $this->upload->display_errors());
//                         return $error;
//                        // $this->load->view('upload_form', $error);
//                 }
//                 else
//                 {
//                         $data = array('upload_data' => $this->upload->data());
//                         return $this->upload->data('file_name');
//                         //$this->load->view('upload_success', $data);
//                 }

// }

public function lifestorydata()
{



		    $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_rules('youtube', 'youtube', 'trim|required');
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('company', 'company', 'trim|required');
        $this->form_validation->set_rules('designation', 'designation', 'trim|required');
        $this->form_validation->set_rules('detail', 'detail', 'trim|required');
        $this->form_validation->set_rules('userfile', 'file', 'trim|xss_clean');

      
    if ($this->form_validation->run() == TRUE)
    {
        $imgname="";  
        if($_FILES['userfile']['name'])
        {
        
         //$imgname=$this->do_upload();
          $imgname=$this->my_controller->do_upload();
        
          if(isset($imgname['error']))
            {
            
                $this->session->set_flashdata('msg',$imgname['error']);
                redirect('securearea/LifestoryCont/add');
           
            }
        }
              $id=$this->input->post('ins'); 
    	$data1 = array(
			'title'=>$this->input->post('title'),
			'youtubeurl'=>$this->input->post('youtube'),
			'authorname'=>$this->input->post('name'),
			'company'=>$this->input->post('company'),
			'designation'=>$this->input->post('designation'),
			'details'=>$this->input->post('detail'),

			'imgurl'=>$imgname);
        
					
			$this->load->model('securearea/Lifestorymodel');
			$res=$this->Lifestorymodel->getdata($data1,$id);

      
		  if($res)
		      {	

			     $this->session->set_flashdata('msg',"succesfully saved");
                 redirect('securearea/LifestoryCont/add');
		      }
		   else
		      {
		
		      $this->session->set_flashdata('msg',"Error Occured");
              redirect('securearea/LifestoryCont/add');
		      }
    }   
    else
    {
         $this->load->model('securearea/Insurance_Model');
        $result=$this->Insurance_Model->showinsid_name();
    
        $data['result']=$result;
       $data['msg']="Error Occured";
    $data['pagename']="lifestory_add";
	 $this->load->view('securearea/template',$data);
       
    }

}
public function updateData()
 {
 	
    $this->form_validation->set_rules('title', 'title', 'trim|required');
        $this->form_validation->set_rules('youtube', 'youtube', 'trim|required');
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('company', 'company', 'trim|required');
        $this->form_validation->set_rules('designation', 'designation', 'trim|required');
        $this->form_validation->set_rules('detail', 'detail', 'trim|required');
         $this->form_validation->set_rules('userfile', 'file', 'trim|xss_clean');

      
    if ($this->form_validation->run() == TRUE)
    {
        $imgname=""; 
        if($_FILES['userfile']['name'])
        {
        
         $imgname=$this->do_upload();
          if(isset($imgname['error']))
            {
            
                $this->session->set_flashdata('msg',$imgname['error']);
                redirect('securearea/LifestoryCont/add');
           
            }
        }
        if($imgname!="")
{              
      $data1 = array(
      'title'=>$this->input->post('title'),
      'youtubeurl'=>$this->input->post('youtube'),
      'authorname'=>$this->input->post('name'),
      'company'=>$this->input->post('company'),
      'designation'=>$this->input->post('designation'),
      'details'=>$this->input->post('detail'),
      'imgurl'=>$imgname
                   );
        }
        else
        {
           $data1 = array(
      'title'=>$this->input->post('title'),
      'youtubeurl'=>$this->input->post('youtube'),
      'authorname'=>$this->input->post('name'),
      'company'=>$this->input->post('company'),
      'designation'=>$this->input->post('designation'),
      'details'=>$this->input->post('detail')
      
                   );
        }
           $insid=$this->input->post('ins');
           $rlid=$this->input->post('rlid'); 
      $this->load->model('securearea/Lifestorymodel');
      $res=$this->Lifestorymodel->updatedata($rlid,$data1,$insid);



      if($res)
          { 

           $this->session->set_flashdata('msg',"succesfully saved");
                 redirect('securearea/LifestoryCont/add');
          }
       else
          {
           
    
          $this->session->set_flashdata('msg',"Error Occured");
              redirect('securearea/LifestoryCont/add');
          }
    }   
    else
    {
         $this->load->model('securearea/Insurance_Model');
        $result=$this->Insurance_Model->showinsid_name();
    
        $data['result']=$result;
       $data['msg']="Error Occured";
    $data['pagename']="lifestory_add";
   $this->load->view('securearea/template',$data);
       
    }

}

public function showedit()
{  
    $this->load->model('securearea/Insurance_Model');
        $result=$this->Insurance_Model->showinsid_name();
        $data['result1']=$result;

      $id=$this->uri->segment(4);
    $this->load->model('securearea/Lifestorymodel');
    $res=$this->Lifestorymodel->showdataidwise($id);
    $data['result']=$res;
    $data['pagename']="lifestory_edit";
    $this->load->view('SecureArea/template',$data);
}

public function delete()
{
    $a=$this->uri->segment(4);
    $this->load->model('securearea/Lifestorymodel');
    $res=$this->Lifestorymodel->deleterec($a);
    redirect('securearea/LifestoryCont');
} 
 

}