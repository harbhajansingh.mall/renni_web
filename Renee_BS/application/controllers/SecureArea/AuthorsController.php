 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthorsController extends CI_Controller {


		function __construct(){
			parent::__construct();
			$this->load->library('MY_Controller');
			if(!$this->my_controller->CheckSessions()) redirect('login/logout');
			$this->load->library('image_lib');
		}

	public function authors_show(){
		$this->load->model('SecureArea/Author_Model');
		$data['result'] = $this->Author_Model->author_show();	
		$this->load->view('SecureArea/header');
		$this->load->view('SecureArea/author_show',$data);
		$this->load->view('SecureArea/footer');
	}
	
	public function authors_add(){
		if($this->input->post()){
			$data['author_name'] = $this->input->post('author_name');
			$this->load->model('SecureArea/Author_Model');
			$data = $this->Author_Model->author_add($data);
		    if($data){
				$this->session->set_flashdata('success', 'Add Successfully');
			}
			redirect(base_url('index.php/securearea/AuthorsController/authors_show'));			
		}
		else{
			$this->load->view('SecureArea/header');
			$this->load->view('SecureArea/author_add.php');
			$this->load->view('SecureArea/footer');
		}
	}

	public function authors_delete($delete_id){
		$this->load->model('SecureArea/Author_Model');
		$data = $this->Author_Model->author_delete($delete_id);
		if($data){
			$this->session->set_flashdata('deletesuccess', 'Delete successfully');
		}
		redirect(base_url('index.php/securearea/AuthorsController/authors_show'));
	}
	
	public function authors_update($delete_id){
		if($this->input->post()){
			$author_name['name'] = $this->input->post('author_name');
			$this->load->model('SecureArea/Author_Model');
			$data = $this->Author_Model->authors_update($author_name,$delete_id);
			if($data){
			$this->session->set_flashdata('updatesuccess', 'Update successfully');
			}
			redirect(base_url('index.php/securearea/AuthorsController/authors_show'));
		}
		else{
			$this->load->view('SecureArea/header');
			$data['data'] = $this->db->get_where('author_master', array('author_id =' => $this->uri->segment(4)))->row_array();
			$this->load->view('SecureArea/author_edit.php',$data);
			$this->load->view('SecureArea/footer');
		}
	}
		
}