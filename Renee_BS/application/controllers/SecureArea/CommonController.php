<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class CommonController extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->CheckSessions();
	}
	
	public function CheckSessions()
	{
		
		if(!isset($_SESSION['username']))
		{
			
			echo "Session is not available";
			//$this->load->view('securearea/login');
			//$this->session->sess_destroy();
			//redirect('login');
		}
		
		
	}
}
?>