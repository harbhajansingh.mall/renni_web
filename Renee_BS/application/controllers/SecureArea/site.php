<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct()
	{
		parent::__construct();
		$this->load->model('securearea/sitemodel');
		//$this->load->helper('url');
	}
	

	public function uploadContent(){
		$memid = $this->uri->segment(3);
		
		$val['info'] = $this->sitemodel->selectData('infodata',$memid);
		$val['qualdata'] = $this->sitemodel->viewContent('qualification');
		$val['prof'] = $this->sitemodel->viewContent('profession');
		$this->load->view('uploadContent',$val);
	}

	

	public function compress_image($source_url, $destination_url, $quality) 
		{ 
			$info = getimagesize($source_url); 
			if ($info['mime'] == 'image/jpeg') 
			$image = imagecreatefromjpeg($source_url); 
			elseif ($info['mime'] == 'image/gif') 
			$image = imagecreatefromgif($source_url); 
			elseif ($info['mime'] == 'image/png') 
			$image = imagecreatefrompng($source_url); 
			imagejpeg($image, $destination_url, $quality); 
			$config['image_library'] = 'gd2';
			$config['create_thumb'] = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width']         = 150;
			$config['height']       = 150;
			$config['new_image'] = 'images/thumbnails';
			$config['source_image'] = $destination_url;
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			return $destination_url; 
		}

	public function uploadImg($memid){

		if ($_POST) { 
		if ($_FILES["file"]["error"] > 0) { $error = $_FILES["file"]["error"]; }
		else if (($_FILES["file"]["type"] == "image/gif") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/pjpeg")) 
		{
			$ext = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
			//$this->db->select_max('id','maximgid');
			//$query = $this->db->get('imgdata');
			//$resmax = $query->row_array();
			$maxval = md5(time());
			$url = 'uploads/main'.$maxval.'.'.$ext; 
			$url_thumb = 'uploads/thumbnails/main'.$maxval.'_thumb.'.$ext; 
			$filename = $this->compress_image($_FILES["file"]["tmp_name"], $url, 50); 
			/*$data = array(
			"imgtitle"=>$this->input->post('imgtitle'),
			"imgpath"=>$url,
			"infoid"=>$memid,
			"thumbnailpath"=>$url_thumb);
			$res = $this->db->insert('imgdata',$data);*/
		}
		else { $error = "Uploaded image should be jpg or gif or png"; } 

		return;
		}
	}


	public function images(){
	
		$memid = $this->uri->segment(3);
		$data = array('infoid' => $memid);
		$val['info'] = $this->sitemodel->selectWhereData('imgdata',$data);
		$val['seldata'] = $this->sitemodel->viewContent('infodata');
		
		$this->load->view('securearea/images',$val);
	}

	

	public function uploadImage(){
		$memid = $this->uri->segment(3);
		$this->uploadImg($memid);
		//$this->images();
	}

	public function deleteImage(){
		$imgid = $this->uri->segment(4);
		$data = array('id' => $imgid);
		$val = $this->sitemodel->selectData('imgdata',$imgid);
		if($val){
			unlink($val['imgpath']);
			unlink($val['thumbnailpath']);
		}
		$query = $this->sitemodel->deleteData('imgdata',$imgid);
		$this->images();
	}

	
	
	
}
