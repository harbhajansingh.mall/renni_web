<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus extends CI_Controller 
{

    function __construct()
    {
        parent::__construct();
        //---Check for user session-----
        $this->load->library('MY_Controller');
        $this->load->model('Contactus_Model'); 
        if (!$this->my_controller->CheckSessions())
            redirect('login/logout');        
      
    }

    public function index()
    {
          
        $data['result'] = $this->Contactus_Model->show();
        $this->load->view('securearea/header');
        $this->load->view('securearea/contactus', $data);
        $this->load->view('securearea/footer');
    }
    public function delete($delete_id){
              
        $data = $this->Contactus_Model->delete($delete_id);
        if ($data) {
            $this->session->set_flashdata('deletesuccess', 'Contact query successfully deleted');
        }
        redirect(site_url('securearea/Contactus'));
    }

    
}