<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs extends CI_Controller {

		function __construct()
		{
			parent::__construct();
			
			//---Check for user session-----
			$this->load->library('MY_Controller');
			if(!$this->my_controller->CheckSessions()) redirect('login/logout');
			//------------------------------

			$this->load->library('image_lib');
                        $this->load->helper('ckeditor');
			
		
		}
	

		public function index()
		   { 
		   	 $this->load->model('securearea/Insurance_Model');
		    $res=$this->Insurance_Model->showParentInsurance();
		    $data['result']=$res;
			 $data['msg']=$this->session->flashdata('msg');
		    $data['pagename']="faqs_add";
                    $data['title'] = "Faqs add";
			$this->load->view('SecureArea/template',$data);
		   }

		public function show()
		   { 
		   	$this->load->model('securearea/Faqs_Model');
		    $res=$this->Faqs_Model->showallrecords();
		   $data['result']=$res;
		   
		    $data['pagename']="faqs_view";
                    $data['title'] = "Faqs list";
			$this->load->view('SecureArea/template',$data);
		   }

		public function process()
		   { 
                    if($this->input->post('sub_cat')!=''){
			 $sub_ins = implode(",", $this->input->post('sub_cat'));
                    }else{
                        $sub_ins='';
                    }
		
		   	$res="";
	    	 $this->form_validation->set_rules('question','question','trim|required');
	     	 $this->form_validation->set_rules('answer','answer','trim|required');
	    	
	    	
	    	
	 
	    if($this->form_validation->run() == TRUE)
	       {
	        $data=array(
	        'question'=>$this->input->post('question'),
	        'answer'=>$this->input->post('answer'),
	        'fstatus'=>$this->input->post('fstatus'),
	        'insurance'=>$this->input->post('ins_get'),
	        'Ins_Ids'=>$sub_ins
			

	                 );
	         	$insid=$this->input->post('ins');

	         	//echo $insid . " kkk";

		    $this->load->model('securearea/Faqs_Model');
		    $res = $this->Faqs_Model->insertrec($data,$insid);
			//echo $this->db->last_query();
			//die;
		    
		   }
		  
		   //return;
		 if($res)
		 {
		 	$this->session->set_flashdata('msg','Record Successfully Added');
		 	redirect(site_url('securearea/Faqs/show'));
		 }
		 else
		 {
		 	// echo $res . " ggg";
		 	//$this->session->set_flashdata('msg','Error Occurred');
		 	
		 }
	    	$this->index();
	  
	   

	  }	

	  public function deletedata()
			   {
				$data=$this->uri->segment(4);
				$this->load->model('securearea/Faqs_Model');
				$res=$this->Faqs_Model->deleterec($data);
                                $this->session->set_flashdata('msg','Record Successfully deleted');
				redirect('SecureArea/Faqs/show');
			   }

     public function showedit()
			{

				$id=$this->uri->segment(4);
				$this->load->model('securearea/Faqs_Model');
				$res=$this->Faqs_Model->showdata($id);
				$data['result']=$res;

		   	 $this->load->model('securearea/Insurance_Model');
		    $res=$this->Insurance_Model->showParentInsurance();
		    $data['result1']=$res;
				$data['pagename']="faqs_edit";
			    $this->load->view('SecureArea/template',$data);
				
			}


     public function updaterec()
		   {	
				$sub_ins = implode(",", $this->input->post('sub_cat'));
		     $faqid=$this->input->post('faqid');
		     $this->form_validation->set_rules('question','question','trim|required');
	     	 $this->form_validation->set_rules('answer','answer','trim|required');
	    	
	    	
	 
	    if($this->form_validation->run() == TRUE)
	       {
	 
	        $data=array(
	        'question'=>$this->input->post('question'),
	        'answer'=>$this->input->post('answer'),
	        'fstatus'=>$this->input->post('fstatus'),
	        'insurance'=>$this->input->post('insurance_get'),
	        'Ins_Ids'=>$sub_ins
	                 );

	    	  	$insid=$this->input->post('ins');
			    $this->load->model('securearea/Faqs_Model');
			    $res=$this->Faqs_Model->updatedata($faqid,$data,$insid);
 
      }
		
			
			
			if($res)
				{	
				$data['msg']="succesfully Updated";
				redirect('SecureArea/Faqs/show');
				}

				else
				{	

				$data['msg']="Error Occured";	
				$data['pagename']="faqs_add";
				$this->load->view('backend/template',$data);
				}


			}
			
			
			
	public function subinc_cat(){
		$data = $this->input->post('ins_val');
		$this->load->model('securearea/Faqs_Model');
	    $res['subcat']=$this->Faqs_Model->subinc_cat($data);
		
		$this->load->view('SecureArea/faqs_subcat',$res);
		
				
	}
			
}
	         
	