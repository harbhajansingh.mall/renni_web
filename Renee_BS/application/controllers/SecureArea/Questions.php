<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Questions extends CI_Controller 
{

    function __construct()
    {
        parent::__construct();
        //---Check for user session-----
        $this->load->library('MY_Controller');
        $this->load->model('Question_Model'); 
        if (!$this->my_controller->CheckSessions())
            redirect('login/logout');        
      
    }

    public function index()
    {          
        $data['result'] = $this->Question_Model->show();
        $this->load->view('securearea/header');
        $this->load->view('securearea/questions', $data);
        $this->load->view('securearea/footer');
    }
    public function delete($delete_id){               
        $data = $this->Question_Model->delete($delete_id);
        if ($data) {
            $this->session->set_flashdata('deletesuccess', 'Question successfully deleted');
        }
        redirect(site_url('securearea/Questions'));
    }

    
}