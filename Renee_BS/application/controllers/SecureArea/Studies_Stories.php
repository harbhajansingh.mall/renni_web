<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Studies_Stories extends CI_Controller {

    function __construct() {
        parent::__construct();

        //---Check for user session-----
        $this->load->library('MY_Controller');
        if (!$this->my_controller->CheckSessions())
            redirect('login/logout');
        //------------------------------

        $this->load->library('Imagecompresser');
        $this->load->helper('ckeditor');
    }

    public function index() {
        $this->load->model('securearea/Studies_Stories_Model');
        $data['result'] = $this->Studies_Stories_Model->studies_stories_show();
        $this->load->view('securearea/header');
        $this->load->view('securearea/studies_stories_show', $data);
        $this->load->view('securearea/footer');
    }

    public function studies_stories_add() {
        if ($this->input->post()) {
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('sub_title_short', 'Sub title short', 'trim|required');
            $this->form_validation->set_rules('sub_title_long', 'Sub title long', 'trim|required');
            $this->form_validation->set_rules('details', 'Detail', 'trim|required');
            $this->form_validation->set_rules('tag', 'Tag', 'trim|required');
            $this->form_validation->set_rules('ins_get', 'Insurence', 'required');




            if ($this->form_validation->run() == TRUE) {
                if ($this->input->post('sub_cat') != '') {
                    $sub_ins = implode(",", $this->input->post('sub_cat'));
                } else {
                    $sub_ins = '';
                }

                $data = array(
                    'title' => $this->input->post('title'),
                    'sub_title_short' => $this->input->post('sub_title_short'),
                    'sub_title_long' => $this->input->post('sub_title_long'),
                    'details' => $this->input->post('details'),
                    'short_reference' => $this->input->post('short_reference'),
                    'tags' => $this->input->post('tag'),
                    'insurance' => $this->input->post('ins_get'),
                    'edate' => date("Y/m/d"),
                    'Ins_Ids' => $sub_ins
                );

                if (!empty($_FILES['file']['name'])) {
                    $name1 = time() . $_FILES['file']['name'];
                    $temp_name1 = $_FILES['file']['tmp_name'];
                    $dir1 = 'uploads/casestudies/' . $name1;

                    $this->imagecompresser->compress_image($temp_name1, $dir1, 50, 'uploads/casestudies',1140,780);
                    $data['imageurl'] = $name1;
                }

                $this->load->model('securearea/Studies_Stories_Model');
                $data['result'] = $this->Studies_Stories_Model->studies_stories_add($data);
                if ($data) {
                    $this->session->set_flashdata('success', 'Added Successfully');
                }
                redirect(site_url('securearea/studies_stories'));
            } else {
                $this->load->model('securearea/Insurance_Model');
                $data['insurence_list'] = $this->Insurance_Model->insurence_list();
                $studentid = $this->session->userdata('au');
                $this->load->view('securearea/header');
                $this->load->view('securearea/studies_stories_add', $data);
                $this->load->view('securearea/footer');
            }
        } else {
            $this->load->model('securearea/Insurance_Model');
            $data['insurence_list'] = $this->Insurance_Model->insurence_list();
            $studentid = $this->session->userdata('au');
            $this->load->view('securearea/header');
            $this->load->view('securearea/studies_stories_add', $data);
            $this->load->view('securearea/footer');
        }
    }
    
    
    public function studies_stories_edit($id) {
        if ($this->input->post()) {
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('sub_title_short', 'Sub title short', 'trim|required');
            $this->form_validation->set_rules('sub_title_long', 'Sub title long', 'trim|required');
            $this->form_validation->set_rules('details', 'Detail', 'trim|required');
            $this->form_validation->set_rules('tag', 'Tag', 'trim|required');
            $this->form_validation->set_rules('ins_get', 'Insurence', 'required');




            if ($this->form_validation->run() == TRUE) {
                if ($this->input->post('sub_cat') != '') {
                    $sub_ins = implode(",", $this->input->post('sub_cat'));
                } else {
                    $sub_ins = '';
                }

                $data = array(
                    'title' => $this->input->post('title'),
                    'sub_title_short' => $this->input->post('sub_title_short'),
                    'sub_title_long' => $this->input->post('sub_title_long'),
                    'details' => $this->input->post('details'),
                    'short_reference' => $this->input->post('short_reference'),
                    'tags' => $this->input->post('tag'),
                    'insurance' => $this->input->post('ins_get'),
                    'edate' => date("Y/m/d"),
                    'Ins_Ids' => $sub_ins
                );

                if (!empty($_FILES['file']['name'])) {
                    $name1 = time() . $_FILES['file']['name'];
                    $temp_name1 = $_FILES['file']['tmp_name'];
                    $dir1 = 'uploads/casestudies/' . $name1;

                    $this->imagecompresser->compress_image($temp_name1, $dir1, 50, 'uploads/casestudies',1140,780);
                    $data['imageurl'] = $name1;
                }

                $this->load->model('securearea/Studies_Stories_Model');
                $data['result'] = $this->Studies_Stories_Model->studies_stories_update($data,$id);
                if ($data) {
                    $this->session->set_flashdata('success', 'Updated Successfully');
                }
                redirect(site_url('securearea/studies_stories'));
            } else {
                $this->load->model('securearea/Insurance_Model');
                $data['insurence_list'] = $this->Insurance_Model->insurence_list();
                $data['result'] = $this->db->get_where('casestudies_stories', array('rlid =' => $id))->row_array();
                $studentid = $this->session->userdata('au');
                $this->load->view('securearea/header');
                $this->load->view('securearea/studies_stories_edit', $data);
                $this->load->view('securearea/footer');
            }
        } else {
            $this->load->model('securearea/Insurance_Model');
            $data['insurence_list'] = $this->Insurance_Model->insurence_list();
            $data['result'] = $this->db->get_where('casestudies_stories', array('rlid =' => $id))->row_array();
            $studentid = $this->session->userdata('au');
            $this->load->view('securearea/header');
            $this->load->view('securearea/studies_stories_edit', $data);
            $this->load->view('securearea/footer');
        }
    }

    public function studies_stories_delete($delete_id) {
        $this->load->model('SecureArea/Studies_Stories_Model');
        $data = $this->Studies_Stories_Model->studies_stories_delete($delete_id);
        if ($data) {
            $this->session->set_flashdata('deletesuccess', 'Deleted successfully');
        }
        redirect(site_url('securearea/studies_stories'));
    }

}
