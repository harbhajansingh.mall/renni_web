<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class KeypointController extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		
		//---Check for user session-----
		$this->load->library('MY_Controller');
		if(!$this->my_controller->CheckSessions()) redirect('login/logout');
		//------------------------------

	}
     public function index()
	{
     
	 $data['message']=$this->session->flashdata('msg');
   		$data['pagename']="keypoint_add";
		$this->load->view('securearea/template',$data);
	}

	public function process()
		   { 
		   	$res="";
	    	 $this->form_validation->set_rules('title','title','trim|required');
	     	 $this->form_validation->set_rules('detail','detail','trim|required');
	    	
	    	
	    	
	 
	    if($this->form_validation->run() == TRUE)
	       {
	     
	        $data=array(
	        'title'=>$this->input->post('title'),
	        'detail'=>$this->input->post('details'),
	        'status'=>$this->input->post('kpstatus')

	                 );
	         	
	         	

		    $this->load->model('securearea/Keypoint_Model');
		    $res=$this->Keypoint_Model->insertrec($data,$insid);
		    
		   }
		  
		   //return;
		 if($res)
		 {
		 	$this->session->set_flashdata('msg','Record Successfully Added');
		 	// echo $res . " hhhhhhh";
		 }
		 else
		 {
		 	// echo $res . " ggg";
		 	$this->session->set_flashdata('msg','Error Occurred');
		 	
		 }
	    	$this->index();
	  //       $data['pagename']="faqs_add";
			// $this->load->view('SecureArea/template',$data); 
	   

	  }	
	  public function show()
		   { 
		   	
		   	
		    $this->load->model('securearea/Keypoint_Model');
		    $res=$this->Keypoint_Model->showallrecords();
		   $data['result']=$res;
		   
		    $data['pagename']="keypoint_show";
			$this->load->view('SecureArea/template',$data);
		   }
}