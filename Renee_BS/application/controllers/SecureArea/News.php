<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

    function __construct() {
        parent::__construct();

        //---Check for user session-----
        $this->load->library('MY_Controller');
        $this->load->library('Imagecompresser');
        if (!$this->my_controller->CheckSessions())
            redirect('login/logout');
        //------------------------------  
        $this->load->helper('ckeditor');
    }

    public function index() {
        $this->load->model('securearea/News_Blog_Model');
        $data['result'] = $this->News_Blog_Model->news_blog_show('news');
        $this->load->view('securearea/header');
        $this->load->view('securearea/news_view', $data);
        $this->load->view('securearea/footer');
    }

    public function news_add() {

        if ($this->input->post()) {
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('author_name', 'Author', 'trim|required');
            $this->form_validation->set_rules('news_reference', 'News Reference', 'trim|required');
            $this->form_validation->set_rules('details', 'Detail', 'trim|required');
            $this->form_validation->set_rules('tag', 'Tag', 'trim|required');
            $this->form_validation->set_rules('ins_get', 'Insurence', 'required');
            $this->form_validation->set_rules('redirection_url', 'Link', 'required');
            $this->form_validation->set_rules('ins_get', 'Insurence', 'required');
            


            if ($this->form_validation->run() == TRUE) {
                if ($this->input->post('sub_cat') != '') {
                    $sub_ins = implode(",", $this->input->post('sub_cat'));
                } else {
                    $sub_ins = '';
                }

                
                $data = array(
                    'title' => $this->input->post('title'),
                    'news_reference' => $this->input->post('news_reference'),
                    'author_id' => $this->input->post('author_name'),
                    'details' => $this->input->post('details'),
                    'tags' => $this->input->post('tag'),
                    'is_featured' => $this->input->post('optradio'),
                    'insurance' => $this->input->post('ins_get'),
                    'redirection_url' => $this->input->post('redirection_url'),
                    'type' => 'news',
                    
                    'nb_date' => date("Y/m/d"),
                    'Ins_Ids' => $sub_ins
                );
                
                if (!empty($_FILES['image']['name'])) {
                    $name1 = time() . $_FILES['image']['name'];
                    $temp_name1 = $_FILES['image']['tmp_name'];
                    $dir1 = 'uploads/newsblog/' . $name1;

                    $this->imagecompresser->compress_image($temp_name1, $dir1, 50, 'uploads/newsblog');
                    $data['image_url']=$name1;
                }

                if (!empty($_FILES['feature_image']['name']) && $this->input->post('optradio') == 1) {
                    $feature_image = time() . $_FILES['feature_image']['name'];
                    $dir11 = 'uploads/newsblog/' . $feature_image;

                    $this->imagecompresser->compress_image($_FILES['feature_image']['tmp_name'], $dir11, 50, 'uploads/newsblog');
                    $data['featured_image']=$feature_image;
                }
                $this->load->model('securearea/News_Blog_Model');
                $data['result'] = $this->News_Blog_Model->news_bloag_add($data);
                if ($data) {
                    $this->session->set_flashdata('success', 'News Successfully added');
                    redirect(site_url('securearea/news'));
                }
            } else {
                $this->load->model('SecureArea/Author_Model');
                $data['authordata'] = $this->Author_Model->author_show();
                $this->load->model('securearea/Insurance_Model');
                $data['insurence_list'] = $this->Insurance_Model->insurence_list();
                $studentid = $this->session->userdata('au');
                $this->load->view('securearea/header');
                $this->load->view('securearea/news_add', $data);
                $this->load->view('securearea/footer');
            }
        } else {
            $this->load->model('SecureArea/Author_Model');
            $data['authordata'] = $this->Author_Model->author_show();
            $this->load->model('securearea/Insurance_Model');
            $data['insurence_list'] = $this->Insurance_Model->insurence_list();
            $studentid = $this->session->userdata('au');
            $this->load->view('securearea/header');
            $this->load->view('securearea/news_add', $data);
            $this->load->view('securearea/footer');
        }
    }

    public function news_delete($delete_id) {
        $this->load->model('SecureArea/News_Blog_Model');
        $data = $this->News_Blog_Model->news_blog_delete($delete_id);
        if ($data) {
            $this->session->set_flashdata('deletesuccess', 'News successfully deleted');
        }
        redirect(site_url('securearea/News'));
    }

    public function news_edit($id) {        
        if ($this->input->post()) {
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('author_name', 'Author', 'trim|required');
            $this->form_validation->set_rules('news_reference', 'News Reference', 'trim|required');
            $this->form_validation->set_rules('details', 'Detail', 'trim|required');
            $this->form_validation->set_rules('tag', 'Tag', 'trim|required');
            $this->form_validation->set_rules('ins_get', 'Insurence', 'required');
            $this->form_validation->set_rules('redirection_url', 'Link', 'required');
            $this->form_validation->set_rules('ins_get', 'Insurence', 'required');
            


            if ($this->form_validation->run() == TRUE) {
                if ($this->input->post('sub_cat') != '') {
                    $sub_ins = implode(",", $this->input->post('sub_cat'));
                } else {
                    $sub_ins = '';
                }

                $data = array(
                    'title' => $this->input->post('title'),
                    'news_reference' => $this->input->post('news_reference'),
                    'author_id' => $this->input->post('author_name'),
                    'details' => $this->input->post('details'),
                    'tags' => $this->input->post('tag'),
                    'is_featured' => $this->input->post('optradio'),
                    'insurance' => $this->input->post('ins_get'),
                    'redirection_url' => $this->input->post('redirection_url'),                    
                    'nb_date' => date("Y/m/d"),
                    'Ins_Ids' => $sub_ins
                );
                
                
                if (!empty($_FILES['image']['name'])) {
                    $name1 = time() . $_FILES['image']['name'];
                    $temp_name1 = $_FILES['image']['tmp_name'];
                    $dir1 = 'uploads/newsblog/' . $name1;

                    $this->imagecompresser->compress_image($temp_name1, $dir1, 50, 'uploads/newsblog');
                    $data['image_url']=$name1;
                }

                if (!empty($_FILES['feature_image']['name']) && $this->input->post('optradio') == 1) {
                    $feature_image = time() . $_FILES['feature_image']['name'];
                    $dir11 = 'uploads/newsblog/' . $feature_image;

                    $this->imagecompresser->compress_image($_FILES['feature_image']['tmp_name'], $dir11, 50, 'uploads/newsblog');
                    $data['featured_image']=$feature_image;
                }
                
                
                
                $this->load->model('securearea/News_Blog_Model');
                $data['result'] = $this->News_Blog_Model->news_bloag_update($data,$id);
                if ($data) {
                    $this->session->set_flashdata('success', 'News Successfully updated');
                    redirect(site_url('securearea/news'));
                }
            } else {
                $this->load->model('securearea/News_Blog_Model');
        $this->load->model('SecureArea/Author_Model');
        $data['authordata'] = $this->Author_Model->author_show();
        $this->load->model('securearea/Insurance_Model');
        $data['insurence_list'] = $this->Insurance_Model->insurence_list();
        $data['result'] = $this->db->get_where('news_blogs', array('nb_id =' => $id))->row_array();        
        $this->load->view('SecureArea/header');
        $this->load->view('SecureArea/news_edit.php', $data);
        $this->load->view('SecureArea/footer');
            }
        } else {
            $this->load->model('securearea/News_Blog_Model');
        $this->load->model('SecureArea/Author_Model');
        $data['authordata'] = $this->Author_Model->author_show();
        $this->load->model('securearea/Insurance_Model');
        $data['insurence_list'] = $this->Insurance_Model->insurence_list();
        $data['result'] = $this->db->get_where('news_blogs', array('nb_id =' => $id))->row_array();        
        $this->load->view('SecureArea/header');
        $this->load->view('SecureArea/news_edit.php', $data);
        $this->load->view('SecureArea/footer');
        }
        
    }

//    public function subinc_cat() {
//        $data = $this->input->post('ins_val');
//        $this->load->model('securearea/Faqs_Model');
//        $res['subcat'] = $this->Faqs_Model->subinc_cat($data);
//        $this->load->view('SecureArea/news_blog_edit', $res);
//    }

}
