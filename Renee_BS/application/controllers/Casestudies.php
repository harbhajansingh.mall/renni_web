<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Casestudies extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->helper('form');
        //$this->load->helper('url');
        $this->load->model('securearea/Studies_Stories_Model');
    }

    public function index() {
        $child_id=$this->uri->segment(3);
        
        if($child_id==''){
        $data['result'] = $this->Studies_Stories_Model->studies_stories_show();
    }else{
        $data['result'] = $this->Studies_Stories_Model->studies_stories_show_bychildid($child_id);
    }
        $data['pagename'] = "case_studies";
        $data['pagetitle'] = "Case Studies";
        $this->load->view('template', $data);
    }

    public function details() {
        //$id=$this->uri->segment(3);
        //$data['result'] = $this->Studies_Stories_Model->studies_stories_readmore($id);
        $data['result'] = $this->Studies_Stories_Model->studies_stories_show();
        $data['pagename'] = "case_studies_open";
        $data['pagetitle'] = "Case Studies Detail";
        $this->load->view('template', $data);
    }

}
