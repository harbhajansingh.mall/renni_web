<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus extends CI_Controller 
{

    function __construct()
    {
        parent::__construct();
        //$this->load->helper('form');
        //$this->load->helper('url');
    }

    public function index()
    {
    $data['pagename']="contact_us";
    $data['pagetitle']="Contact Us";
    $this->load->view('template',$data);
    }
    public function save_contactus(){
        $this->load->model('Contactus_Model');
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['phone'] = $this->input->post('phone');
        $data['best_time'] = $this->input->post('best_time');
        $data['insurance']=$this->input->post('insurance');
        $data['subject']=$this->input->post('subject');
        $data['query']=$this->input->post('query');
        $ip = $this->input->ip_address();
        $data['ip_address'] = $ip;
       
        $this->Contactus_Model->add($data);
        echo 'Your query has sent to admin successfully.';
    }

    
}