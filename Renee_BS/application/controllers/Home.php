<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->helper('form');
        //$this->load->helper('url');
    }

    public function index() {
        $this->load->model('securearea/Faqs_Model');
        $res = $this->Faqs_Model->showallrecords_front_home();
        $data['result'] = $res;
        $data['pagetitle'] = "Renni Home";
        $this->load->view('index', $data);
    }

    public function subscribe() {
        $this->load->model('Subscribe_Model');
        $data['email'] = $this->input->post('emailid');
        $ip = $this->input->ip_address();
        $data['ip_address'] = $ip;
        if ($this->Subscribe_Model->add($data)) {
            echo 'Thank you for your subscription.';
        } else {
            echo 'This email address is already subscribed, thank you!';
        }
    }

    public function save_question() {
        $this->load->model('Question_Model');
        $data['name'] = $this->input->post('name');
        $data['email'] = $this->input->post('email');
        $data['phone'] = $this->input->post('phonenumber');
        $data['question'] = $this->input->post('question');
        $ip = $this->input->ip_address();
        $data['ip_address'] = $ip;
        $this->Question_Model->add($data);
        echo 'Your question has sent to admin successfully.';
    }

}
