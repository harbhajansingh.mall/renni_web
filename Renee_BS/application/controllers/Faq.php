<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

    function __construct() {
        parent::__construct();
        //$this->load->helper('form');
        //$this->load->helper('url');
    }

    public function index() {
        $id = $this->uri->segment(3);
        $this->load->model('securearea/Faqs_Model');
        if($id==''){
        $res = $this->Faqs_Model->showallrecords_front();
        }else{
            $res=$this->Faqs_Model->showdata_byinsuranceid($id);
        }
        $this->load->model('securearea/Insurance_Model');
        $parents = $this->Faqs_Model->getinsuranceparent();
        $data['parents'] = $parents;
        $data['result'] = $res;
        $data['pagename'] = "faqs";
        $data['pagetitle'] = "Renni FAQ's";
        $this->load->view('template', $data);
    }

}
