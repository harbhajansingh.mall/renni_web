	
<!-- Banner Part -->
<section class="faq-top-section">
    <div class="container">			
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                &nbsp;				
            </div>
        </div> 
    </div>
</section>
<!-- //////////// Banner Part //////////// -->

<style>
    .activtabfq{
        color: #a1b1bc !important;
    }
    .panel-body{color:#000; }
</style>
<!-- section 05 -->
<section class="section-05">
    <div class="container">
        <div class="row panel-group">
            <div class="col-md-3">
                <div class="Left_Bar_Sec">
                    <ul>
                        <li><a <?php echo faq_selected_subtab('activtabfq','',$this->uri->segment(3));?> href="<?php echo site_url('faq'); ?>">
                                General FAQ’s
                                <i class="fa fa-angle-down Dropdown_arrow_ico" aria-hidden="true"></i>
                            </a></li>
                        <?php foreach ($parents as $parent): ?>
                            <li>
                                <a <?php echo faq_selected_subtab('activtabfq',$parent->ins_id,$this->uri->segment(3));?> href="<?php echo site_url('faq/index/' . $parent->ins_id); ?>">
                                    <?php echo $parent->ins_name; ?>
                                    <i class="fa fa-angle-down Dropdown_arrow_ico" aria-hidden="true"></i>
                                </a>
                            </li>
                        <?php endforeach; ?>							
                    </ul>
                </div>				
            </div>
            <div class="col-md-9">
                <!-- Tabs --> 
                <div class="form-tabs" id="accordion-f" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                                                
                        <?php foreach ($result as $resul): ?>
                            <div class="panel-heading tab-bg-clr01" role="tab" id="heading01-f">
                                <h4 class="panel-title">
                                    <a class="faq_tab" role="button" data-toggle="collapse" data-parent="#accordion-f" href="#collapse<?php echo $resul->faqid; ?>-f" aria-expanded="true" aria-controls="collapse<?php echo $resul->faqid; ?>-f" class="collapsed">
                                        <?php echo $resul->question; ?>
                                        <i class="fa fa-angle-down faq-clr01"></i>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse<?php echo $resul->faqid; ?>-f" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading01-f" aria-expanded="false" >
                                <div class="panel-body faq-text">
                                    <?php echo $resul->answer; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <!-- Tabs ////////// -->					
            </div>				
        </div>

    </div>
</section>
<!-- //////////// section 05 //////////// -->



<!-- //////////// section 05 //////////// -->
<section class="logo-section">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <?php $this->load->view('all_logo'); ?>
            </div>
        </div>
    </div>
</section>


<!-- section 08 -->
<section class="section-08-iner">
    <div class="section-img-inner">
        <div class="container">
            <div class="row">            
                <div class="col-lg-7 col-sm-6 discover-t_not">Not sure about something? </div>
                <div class="col-lg-5 col-sm-6 two-button-inr text-right">
                    <a href="javascript:void(0);" class="button-request-inr">REQUEST ASSISTANCE</a>
                </div>
            </div>    
        </div>
    </div>
</section> 
<!-- //////////// section 08 //////////// -->
