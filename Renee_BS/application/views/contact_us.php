<!-- Banner Part -->
<section class="Trauma-top-section contact_page">
    <div class="container">			
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <div class="banner-text-inner">
                    <div class="take-to">
                        Get in Touch 
                    </div>
                    <p>We help you understand insurance better.</p> 					 
                </div>					
            </div>
        </div> 
    </div>
</section>
<!-- //////////// Banner Part //////////// -->

<section class="Contact_section"> 
    <div class="top_sect">
        <h3>Click – Call – Post</h3>
        <p>Contact us about: general advice, information, questions or any concerns you may have. </p>
    </div>
    <div class="top_sect_in">
        <h4>REASONS FOR INQUIRY</h4>
        <p>Every Renni web page section has a heading and a sub-heading </p>
    </div>
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <!-- <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
        </ol> -->

        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <ul class="tab-list carousel-indicators">
                    <li id="tab-one" class="tab-line01 active" data-target="#carousel-example-generic" data-slide-to="0">
                        <a href="#">
                            <div class="round-img">
                                <div class="rimg01">
                                    <img src="<?php echo base_url('assets/img/icons/contact_icon01.png');?>" alt="" />
                                </div>
                                <div class="round-img-div">Request a Demo</div>
                                <div class="title-bor"></div>
                            </div>
                            <div class="renni-p">
                                We’ll step you through some common <br>examples of how insurance can work for you. 
                            </div>
                            <div class="learn-more">
                                Discover More
                            </div>              
                            <div class="tab-img-h">&nbsp;</div>
                        </a>
                    </li>
                    <li class="tab-line02" data-target="#carousel-example-generic" data-slide-to="1">
                        <a href="">
                            <div class="round-img">
                                <div class="rimg01">
                                    <img src="<?php echo base_url('assets/img/icons/contact_icon02.png');?>" alt="" />
                                </div>
                                <div class="round-img-div">Customer Support</div>
                                <div class="title-bor"></div>
                            </div>
                            <div class="renni-p">
                                Can’t find what your looing for? Contact us <br>and we’ll get back to you as soon as we can.
                            </div>
                            <div class="learn-more">
                                Contact Us
                            </div>
                            <div class="tab-img-h">&nbsp;</div>
                        </a>
                    </li>
                    <li class="tab-line03" data-target="#carousel-example-generic" data-slide-to="2">
                        <a href="">
                            <div class="round-img">
                                <div class="rimg01">
                                    <img src="<?php echo base_url('assets/img/icons/contact_icon03.png');?>" alt="" />
                                </div>
                                <div class="round-img-div">Request A Quote</div>
                                <div class="title-bor"></div>
                            </div>
                            <div class="renni-p">
                                Take the path to protection today with a no<br> obligation quote.
                            </div>
                            <div class="learn-more">
                                Start
                            </div>
                            <div class="tab-img-h">&nbsp;</div>
                        </a>
                    </li>               
                </ul>
            </div>
        </div>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <?php $this->load->view('contactus_form'); ?>
        </div>
        <!-- Controls -->
        <a class="left carousel-control prev-button" href="#carousel-example-generic" role="button" data-slide="prev">
            <img src="<?php echo base_url('assets/img/icons/prev.png');?>" alt="prev" />
        </a>
        <a class="right carousel-control next-button" href="#carousel-example-generic" role="button" data-slide="next">
            <img src="<?php echo base_url('assets/img/icons/next.png');?>" alt="Next" />
        </a>
    </div>

</section>

<!-- //////////// section 02 //////////// -->

<!-- //////////// section 05 //////////// -->
<section class="">
    <div class="container-fluid">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding">
            <div class="connect_info_bg">
                <div class="col-md-12">
                    <span class="connect_title">Connect With Us</span>
                    <p>
                        <a href=""><i class="fa fa-facebook"></i></a>
                        <a href=""><i class="fa fa-twitter"></i></a>
                        <a href=""><i class="fa fa-google-plus"></i></a>
                        <a href=""><i class="fa fa-linkedin"></i></a>
                        <a href=""><i class="fa fa-skype"></i></a>
                    </p>
                    <p>Follow us to see blog updates, hear the latest insurance news, and keep up to date with activities from our community support partners. </p>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding">
            <div class="contact_info_bg">
                <div class="col-md-12">
                    <p class="contact_title">Contact Us</p>
                    <span class="connect_title02">RENNI</span>                       
                    <p class="renni_info">We put our resources into providing affordable policies and creating insightful content that helps you understand insurance.</p>
                    <div class="row">
                        <div class="col-md-1">
                            <div class="mail_icon">
                                <img class="fa fa" src="<?php echo base_url('assets/img/inner-img/mail_icon.png');?>" />
                            </div>
                        </div>
                        <div class="col-md-10 icon_text">
                            <span>Postal Address</span><br>
                            Renni Australia Pty Ltd<br>
                            PO Box A1073 <br>
                            Sydney NSW 2000
                        </div>
                    </div>
                    <div class="row contac_space">
                        <div class="col-md-1">
                            <div class="mail_icon">
                                <img class="fa fa" src="<?php echo base_url('assets/img/inner-img/mobile_icon.png');?>" />
                            </div>
                        </div>
                        <div class="col-md-10 icon_text">
                            <span>Call Renni</span><br>
                            Phone: (08) 9777 7777<br>
                            Fax:      (08) 9777 7778
                        </div>
                    </div>
                    <div class="row contac_space">
                        <div class="col-md-1">
                            <div class="mail_icon">
                                <img class="fa fa" src="<?php echo base_url('assets/img/inner-img/email_icon.png');?>" />
                            </div>
                        </div>
                        <div class="col-md-10 icon_text">
                            <span>E-mail Renni</span><br>
                            <a href="">enquiries@renni.com.au</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>	
<!-- //////////// section 05 //////////// -->
<section class="logo-section">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <?php $this->load->view('all_logo'); ?>
            </div>
        </div>
    </div>
</section>