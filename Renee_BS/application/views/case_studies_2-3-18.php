<!-- Banner Part -->
	<section class="case_top-section">
		<div class="container">			
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
					<div class="banner-text-inner02">
						<div class="take-to">
							Everyday examples of HOW
						</div>
						<div class="today_icome">
							<span>insurance</span> Can work for you.
						</div>
						<div class="request-now-case">
							<a href="contact.html">REQUEST A QUOTE</a>
						</div>
					</div>					
				</div>
			</div> 
		</div>
	</section>
	<!-- //////////// Banner Part //////////// -->
		
		
		
	<!-- section 03 REAL LIFE STORIES-->
	<section class="case_section">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-xs-12">
					<span class="personal_Title">Personal Insurance</span>
					<div class="burgger_menu_div">
						<a href="javascript:void(0);">
							<img src="<?php echo base_url('assets/img/menu-img.png')?>" alt="" />
						</a>
					</div>
					<div class="all_insurance">						
						<ul>
							<li class="per_ins">Personal Insurance</li>
							<li><a href="life_insurance.html">Life Insurance</a></li>
							<li><a href="trauma-nsurance.html">Trauma Insurance</a></li>
							<li><a href="disability-insurance.html">Permanent Disability Insurance</a></li>
							<li><a href="income-protection.html">Income Protection</a></li>
							<li class="per_ins">Travel Insurance</li>
							<li><a href="">Travel Insurance Type -1</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-7 col-xs-12">
					<div class="tab_case">						
						<ul class="nav nav-tabs">
			 				<li class="active"><a href="#tab_01" data-toggle="tab" aria-expanded="true">All</a></li>
							<li class=""><a href="#tab_02" data-toggle="tab" aria-expanded="false">Life</a></li>
							<li class=""><a href="#tab_03" data-toggle="tab" aria-expanded="false">Disability</a></li>
							<li class=""><a href="#tab_04" data-toggle="tab" aria-expanded="false">Income</a></li>
							<li class=""><a href="#tab_05" data-toggle="tab" aria-expanded="false">Trauma</a></li>
							<li class=""><a href="#tab_06" data-toggle="tab" aria-expanded="false">Other</a></li>

							<!--li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
							  Dropdown <span class="caret"></span>
							</a>
							<ul class="dropdown-menu">
							  <li><a href="#dropdown1" data-toggle="tab">Action</a></li>
							  <li class="divider"></li>
							  <li><a href="#dropdown2" data-toggle="tab">Another action</a></li>
							</ul>
						  </li-->
						</ul>
					</div>
				</div>
			</div>
			
			

			<div id="myTabContent" class="tab-content">
			  	<div class="tab-pane fade active in" id="tab_01">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
						<div class="case_studies_div">
							<div class="real-title-inr">
								<img src="<?php echo base_url('assets/img/inner-img/Icome_Help_01.png')?>" class="img-responsive">
							</div>
							<div class="button_over">
								<a href="">INCOME </a>
								<a href="">TRAUMA</a>
								<a href="">INSURANCE TAG</a>
							</div>
							<div class="Video_CNT">
								<span>Mary had a stroke</span>
								<div class="Date_TPD">
									Trauma insurance : $20K lump sum     Income protection : 75% of previous income 
								</div>
							</div>
							<div class="tab-content01">Mary (37) suffered a stroke as a result of a blood clot becoming lodged in a vein inside her brain. In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet. Morbi tristique, mauris eu imperdiet
							</div>
							<div class="more_case">
								<a href="<?php echo site_url('Casestudies/details')?>">Read More</a>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
						<div class="real-title-inr">
							<img src="<?php echo base_url('assets/img/inner-img/Icome_Help_02.png')?>" class="img-responsive">
						</div>
						<div class="button_over">
							<a href="">TRAUMA</a>
							<a href="">Life</a>
						</div>
						<div class="Video_CNT">
							<span>Sally copes with breast cancer</span>
							<div class="Date_TPD">
								Trauma insurance payout of $105,000 (attached to life insurance policy)  
							</div>
						</div>
						<div class="tab-content01">Sally (41) was undergoing chemotherapy for breast cancer. She and her family were put under additional pressure when her self-employed husband lost his major business client.Morbi venenatis lacinia rhoncus. Pellentesque non bibendum tellus, vitae semper sem. Morbi, In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris, eget ultrices mauris rhoncus non. Morbi tristique, mauris 
						</div>
						<div class="more_case">
							<a href="<?php echo site_url('Casestudies/details')?>">Read More</a>
						</div>
					</div>
				</div>
				<div class="row case_space">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
						<div class="real-title-inr">
							<img src="<?php echo base_url('assets/img/inner-img/Icome_Help_08.png')?>" class="img-responsive">
						</div>
						<div class="button_over">
							<a href="">INCOME </a>
							<a href="">TRAUMA</a>
							<a href="">INSURANCE TAG</a>
						</div>
						<div class="Video_CNT">
							<span>Tom develops skin cancer</span>
							<div class="Date_TPD">
								Trauma insurance payout of $120,000 
							</div>
						</div>
						<div class="tab-content01">Tom, a 42 year old self-employed tradesman, noticed a spot on his left leg that would not heal. His doctor ran some tests and informed Tom he had stage two skin cancer (basal cell carcinoma). Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, In varius varius justo, eget ultrices mauris rhoncus non. 
						</div>
						<div class="more_case">
							<a href="<?php echo site_url('Casestudies/details')?>">Read More</a>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
						<div class="real-title-inr">
							<img src="<?php echo base_url('assets/img/inner-img/Icome_Help_03.png')?>" class="img-responsive">
						</div>
						<div class="button_over">
							<a href="">INCOME </a>
							<a href="">TRAUMA</a>
							<a href="">INSURANCE TAG</a>
						</div>
						<div class="Video_CNT">
							<span>Jeff diagnosed with terminal bowel cancer</span>
							<div class="Date_TPD">
								Trauma insurance  ($20K lump sum)  |   Income protection (75% of previous income)
							</div>
						</div>
						<div class="tab-content01">Mary (37) suffered a stroke as a result of a blood clot becoming lodged in a vein inside her brain. In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, In varius varius justo, eget ultrices mauris rhoncus non.
						</div>
						<div class="more_case">
							<a href="<?php echo site_url('Casestudies/details')?>">Read More</a>
						</div>
					</div>
				</div>
				<div class="row case_space">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
						<div class="real-title-inr">
							<img src="<?php echo base_url('assets/img/inner-img/Icome_Help_04.png')?>" class="img-responsive">
						</div>
						<!-- <div class="button_over">
							<a href="">INCOME </a>
							<a href="">TRAUMA</a>
							<a href="">INSURANCE TAG</a>
						</div> -->
						<div class="Video_CNT">
							<span>Jack is diagnosed with Multiple Sclerosis.</span>
							<div class="Date_TPD">
								Trauma insurance  ($20K lump sum)  |   Income protection (75% of previous income)
							</div>
						</div>
						<div class="tab-content01">Keith (Mr. Wellness), collapsed unexpectedly at work – he was later diagnosed with a brain tumor. Hear how income protection kept Keith’s business solvent while he recuperated from neurosurgery. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, In varius varius justo, eget ultrices mauris rhoncus non. 
						</div>
						<div class="more_case">
							<a href="<?php echo site_url('Casestudies/details')?>">Read More</a>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
						<div class="real-title-inr">
							<img src="<?php echo base_url('assets/img/inner-img/Icome_Help_05.png')?>" class="img-responsive">
						</div>
						<div class="button_over">
							<a href="">INCOME </a>
							<a href="">TRAUMA</a>
							<a href="">INSURANCE TAG</a>
						</div>
						<div class="Video_CNT">
							<span>Sophie gets thyroid cancer</span>
							<div class="Date_TPD">
								Trauma insurance payout of $781,385
							</div>
						</div>
						<div class="tab-content01">Sophie, a 52 year old bookkeeper, found she had thyroid cancer after her doctor ran some tests for some pain and swelling in her neck. n varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, In varius varius justo, eget ultrices mauris rhoncus non.
						</div>
						<div class="more_case">
							<a href="<?php echo site_url('Casestudies/details')?>">Read More</a>
						</div>
					</div>
				</div>
				<div class="row case_space">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
						<div class="real-title-inr">
							<img src="<?php echo base_url('assets/img/inner-img/Icome_Help_06.png')?>" class="img-responsive">
						</div>
						<div class="button_over">
							<a href="">TRAUMA</a>
							<a href="">INSURANCE TAG</a>
						</div>
						<div class="Video_CNT">
							<span>Mark has a road accident </span>
							<div class="Date_TPD">
								Permanent disability Cover (own occupation) for $2 million
							</div>
						</div>
						<div class="tab-content01">Mark (32) sold mining and excavating equipment throughout Australia. He was involved in a serious car accident coming back from a site visit and was left with serious facial injuries and a severe, permanent speech impediment. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, In varius varius justo, eget ultrices mauris rhoncus non. 
						</div>
						<div class="more_case">
							<a href="<?php echo site_url('Casestudies/details')?>">Read More</a>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
						<div class="real-title-inr">
							<img src="<?php echo base_url('assets/img/inner-img/Icome_Help_07.png')?>" class="img-responsive">
						</div>
						<div class="button_over">
							<a href="">TRAUMA</a>
							<a href="">LIFE INSURANCE</a>
						</div>
						<div class="Video_CNT">
							<span>Sarah died in a road accident while cycling</span>
							<div class="Date_TPD">
								Life cover for $350,000
							</div>
						</div>
						<div class="tab-content01">When Sarah died unexpected Tom had to learn how to be a single dad. Life insurance helped make things easier. In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, In varius varius justo, eget ultrices mauris rhoncus non.
						</div>
						<div class="more_case">
							<a href="<?php echo site_url('Casestudies/details')?>">Read More</a>
						</div>
					</div>
				</div>
			  	</div>

			  	<div class="tab-pane fade" id="tab_02">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
							<div class="case_studies_div">
								<div class="real-title-inr">
									<img src="<?php echo base_url('assets/img/inner-img/Icome_Help_01.png')?>" class="img-responsive">
								</div>
								<div class="button_over">
									<a href="">INCOME </a>
									<a href="">TRAUMA</a>
									<a href="">INSURANCE TAG</a>
								</div>
								<div class="Video_CNT">
									<span>Mary had a stroke</span>
									<div class="Date_TPD">
										Trauma insurance : $20K lump sum     Income protection : 75% of previous income 
									</div>
								</div>
								<div class="tab-content01">Mary (37) suffered a stroke as a result of a blood clot becoming lodged in a vein inside her brain. In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet. Morbi tristique, mauris eu imperdiet
								</div>
								<div class="more_case">
									<a href="<?php echo site_url('Casestudies/details')?>">Read More</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
							<div class="real-title-inr">
								<img src="<?php echo base_url('assets/img/inner-img/Icome_Help_02.png')?>" class="img-responsive">
							</div>
							<div class="button_over">
								<a href="">TRAUMA</a>
								<a href="">Life</a>
							</div>
							<div class="Video_CNT">
								<span>Sally copes with breast cancer</span>
								<div class="Date_TPD">
									Trauma insurance payout of $105,000 (attached to life insurance policy)  
								</div>
							</div>
							<div class="tab-content01">Sally (41) was undergoing chemotherapy for breast cancer. She and her family were put under additional pressure when her self-employed husband lost his major business client.Morbi venenatis lacinia rhoncus. Pellentesque non bibendum tellus, vitae semper sem. Morbi, In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris, eget ultrices mauris rhoncus non. Morbi tristique, mauris 
							</div>
							<div class="more_case">
								<a href="<?php echo site_url('Casestudies/details')?>">Read More</a>
							</div>
						</div>
					</div>
			 	</div>
			  	<div class="tab-pane fade" id="tab_03">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
							<div class="case_studies_div">
								<div class="real-title-inr">
									<img src="<?php echo base_url('assets/img/inner-img/Icome_Help_01.png')?>" class="img-responsive">
								</div>
								<div class="button_over">
									<a href="">INCOME </a>
									<a href="">TRAUMA</a>
									<a href="">INSURANCE TAG</a>
								</div>
								<div class="Video_CNT">
									<span>Mary had a stroke</span>
									<div class="Date_TPD">
										Trauma insurance : $20K lump sum     Income protection : 75% of previous income 
									</div>
								</div>
								<div class="tab-content01">Mary (37) suffered a stroke as a result of a blood clot becoming lodged in a vein inside her brain. In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet. Morbi tristique, mauris eu imperdiet
								</div>
								<div class="more_case">
									<a href="<?php echo site_url('Casestudies/details')?>">Read More</a>
								</div>
							</div>
						</div>						
					</div>
			 	</div>
			  	<div class="tab-pane fade" id="tab_04">
					gdfgdfg
			 	 </div>
			 	 <div class="tab-pane fade" id="tab_05">
					dfgdg
			 	 </div>
			 	 <div class="tab-pane fade" id="tab_06">
					dgfgdf
			 	 </div>
			</div>

			
			
			
			<div class="row case_space">
				<div class="col-sm-12">
                    <div class="form-group load_more">
                        <button class="submit_button">LOAD MORE</button>
                    </div>
                </div>
			</div>
		</div>
	</section>
	<!-- //////////// section 03 //////////// -->	
	
	<!-- //////////// section 05 //////////// -->
	<section class="logo-section" style="border-top: 1px solid #eee;">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
					<?php $this->load->view('all_logo');  ?>
				</div>
			</div>
		</div>
	</section>
	