<!-- Banner Part -->
	<section class="Trauma-top-section">
		<div class="container">			
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
					<div class="banner-text-inner">
						<div class="take-to">
							TAKE THE PATH TO
						</div>
						<div class="today_Trauma">
							PROTECTION <span>TODAY.</span>
						</div>
						<div class="request-now">
							<a href="<?php echo site_url('contactus') ?>">REQUEST Now</a>
						</div>
					</div>					
				</div>
			</div> 
		</div>
	</section>
	<!-- //////////// Banner Part //////////// -->
		
	<!-- Section 02 -->
	<section class="section-02">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
					<?php $this->load->view('left_panel_inner_ins');  ?>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
					<div class="real-title01">WHAT IS TRAUMA INSURANCE?</div>
					<div class="border-blueinr"></div>
					<div class="tab-content">Trauma insurance (or critical illness insurance) pays you a lump sum if you suffer a serious injury or illness such as: heart attack, cancer or stroke. It is paid to you for the specific injury or illness that you are covered for. </div>
				</div>
			</div>
			<div class="row top-space">
				<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">
					<div class="watch"><img src="<?php echo base_url('assets/img/inner-img/Trauma_mind.png')?>" alt="" /></div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-7 col-xs-12">
					<div class="real-title01">HOW DOES TRAUMA INSURANCE HELP?</div>
					<div class="border-blueinr"></div>
					<div class="tab-content01">For many of us a serious injury or illness will mean taking extended time off work to recover.  Trauma insurance helps you:
					</div>
					<div class="icn_space_Icome">
						<ul class="banner-list_Trauma">
							<li><i class="fa fa-check-circle" aria-hidden="true"></i>Pay existing out-of-pocket medical expenses </li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i>Cover rehabilitation costs</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i>Provide a source of revenue while you are unable to work</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i>Pay the bills and mortgage</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i>Meet specific living expenses such as childcare</li>
						</ul>
					</div>
					<div class="box-future text-center">
						<div class="future-title">HELP PROTECT YOURSELF FROM LIFE’s <br>SERIOUS INJURIES AND ILLNESSES</div>
						<div class="getsefl">Get covered with Trauma insurance from as <br>little as  <span>$20 per month.</span>
						</div>
						<div class="get-button"><a href="">GET INSURED</a></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- //////////// section 02 //////////// -->
		
	<!-- section 03 REAL LIFE STORIES-->
	<section class="section-03">
		<div class="container">			
			<div class="row top-space">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
						<div class="real-title">
							Trauma Cover  <br><strong>Michael suffered a heart attack </strong>
							<div class="border-blue"></div>
						</div>
					</div>
					<div class="testimonial-inner02"> 						 
						<div class="t-box-space">							
							<span class="mau-text">Michael suffered a heart attack while sking in Europe. He was young, fit, did not drink, and was a non-smoker. Listen to how trauma insurance helped his family through an emotional and financially stressful time.
								<br><br></span>
							<div class="row mau-img tp-sp">
								<span class="col-md-9 user-text">
									<!-- <img src="assets/img/inner-img/img03.png" alt=""> -->
									<span class="frog-desi">
									<strong>Michael (36)</strong><br>
									Self employed
									</span>
								</span>
							</div>
						</div>
				   </div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">                    
					<div class="testimonial-inner">  
						<div class="quote-img">
							<div class="quate-icon"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
						</div>
						<div class="t-img_icome">
							<a href="#" class='click' value="QH03CgaGk3I">
								<img src="<?php echo base_url('assets/img/inner-img/Trauma_video.png')?>" alt="" class="img-responsive" />
							</a>
						</div>						
				   </div>
				</div>
			</div>			
		</div>
	</section>
	<!-- //////////// section 03 //////////// -->
	<!-- section 03 REAL LIFE STORIES-->
	<section class="section-04">
		<div class="container">
			<!-- Section 02 -->
			<div id="carousel-example-generic01" class="carousel slide" data-ride="carousel">				
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="chances_Trauma text-center">
									 HOW WOULD YOU COPE WITH A SERIOUS INJURY OR ILLNESS?
									<div class="border-white"></div>
								</div>
								<div class="text-center what-content">The majority of trauma claims over the five year period 2010 – 2014 were the result of unexpected medical issues.1 </div>
							</div>
						</div>
					</div>
					
					<div class="item">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="chances_Trauma text-center">
									 HOW WOULD YOU COPE WITH A SERIOUS INJURY OR ILLNESS?
									<div class="border-white"></div>
								</div>
								<div class="text-center what-content">The majority of trauma claims over the five year period 2010 – 2014 were the result of unexpected medical issues.1 </div>
							</div>
						</div>
					</div>										
				</div>
				<div class="col-md-8 col-md-offset-2 text-center">
					<span class="next-pre-button-in">
						<a class="carousel-control01 next-button-in" href="#carousel-example-generic01" role="button" data-slide="next">
							&gt;
						</a>
						<a class="carousel-control01 prev-button-in" href="#carousel-example-generic01" role="button" data-slide="prev">
							&lt;
						</a>
					</span>
				</div>
			</div>	
		</div>
	</section>
	<div class="text-center down-arrow">
		<img src="<?php echo base_url('assets/img/inner-img/down-arrow.png')?>" alt="" />
	</div>
	
	<section class="references-section">
		<div class="container">
			<div class="col-md-12">
				<span class="more-plus">
					<a href="" class="">References
						<span class="plus"><i class="fa fa-plus"></i></span>
					</a>
				</span>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="male-female_Trauma text-center">
						Leading Causes of TAL Trauma <br>Claims from 2010 – 2014  (Male/Female)
					</div>
					<div class="male-img">
						<img src="<?php echo base_url('assets/img/inner-img/Trauma_percent.png')?>" class="img-responsive" alt="" />
					</div>
				</div>
				<!-- Case Studies Slider -->
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
					<div class="real-title-inr">
						Case Studies<br><strong>HOW TRAUMA INSURANCE HELPS</strong>
					</div>
					<div id="carousel-example-generic-t" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
									<div class="case-studies">
										<div class="tom-title">
											<img src="<?php echo base_url('assets/img/inner-img/Trauma_tM.png')?>" />
											TOM DEVELOPS SKIN CANCER
											<div class="top-content">
											<i>Tom, a 42 year old self-employed tradesman, noticed a spot on his left leg that would not heal.  His doctor r..</i>
											</div>
											<div class="full-story"><a href="">Full Story</a></div>
											<div>
												<span class="next-pre-button-in-dummy">
													<a href="" class="next-button-in-dummy">&nbsp;</a>
													<a href="" class="prev-button-in-dummy">&nbsp;</a>
												</span>
											</div>
										</div>
										<!-- <span><img src="assets/img/inner-img/i.png" alt="test" /></span> -->
									</div>
									<div class="case-arrow">
										<img src="<?php echo base_url('assets/img/inner-img/case-arrow.png')?>" alt="test" />
									</div>
									<div class="case-name">
										TOM (42)<br><span>Builder</span>
									</div>
								</div>
							</div>					
							<div class="item">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
									<div class="case-studies">
										<div class="tom-title">
											<img src="<?php echo base_url('assets/img/inner-img/Trauma_tM.png')?>" />
											MARY SUFFERS A SERIES OF STROKES
											<div class="top-content">
											Mary suffered a stroke as a result of a blood clot becoming lodged in a vein inside her brain. She spent... 
											</div>
											<div class="full-story">Full Story</div>
											<div>
												<span class="next-pre-button-in-dummy">
													<a href="" class="next-button-in-dummy">&nbsp;</a>
													<a href="" class="prev-button-in-dummy">&nbsp;</a>
												</span>
											</div>
										</div>
										<!-- <span><img src="assets/img/inner-img/i.png" alt="test" /></span> -->
									</div>
									<div class="case-arrow">
										<img src="<?php echo base_url('assets/img/inner-img/case-arrow.png')?>" alt="test" />
									</div>
									<div class="case-name">
										MARY (37)<br><span>, Nurse</span>
									</div>
								</div>
							</div>										
						</div>
						<div class="col-md-6 col-md-offset-2 text-center">
							<span class="next-pre-button-in-t">
								<a class="carousel-control01 next-button-in" href="#carousel-example-generic-t" role="button" data-slide="next">
									&gt;
								</a>
								<a class="carousel-control01 prev-button-in" href="#carousel-example-generic-t" role="button" data-slide="prev">
									&lt;
								</a>
							</span>
						</div>
					</div>
				</div>	
				<!-- Case Studies Slider -->
			</div>
			<div class="row top-space02">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="renni-inner">
						<span class="protection-icon">
							<img src="<?php echo base_url('assets/img/inner-img/Icome_Icon01.png')?>" alt="test" />
						</span>
						<span class="protection-text">RENNI LIFE INSURANCE</span><br>
						Protect your family when they need you most. Apply for life insurance today. 
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="renni-inner">
						<span class="protection-icon">
							<img src="<?php echo base_url('assets/img/inner-img/Trauma_Icon01.png')?>" alt="test" />
						</span>
						<span class="protection-text">SAVE ON YOUR TRAUMA POLICY</span><br>
						Take out Renni trauma cover and receive a 10% discount on your first year’s premium. 
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!-- section 06 -->
    <section class="section-06-inr">
        <div class="container-fluid">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
				<div class="first-box01_Icome">
					<div class="tool-title-inr">
						<span>Have you considered?</span><br>
						Renni Facts & Figures
					</div>
					<div class="border-white02"></div>
					<div class="tool-content-inr_Icome">
						Most of us don’t consider the reason why we can’t work until it’s too late. The high cost of treating a traumatic injury or serious illness can quickly eat into your savings and threaten your financial well-being. 
					</div>
					<div class="discover-button">
					   <a href="">DISCOVER</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
				<div class="first-box02_Icome">
					<div class="tool-title-inr">
						<span>How can trauma cover work for you?</span><br>
						RENNI CASE STUDIES
					</div>
					<div class="border-white02"></div>
					<div class="tool-content-inr_Icome">
						Unsure about trauma insurance? Review our Renni Case Studies for more examples of how trauma cover can work for you. 
					</div>
					<div class="discover-button">
					   <a href="<?php echo site_url('Casestudies') ?>">LEARN MORE</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
				<div class="first-box03_Icome">
					<div class="tool-title-inr">
						<span>Where do your needs fit?</span><br>
						TOOLS & CALCULATORS
					</div>
					<div class="border-white02"></div>
					<div class="tool-content-inr_Icome">
						You can’t afford to be covered for everything. If you’re unsure about what you need take some time with our tools and calculators.
					</div>
					<div class="discover-button">
					   <a href="">EXPLORE</a>
					</div>
				</div>
			</div>
        </div>    
    </section>
	<!-- //////////// section 06 //////////// -->
	
	<!-- section 05 -->
	<section class="section-05">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="real-title"><strong>RENNI FAQ’s. TRAUMA INSURANCE</strong>
						<div class="border-blue"></div>
					</div>
				</div>
			</div>
			<div class="row panel-group">
				<div class="col-md-6">
					<!-- Tabs --> 
					<div class="form-tabs" id="accordion-f" role="tablist" aria-multiselectable="true">
                                            <?php get_faq_by_insurance(6); ?>						
					</div>
					<!-- Tabs ////////// -->
					<div class="more-button">
						<a href="<?php echo site_url('faq') ?>">More FAQ‘s &nbsp; <img src="<?php echo base_url('assets/img/more-arrow.png')?>" alt="" /></a>
					</div>
				</div>
				<div class="col-md-6">
					<?php $this->load->view('ask_form'); ?>			
				</div>
			</div>
			<div class="row">				
				<div id="carousel-example-generic03" class="carousel slide" data-ride="carousel">	
					<div class="faqs">
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="did-you">
								<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
									<div class="did-title">Did you know?</div>
									<div class="getsefl-inr">A trauma insurance payout can be used to meet your specific circumstances.  You can also insure your entire family, which lets you take care of them without worrying about all the bills!
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="get-inr"><a href="">GET INSURED</a></div>
								</div>
							</div>
						</div>					
						<div class="item">
							<div class="did-you">
								<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
									<div class="did-title">Did you know?</div>
									<div class="getsefl-inr">A trauma insurance payout can be used to meet your specific circumstances.  You can also insure your entire family, which lets you take care of them without worrying about all the bills!
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="get-inr"><a href="">GET INSURED</a></div>
								</div>
							</div>
						</div>										
						</div>										
					</div>					
					<div class="col-md-12 clearfix">
						<span class="next-pre-button-in-03">
							<a class="carousel-control01 next-button-in-03" href="#carousel-example-generic03" role="button" data-slide="next">
								&gt;
							</a>
							<a class="carousel-control01 prev-button-in-03" href="#carousel-example-generic03" role="button" data-slide="prev">
								&lt;
							</a>
						</span>
					</div>				
				</div>
			</div>
		</div>
	</section>
	<!-- //////////// section 05 //////////// -->
	
	<!-- //////////// section 05 //////////// -->
	<?php $this->load->view('casestudies_news_4more');  ?>
	<!-- //////////// section 05 //////////// -->
	<section class="logo-section">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
					<?php $this->load->view('all_logo');  ?>
				</div>
			</div>
		</div>
	</section>
	
	
    <!-- section 08 -->
    <section class="section-08-iner">
    	<div class="section-img-inner">
			<div class="container">
				<div class="row">            
					<div class="col-lg-7 col-sm-6 discover-t_not">Not sure about something? </div>
					<div class="col-lg-5 col-sm-6 two-button-inr text-right">
						<a href="javascript:void(0);" class="button-request-inr">REQUEST ASSISTANCE</a>
					</div>
				</div>    
			</div>
        </div>
    </section> 
    <!-- //////////// section 08 //////////// -->
    