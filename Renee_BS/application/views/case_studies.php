<!-- Banner Part -->
<section class="case_top-section">
    <div class="container">			
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <div class="banner-text-inner02">
                    <div class="take-to">
                        Everyday examples of HOW
                    </div>
                    <div class="today_icome">
                        <span>insurance</span> Can work for you.
                    </div>
                    <div class="request-now-case">
                        <a href="<?php echo site_url('contactus') ?>">REQUEST A QUOTE</a>
                    </div>
                </div>					
            </div>
        </div> 
    </div>
</section>
<!-- //////////// Banner Part //////////// -->

<style>
    .activtabfq{
        color: #21c2f8 !important;
    }
</style>

<!-- section 03 REAL LIFE STORIES-->
<section class="case_section">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-xs-12">
                <span class="personal_Title">Personal Insurance</span>
                <div class="burgger_menu_div">
                    <a onclick="$('#allinsurance').toggle('show');" href="javascript:void(0);">
                        <img src="<?php echo base_url('assets/img/menu-img.png') ?>" alt="" />
                    </a>
                </div>
                <div id="allinsurance" class="all_insurance">						
                    <ul>
                        <li class="per_ins">Personal Insurance</li>
                        <?php
                        $this->load->helper('get_menu_data');
                        $children=getchildInsurence(20);
                        foreach ($children as $child): ?>
                        <li><a <?php echo faq_selected_subtab('activtabfq',$child->ins_id,$this->uri->segment(3));?> href="<?php echo site_url('Casestudies/index/'.$child->ins_id) ?>"><?php echo $child->ins_name;?></a></li>
                       <?php
                       endforeach;
                        ?>
                        
                    </ul>
                </div>
            </div>
            <div class="col-md-7 col-xs-12">
                <div class="tab_case">						
                    <ul class="nav nav-tabs">
                        <li <?php echo faq_selected_subtab('active','',$this->uri->segment(3));?>><a href="<?php echo site_url('Casestudies/') ?>" >All</a></li>
                        <li <?php echo faq_selected_subtab('active',2,$this->uri->segment(3));?>><a href="<?php echo site_url('Casestudies/index/2') ?>" >Life</a></li>
                        <li <?php echo faq_selected_subtab('active',4,$this->uri->segment(3));?>><a href="<?php echo site_url('Casestudies/index/4') ?>" >Disability</a></li>
                        <li <?php echo faq_selected_subtab('active',5,$this->uri->segment(3));?>><a href="<?php echo site_url('Casestudies/index/5') ?>" >Income</a></li>
                        <li <?php echo faq_selected_subtab('active',6,$this->uri->segment(3));?>><a href="<?php echo site_url('Casestudies/index/6') ?>" >Trauma</a></li>
                        

                        <!--li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                          Dropdown <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                          <li><a href="#dropdown1" data-toggle="tab">Action</a></li>
                          <li class="divider"></li>
                          <li><a href="#dropdown2" data-toggle="tab">Another action</a></li>
                        </ul>
                  </li-->
                    </ul>
                </div>
            </div>
        </div>
        <style>
            .custom-responsive{
                max-height: 310px;
                width: 100%;
            }
        </style>


        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade active in">
                <?php
                if(empty($result)){
                    echo 'No record found.';
                    
                }else{
                $row = 0;
                foreach ($result as $res):
                    if ($row % 2 == 0) {
                        $column = 0;
                        if ($row == 0) {
                            echo '<div class="row">';
                        } else {
                            echo '<div class="row case_space">';
                        }
                    }
                    ?>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
                        <div class="case_studies_div">
                            <div class="real-title-inr">   
                                <?php if ($res->imageurl != '') { ?>
                                    <img src="<?php echo base_url(); ?>/uploads/casestudies/<?php echo $res->imageurl; ?>" class="img-responsive custom-responsive">
                                <?php } ?>
                            </div>
                            <div class="button_over">                                
                                <?php
                                if($res->tags!=''){
                                $tags = explode(',', $res->tags);
                                foreach ($tags as $tag):
                                    echo '<a href="">' . $tag . '</a>';
                                endforeach;
                                }
                                ?>                                
                            </div>
                            <div class="Video_CNT">
                                <span><?php echo $res->title; ?></span>
                                <div class="Date_TPD">
                                    <?php echo $res->sub_title_short; ?> 
                                </div>
                            </div>
                            <div class="tab-content01">
                                <?php echo $res->sub_title_long; ?>
                            </div>
                            <div class="more_case">
                                <a href="<?php echo site_url('Casestudies/details/'.$res->rlid) ?>">Raed More</a>
                            </div>
                        </div>
                    </div>

                    <?php
                    $column++;
                    if ($column == 2) {
                        echo '</div>';
                    }
                    // print_r($resul);
                    $row++;
                endforeach;
                if ($column == 1) {
                    echo '</div>';
                }
                }
                ?>             
                
            </div>
<!--        <div class="row case_space">
            <div class="col-sm-12">
                <div class="form-group load_more">
                    <button class="submit_button">LOAD MORE</button>
                </div>
            </div>
        </div>-->
    </div>
</section>
<!-- //////////// section 03 //////////// -->	

<!-- //////////// section 05 //////////// -->
<section class="logo-section" style="border-top: 1px solid #eee;">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <?php $this->load->view('all_logo'); ?>
            </div>
        </div>
    </div>
</section>