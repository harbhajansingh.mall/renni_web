<!DOCTYPE html>
<html lang="en">
    <head>    
        <title>Renni :: <?php echo (isset($pagetitle)) ? $pagetitle : 'Renni'; ?></title> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">    

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url('css/public/bootstrap.min.css') ?>" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?php echo base_url('css/public/custom.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/public/inner_css.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/public/media-query.css') ?>" rel="stylesheet">
        <!-- jQuery -->
        <script src="<?php echo base_url('js/jquery.js') ?>"></script>
        <!-- Custom Fonts -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <style>
            .activetab{
                color:#333 !important;
            }
        </style>

    </head>


    <body>
        <?php $this->load->helper('tabSelect'); ?>
        <div id="youtubelightbox" class="popup">
            <div class="content1">
                <img src="<?php echo base_url('assets/img/close.png') ?>" alt="quit" class="x" id="x" />
                <div id="player"></div>
                <!-- <p><a href='' class='close'>Close</a></p> -->
            </div>
        </div>

        <!-- Header Part -->
        <section class="inner-section-h">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 no-padding">
                        <div class="logo">
                            <a href="<?php echo site_url() ?>"><img src="<?php echo base_url('assets/img/logo.png') ?>" alt="" /></a>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 no-padding text-right">
                        <div class="call-us">
                            <span class="why-choose">About Renni ? &nbsp; I</span>
                            <span class="number"><span class="call-title">CALL</span> 1300 481 481</span>
                        </div>
                    </div>
                </div>      
            </div>      
        </section>          
        <!-- //////////// Header Part //////////// -->
        <!-- Menu Bar -->
        <!-- Header Part -->
        <section class="menu-bar">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-8 no-padding">
                        <div class="life-title"><?php echo $pagetitle; ?></div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 text-right no-padding">                    
                        <nav class="navbar navbar-inverse">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="collapse navbar-collapse" id="myNavbar">
                                <div class="menubr" id="top-nav">   
                                    <ul class="nav navbar-nav navbar-right">   
                                        <li class="dropdown"><a <?php echo front_selected_tab('activetab', ['lifeinsurance', 'traumainsurance', 'disabilityinsurance', 'incomeprotection'], $this->uri->segment(1)); ?> href="#">Insurances <i class="fa fa-angle-down Dropdown_arrow_ico" aria-hidden="true"></i></a>
                                            <ul class="dropdown-menu">

                                                <li class="dropdown"><a href="#">Personal Insurance<i class="fa fa-angle-right Right_arrow_ico" aria-hidden="true"></i></a>
                                                    <ul class="dropdown-menu">                                                                
                                                        <li><a href="<?php echo site_url('Lifeinsurance') ?>">Life Insurance</a></li>
                                                        <li><a href="<?php echo site_url('Traumainsurance') ?>">Trauma Insurance</a></li>
                                                        <li><a href="<?php echo site_url('Disabilityinsurance') ?>">Permanent Disability Insurance</a></li>
                                                        <li><a href="<?php echo site_url('Incomeprotection') ?>">Income Protection</a></li>
                                                    </ul>
                                                </li>

                                                <!--                                                <li><a href="#">Travel Insurance</a></li>
                                                                                                <li><a href="#">Asteroid Insurance</a></li>
                                                                                                <li><a href="#">Insurance Category</a></li>
                                                                                                <li><a href="#">Insurance Category</a></li>
                                                                                                <li><a href="#">Product disclosures</a></li>
                                                                                                <li><a href="#">Underwriters</a></li>-->
                                            </ul>
                                        </li>
                                        <li><a <?php echo front_selected_tab('activetab', ['casestudies'], $this->uri->segment(1)); ?> href="<?php echo site_url('Casestudies') ?>">Case Studies</a></li>
                                        <li><a <?php echo front_selected_tab('activetab', ['faq'], $this->uri->segment(1)); ?> href="<?php echo site_url('faq') ?>">FAQ’s</a></li>
                                        <li><a <?php echo front_selected_tab('activetab', ['blogs'], $this->uri->segment(1)); ?> href="<?php echo site_url('blogs') ?>">Blogs</a></li>
                                        <li><a <?php echo front_selected_tab('activetab', ['contactus'], $this->uri->segment(1)); ?> href="<?php echo site_url('contactus') ?>">Contact Us</a></li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>              
                </div>      
            </div>      
        </section>
        <!-- Menu Bar Ends -->


