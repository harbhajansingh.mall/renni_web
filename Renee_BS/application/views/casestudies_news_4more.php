<section class="section-services">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
						<div class="round-life">
							<img src="<?php echo base_url('assets/img/inner-img/s-img01.png')?>" />
						</div>
					</div>
					<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
						<div class="case-inr"><span><a href="<?php echo site_url('Casestudies') ?>">CASE STUDIES</a></span><br>
						Phasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam. Aliquam in tortor enim. Blandit vel sapien vitae, 
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
						<div class="round-life">
							<img src="<?php echo base_url('assets/img/inner-img/s-img02.png')?>" />
						</div>
					</div>
					<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
						<div class="case-inr"><span><a href="<?php echo site_url('Blogs') ?>">NEWS</a></span><br>
						Phasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam. Aliquam in tortor enim. Blandit vel sapien vitae, 
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
						<div class="round-life">
							<img src="<?php echo base_url('assets/img/inner-img/s-img03.png')?>" />
						</div>
					</div>
					<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
						<div class="case-inr"><span><a href="">PRODUCT DISCLOSURE</a></span><br>
						Phasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam. Aliquam in tortor enim. Blandit vel sapien vitae, 
						</div>
					</div>
				</div>
			</div>
			<div class="row services-space">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
						<div class="round-life">
							<img src="<?php echo base_url('assets/img/inner-img/s-img04.png')?>" />
						</div>
					</div>
					<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
						<div class="case-inr"><span><a href="">UNDERWRITERS</a></span><br>
						Phasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam. Aliquam in tortor enim. Blandit vel sapien vitae, 
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
						<div class="round-life">
							<img src="<?php echo base_url('assets/img/inner-img/s-img05.png')?>" />
						</div>
					</div>
					<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
						<div class="case-inr"><span><a href="">PRIVACY</a></span><br>
						Phasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam. Aliquam in tortor enim. Blandit vel sapien vitae, 
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
						<div class="round-life">
							<img src="<?php echo base_url('assets/img/inner-img/s-img06.png')?>" />
						</div>
					</div>
					<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
						<div class="case-inr"><span><a href="<?php echo site_url('contactus') ?>">CONTACT US</a></span><br>
						Phasellus enim libero, blandit vel sapien vitae, condimentum ultricies magna et. Quisque euismod orci ut et lobortis aliquam. Aliquam in tortor enim. Blandit vel sapien vitae, 
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>