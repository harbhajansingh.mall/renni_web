<ul class="life_insurance">
	<li>
		<a href="<?php echo site_url('Lifeinsurance')?>" <?php echo faq_selected_subtab('active','Lifeinsurance',$this->uri->segment(1));?>><i class="round-icon"><img src="<?php echo base_url('assets/img/inner-img/life0.png')?>" alt="" /></i> LiFE Insurance <i class="arrow-left"> > </i></a>
	</li><li>
		<a href="<?php echo site_url('Disabilityinsurance')?>" <?php echo faq_selected_subtab('active','Disabilityinsurance',$this->uri->segment(1));?>><i class="round-icon"><img src="<?php echo base_url('assets/img/inner-img/life01.png')?>" alt="" /></i> Disability Insurance <i class="arrow-left"> > </i></a>
	</li><li>
		<a href="<?php echo site_url('Incomeprotection')?>" <?php echo faq_selected_subtab('active','Incomeprotection',$this->uri->segment(1));?>><i class="round-icon"><img src="<?php echo base_url('assets/img/inner-img/life02.png')?>" alt="" /></i> Income Protection <i class="arrow-left"> > </i></a>
	</li><li>
		<a href="<?php echo site_url('Traumainsurance')?>" <?php echo faq_selected_subtab('active','Traumainsurance',$this->uri->segment(1));?>><i class="round-icon"><img src="<?php echo base_url('assets/img/inner-img/life03.png')?>" alt="" /></i> Trauma Protection <i class="arrow-left"> > </i></a>
	</li>
</ul>