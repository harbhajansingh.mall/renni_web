<div class="form-title">
    <form method="post" id="questionf">
        <input required="required" type="text" placeholder="Name *" name="name" class="input-box" />
        <input required="required" type="email" placeholder="Email*" name="email" class="input-box" />
        <input required="required" type="text" placeholder="Phone number*" name="phonenumber" class="input-box" />
        <textarea required="required" name="question" placeholder="Your question*" class="textarea-box"></textarea>
        <input type="submit" value="ASK A QUESTION" class="button-box" />
        <div id="questiondv" style="font-size: 14px;color: #21c2f8;font-weight: 600;width: 100%;padding-top: 10px;"></div>
        <span class="privacy-policy">
            Our Privacy Policy.
        </span>
    </form>
</div>
<script>
    $(document).ready(function () {
        $('#questionf').submit(function () {
            $.ajax({
                url: '<?php echo base_url(); ?>index.php/Home/save_question',
                data: $(this).serialize(),
                type: 'post',
                success: function (data) {
                    $('#questiondv').html(data);
                }
            });
            $(this)[0].reset();
            return false;
        });
    });
</script>