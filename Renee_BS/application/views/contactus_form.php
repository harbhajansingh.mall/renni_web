 <!-- Request a Demo -->
            <div class="item active">
                <!-- Show Content Section -->
                <div id="contact_form" class="">
                    <div class="contact_form">
                        <form method="post" id="contactus1">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>NAME *</label>
                                        <input type="text" class="form-control" name="name" required="yes"> 
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>EMAIL *</label>
                                        <input type="email" class="form-control" name="email" required="yes"> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>PHONE NUMBER *</label>
                                        <input type="text" class="form-control" name="phone" required="yes"> 
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>BEST TIME TO CONTACT YOU *</label>
                                        <input type="text" class="form-control" name="best_time" required="yes"> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>INSURANCE *</label>
                                        <select class="form-control" required="yes" name="insurance">
                                            <option value="">Select insurance</option>
                                            <option value="LIFE INSURANCE">LIFE INSURANCE</option>
                                            <option value="DISABILITY INSURANCE">DISABILITY INSURANCE</option>
                                            <option value="INCOME PROTECTION">INCOME PROTECTION</option>
                                            <option value="TRAUMA PROTECTION">TRAUMA PROTECTION</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>SUBJECT *</label>
                                        <input type="text" class="form-control" name="subject" required="yes">   
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>What would you like more information about?</label> 
                                        <textarea class="form-control" name="query"></textarea>   
                                        <em class="option_c">Disclaimer: Renni provides general insurance and policy information only. We are unable to provide personal advice. For more information please refer our FAQ's.</em> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <button type="submit" class="submit_button">SUBMIT NOW</button>
                                    </div>
                                    <div id="contacusdv1" style="font-size: 14px;color: #21c2f8;font-weight: 600;width: 100%;padding-top: 10px;"></div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>          
            </div>

            <!-- Customer Support -->
            <div class="item">
                <!-- Show Content Section -->
                <div id="contact_form" class="">
                    <div class="contact_form">
                        <form method="post" id="contactus2">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>NAME *</label>
                                        <input type="text" class="form-control" name="name" required="yes"> 
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>EMAIL *</label>
                                        <input type="email" class="form-control" name="email" required="yes"> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>PHONE NUMBER *</label>
                                        <input type="text" class="form-control" name="phone" required="yes"> 
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>BEST TIME TO CONTACT YOU *</label>
                                        <input type="text" class="form-control" name="best_time" required="yes"> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>INSURANCE *</label>
                                        <select class="form-control" required="yes" name="insurance">
                                            <option value="">Select insurance</option>
                                            <option value="LIFE INSURANCE">LIFE INSURANCE</option>
                                            <option value="DISABILITY INSURANCE">DISABILITY INSURANCE</option>
                                            <option value="INCOME PROTECTION">INCOME PROTECTION</option>
                                            <option value="TRAUMA PROTECTION">TRAUMA PROTECTION</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>SUBJECT *</label>
                                        <input type="text" class="form-control" name="subject" required="yes">   
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>What would you like more information about?</label> 
                                        <textarea class="form-control" name="query"></textarea>   
                                        <em class="option_c">Disclaimer: Renni provides general insurance and policy information only. We are unable to provide personal advice. For more information please refer our FAQ's.</em> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <button type="submit" class="submit_button">SUBMIT NOW</button>
                                    </div>
                                    <div id="contacusdv2" style="font-size: 14px;color: #21c2f8;font-weight: 600;width: 100%;padding-top: 10px;"></div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>          
            </div>
            <!-- Request A Quote -->
            <div class="item">
                <!-- Show Content Section -->
                <div id="contact_form" class="">
                    <div class="contact_form">
                        <form method="post" id="contactus3">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>NAME *</label>
                                        <input type="text" class="form-control" name="name" required="yes"> 
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>EMAIL *</label>
                                        <input type="email" class="form-control" name="email" required="yes"> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>PHONE NUMBER *</label>
                                        <input type="text" class="form-control" name="phone" required="yes"> 
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>BEST TIME TO CONTACT YOU *</label>
                                        <input type="text" class="form-control" name="best_time" required="yes"> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>INSURANCE *</label>
                                        <select class="form-control" required="yes" name="insurance">
                                            <option value="">Select insurance</option>
                                            <option value="LIFE INSURANCE">LIFE INSURANCE</option>
                                            <option value="DISABILITY INSURANCE">DISABILITY INSURANCE</option>
                                            <option value="INCOME PROTECTION">INCOME PROTECTION</option>
                                            <option value="TRAUMA PROTECTION">TRAUMA PROTECTION</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>SUBJECT *</label>
                                        <input type="text" class="form-control" name="subject" required="yes">   
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>What would you like more information about?</label> 
                                        <textarea class="form-control" name="query"></textarea>   
                                        <em class="option_c">Disclaimer: Renni provides general insurance and policy information only. We are unable to provide personal advice. For more information please refer our FAQ's.</em> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <button type="submit" class="submit_button">SUBMIT NOW</button>
                                    </div>
                                    <div id="contacusdv3" style="font-size: 14px;color: #21c2f8;font-weight: 600;width: 100%;padding-top: 10px;"></div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>          
            </div>
            <script>
    $(document).ready(function () {
        $('#contactus1').submit(function () {
           
            $.ajax({
                url: '<?php echo base_url(); ?>index.php/Contactus/save_contactus',
                data: $(this).serialize(),
                type: 'post',
                success: function (data) {
                    $('#contacusdv1').html(data);
                }
            });
            $(this)[0].reset();
            return false;
        });
        $('#contactus2').submit(function () {
        
            $.ajax({
                url: '<?php echo base_url(); ?>index.php/Contactus/save_contactus',
                data: $(this).serialize(),
                type: 'post',
                success: function (data) {
                    $('#contacusdv2').html(data);
                }
            });
            $(this)[0].reset();
            return false;
        });
        $('#contactus3').submit(function () {
        
            $.ajax({
                url: '<?php echo base_url(); ?>index.php/Contactus/save_contactus',
                data: $(this).serialize(),
                type: 'post',
                success: function (data) {
                    $('#contacusdv3').html(data);
                }
            });
            $(this)[0].reset();
            return false;
        });
    });
</script>