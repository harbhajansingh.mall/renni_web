<!-- section 05 -->
<section class="section_news">
    <div class="container">			
        <div class="row">
            <div class="col-md-3">
                <div class="col-md-12 no-padding">
                    <div class="Left_Bar_Sec">
                        <div class="Search_news">
                            <input type="" name="" placeholder="Keywords typing..." class="new_search">
                            <span><i class="fa fa-search" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
                <style>
                    .Drop_Down li a.activtabfq{
                        color: #21c2f8 !important;
                    }
                </style>
                <div class="col-md-12 no-padding">
                    <div class="__All_ins">
                        <span class="yellow"><img src="<?php echo base_url('assets/img/inner-img/yellow_blue.jpg'); ?>" alt=""></span>
                        Insurance Categories <span><a href="<?php echo site_url('blogs/details/' . $this->uri->segment(3)) ?>">Show All</a></span>
                    </div>
                    <div class="show_all">
                        <ul class="panel-group" id="accordion">
                            <li class="panel panel-default"><a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">General Insurance
                                    <span><i class="fa fa-angle-down" aria-hidden="true"></i></span>	
                                </a>
                                <ul id="collapseOne" class="Drop_Down panel-collapse collapse in">
                                    <?php
                                    $this->load->helper('get_menu_data');
                                    $children = getchildInsurence(20);
                                    foreach ($children as $child):
                                        ?>
                                        <li><a <?php echo faq_selected_subtab('activtabfq', $child->ins_id, $this->uri->segment(4)); ?> href="<?php echo site_url('blogs/details/' . $this->uri->segment(3) . '/' . $child->ins_id) ?>"><?php echo $child->ins_name; ?></a></li>
                                        <?php
                                    endforeach;
                                    ?>
                                </ul>
                            </li>


                        </ul>
                    </div>
                    <div class="__All_ins Search_TAgs">
                        <span class="yellow"><img src="<?php echo base_url('assets/img/inner-img/yellow_blue.jpg'); ?>" alt=""></span>
                        Search Tags
                    </div>
                    <div class="__Tags">
                        <ul>
                            <li><a href="">Cruise</a></li>
                            <li><a href="">Tag</a></li>
                            <li><a href="">Domestic</a></li>
                        </ul>
                    </div>
                    <div class="__Tags __Tags_space">
                        <ul>
                            <li><a href="">Tag</a></li>
                            <li><a href="">Li</a></li>
                            <li><a href="">TDP</a></li>
                            <li><a href="">International</a></li>
                        </ul>
                    </div>
                    <div class="__Tags __Tags_space">
                        <ul>
                            <li><a href="">Income</a></li>
                            <li><a href="">Trauma</a></li>
                            <li><a href="">Some Tag</a></li>
                        </ul>
                    </div>
                    <div class="__Tags __Tags_space">
                        <ul>
                            <li><a href="">Sum</a></li>
                            <li><a href="">Corporate</a></li>
                            <li><a href="">Cover</a></li>
                        </ul>
                    </div>
                    <div class="__All_ins Search_TAgs">
                        <span class="yellow"><img src="<?php echo base_url('assets/img/inner-img/yellow_blue.jpg'); ?>" alt=""></span>
                        Archives
                    </div>
                    <div class="__Archives">
                        <ul>
                            <li><a href="">March 2017 <span>9</span></a></li>
                            <li><a href="">February 2017 <span>14</span></a></li>
                            <li><a href="">January 2017 <span>15</span></a></li>
                            <li><a href="">December 2016 <span>23</span></a></li>
                        </ul>
                    </div>
                    <div class="__All_ins">
                        <span class="yellow"><img src="<?php echo base_url('assets/img/inner-img/yellow_blue.jpg'); ?>" alt=""></span>
                        Share This
                    </div>
                    <div class="__Archives_icon">
                        <a href="" id="facebook">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="" id="twitter">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="" id="linkedin">
                            <i class="fa fa-linkedin"></i>
                        </a>
                        <a href="" id="google">
                            <i class="fa fa-google-plus"></i>
                        </a>
                    </div>
                    <div class="__All_ins Search_TAgs">
                        <span class="yellow"><img src="<?php echo base_url('assets/img/inner-img/yellow_blue.jpg'); ?>" alt=""></span>
                        Highlights
                    </div>
                    <div class="__Highlights">
                        <ul>
                            <li>
                                <span>June 6, 2012</span>
                                <div class="__Highlights_date">Be prepared for the worst Sydney Morning Herald</div>
                            </li>
                            <li>
                                <span>October 24, 2012</span>
                                <div class="__Highlights_date">Home Put at Risk by Income Protection Gap risk info</div>
                            </li>
                            <li>
                                <span>October 9, 2014</span>
                                <div class="__Highlights_date">Shock slug for cancer patients loreum bibdenum geginum fefitum butinum gogoenimun </div>
                            </li>
                            <li>
                                <span>October 24, 2012</span>
                                <div class="__Highlights_date">Home Put at Risk by Income Protection Gap risk info-- This is how it will appear if the new article is without the image</div>
                            </li>
                            <li>
                                <span>June 6, 2012</span>
                                <div class="__Highlights_date">Be prepared for the worst Sydney Morning Herald----- this one is without the image</div>
                            </li>
                        </ul>
                    </div>
                    <div class="__All_ins">
                        <span class="yellow"><img src="<?php echo base_url('assets/img/inner-img/yellow_blue.jpg'); ?>" alt=""></span>
                        Quick Suggestion
                    </div>
                    <div class="__Suggestion">
                        <form>
                            <input type="text" name="" value="Your Name">
                            <textarea placeholder="Found something you think we should post? Type here the link with a quick comment"></textarea>
                            <button class="send__Button">SEND TO RENNI</button>
                        </form>
                    </div>
                    <div class="__All_ins">
                        <span class="yellow"><img src="<?php echo base_url('assets/img/inner-img/yellow_blue.jpg'); ?>" alt=""></span>
                        Renni Blog
                    </div>
                    <div class="__Suggestion__blog">
                        <div class="__blog__inner01">
                            <div class="Charlotte_Olivia"> Charlotte Olivia</div>		
                            <div class="__read_more"><a href="#">Read</a></div>
                        </div>
                    </div>
                    <div class="__Rectangle_1">
                        <div class="__blog__inner">
                            <div class="insurance_Worth"> Is insurance Worth it?<br><span>Dec. 12, 2018</span></div>
                            <div class="__Under_labour">Under labour he cost of care has stabilized. Is persnal insurance still worth it?</div>
                            <div class="__read_more"><a href="#">Read</a></div>
                        </div>
                    </div> 
                </div>			
            </div>

            <div class="col-lg-9">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">

                        <?php
                        if (empty($result)) {
                            echo '<div class="item active"><div class="row">
                                            <div class="col-md-12 first_News_Sec"><div class="__It_Costs">';
                            echo 'No record found.';
                            echo '</div></div></div></div>';
                        } else {
                            foreach ($result as $res):
                                ?>
                                <?php if ($res->nb_id == $this->uri->segment(3)) { ?>
                                    <div class="item active">
                                    <?php } else { ?>
                                        <div class="item">
                                        <?php } ?>

                                        <div class="row">
                                            <div class="col-md-12 first_News_Sec">
                                                <div class="__It_Costs"><?php echo $res->title; ?></div>
                                                <div class="__new_date_time">By <?php echo $res->author_id; ?> &nbsp; | &nbsp; <?php echo date('F j, Y', strtotime($res->nb_date)); ?> &nbsp;</div>
                                                <div class="__news_content"><?php echo $res->news_reference; ?></div>
                                                <div class="__News02_Content">
                                                    <?php echo $res->details; ?>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="tag_Name">
                                            <?php
                                            if ($res->tags != '') {
                                                $tags = explode(',', $res->tags);
                                                foreach ($tags as $tag):
                                                    echo '<a href="">' . $tag . '</a>';
                                                endforeach;
                                            }
                                            ?>  

                                        </div>

                                    </div>
                                    <?php
                                endforeach;
                            }
                            ?>

                        </div>


                        <div class="border_button_case">
                            <!-- Left and right controls -->
                            <a class="left carousel-control previously_button02" href="#myCarousel" data-slide="prev">Previous
                            </a>
                            <a class="right carousel-control previously_button02" href="#myCarousel" data-slide="next">Next
                            </a>
                        </div>

                    </div>	

                </div>

            </div>
        </div>
</section>
<!-- //////////// section 05 //////////// -->



<!-- //////////// section 05 //////////// -->
<section class="logo-section">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <?php $this->load->view('all_logo'); ?>
            </div>
        </div>
    </div>
</section>


<!-- section 08 -->
<section class="section-08-iner">
    <div class="section-img-inner">
        <div class="container">
            <div class="row">            
                <div class="col-lg-7 col-sm-6 discover-t_not">Not sure about something? </div>
                <div class="col-lg-5 col-sm-6 two-button-inr text-right">
                    <a href="javascript:void(0);" class="button-request-inr">REQUEST ASSISTANCE</a>
                </div>
            </div>    
        </div>
    </div>
</section> 
<!-- //////////// section 08 //////////// -->