<!-- Banner Part -->
<section class="inner-top-section">
    <div class="container">			
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <div class="banner-text-inner">
                    <div class="take-to">
                        TAKE THE PATH TO
                    </div>
                    <div class="today">
                        PROTECTION <span>TODAY.</span>
                    </div>
                    <div class="request-now">
                        <a href="<?php echo site_url('contactus') ?>">REQUEST Now</a>
                    </div>
                </div>					
            </div>
        </div> 
    </div>
</section>
<!-- //////////// Banner Part //////////// -->

<!-- Section 02 -->
<section class="section-02">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                <?php $this->load->view('left_panel_inner_ins'); ?>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                <div class="real-title01">What is Life Insurance and why do you need it?</div>
                <div class="border-blueinr"></div>
                <div class="tab-content">Life insurance is a lump sum paid your family or loves ones should you die unexpectedly. It helps protect your family’s future by assisting them financially when you are no longer around. Life insurance doesn’t make up for you not being there for your loved ones but it’s the next best thing – your family will thank you for it. </div>
            </div>
        </div>
        <div class="row top-space">
            <div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">
                <div class="watch"><img src="<?php echo base_url('assets/img/inner-img/watch.png') ?>" alt="" /></div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-7 col-xs-12">
                <div class="real-title01">How does Life Insurance help?</div>
                <div class="border-blueinr"></div>
                <div class="tab-content01">The money paid to your family or estate will help them:</div>
                <div class="icn-space">
                    <ul class="banner-list">
                        <li><i class="fa fa-check-circle" aria-hidden="true"></i>Pay off existing debts (such as mortgages and personal loans)</li>
                        <li><i class="fa fa-check-circle" aria-hidden="true"></i>Cover essential living costs such as utilities & insurance premiums</li>
                        <li><i class="fa fa-check-circle" aria-hidden="true"></i>Maintain your current standard of living including contributing to the cost of raising your children (e.g., education and extracurricular </li>
                    </ul>
                </div>
                <div class="box-future text-center">
                    <div class="future-title">NURTURE YOUR FUTURE</div>
                    <div class="getsefl">Get yourself covered for as little as <br>
                        <span>$15 per month.</span>
                    </div>
                    <div class="get-button"><a href="">GET INSURED</a></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //////////// section 02 //////////// -->

<!-- section 03 REAL LIFE STORIES-->
<section class="section-03">
    <div class="container">			
        <div class="row top-space">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                    <div class="real-title">
                        Life Cover<br><strong>John is Diagnosed with Prostate Cancer</strong>
                        <div class="border-blue"></div>
                    </div>
                </div>
                <div class="testimonial-inner02"> 						 
                    <div class="t-box-space">							
                        <span class="mau-text">John was semi-retired when he was diagnosed with prostate cancer. Listen to how Trauma insurance helped John focus on his cancer treatment and getting well again. 
                            <br><br></span>
                        <div class="row mau-img tp-sp">
                            <span class="col-md-9 user-text">
                                <img src="<?php echo base_url('assets/img/inner-img/img03.png') ?>" alt="">
                                <span class="frog-desi">
                                    <strong>John Simha</strong><br>
                                    Frog Designs<br> Founder &amp; Art Director
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">                    
                <div class="testimonial-inner">  
                    <div class="quote-img">
                        <div class="quate-icon"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
                    </div>
                    <div class="t-img">
                        <a href="#" class='click' value="QH03CgaGk3I">
                            <img src="<?php echo base_url('assets/img/t-img.png') ?>" alt="" class="img-responsive" />
                        </a>
                    </div>						
                </div>
            </div>
        </div>			
    </div>
</section>
<!-- //////////// section 03 //////////// -->
<!-- section 03 REAL LIFE STORIES-->
<section class="section-04">
    <div class="container">
        <!-- Section 02 -->
        <div id="carousel-example-generic01" class="carousel slide" data-ride="carousel">				
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="chances text-center">
                                What are your chances?
                                <div class="border-white"></div>
                            </div>
                            <div class="text-center what-content">Cardiovascular disease (CVD) is a major cause of death in Australia. There were  43,603 deaths attributed to CVD in Australia in 2013.  On average, one Australian dies as a result of CVD every 12 minutes.2  </div>
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="chances text-center">
                                What are your chances?
                                <div class="border-white"></div>
                            </div>
                            <div class="text-center what-content">Cardiovascular disease (CVD) is a major cause of death in Australia. There were  43,603 deaths attributed to CVD in Australia in 2013.  On average, one Australian dies as a result of CVD every 12 minutes.2  </div>							
                        </div>
                    </div>
                </div>										
            </div>
            <div class="col-md-8 col-md-offset-2 text-center">
                <span class="next-pre-button-in">
                    <a class="carousel-control01 next-button-in" href="#carousel-example-generic01" role="button" data-slide="next">
                        &gt;
                    </a>
                    <a class="carousel-control01 prev-button-in" href="#carousel-example-generic01" role="button" data-slide="prev">
                        &lt;
                    </a>
                </span>
            </div>
        </div>	
    </div>
</section>
<div class="text-center down-arrow">
    <img src="<?php echo base_url('assets/img/inner-img/down-arrow.png') ?>" alt="" />
</div>

<section class="references-section">
    <div class="container">
        <div class="col-md-12">
            <span class="more-plus">
                <a href="" class="">References
                    <span class="plus"><i class="fa fa-plus"></i></span>
                </a>
            </span>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="male-female">
                    Leading cause of death in 2012 (Male/Female)
                </div>
                <div class="male-img">
                    <img src="<?php echo base_url('assets/img/inner-img/percent-img.png') ?>" class="img-responsive" alt="" />
                </div>
            </div>
            <!-- Case Studies Slider -->
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
                <div class="real-title-inr">
                    Case Studies<br><strong>HOW  LIFE INSURANCE HELPS</strong>
                </div>
                <div id="carousel-example-generic-t" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                <div class="case-studies">
                                    <div class="tom-title">
                                        <img src="<?php echo base_url('assets/img/inner-img/t-img.png') ?>" />
                                        TOM BECOMES A SINGLE DAD 
                                        <div class="top-content">
                                            When Sarah died unexpected Tom had to learn how to be a single dad. Life insurance helped make things ea... 
                                        </div>
                                        <div class="full-story"><a href="">Full Story</a></div>
                                        <div>
                                            <span class="next-pre-button-in-dummy">
                                                <a href="" class="next-button-in-dummy">&nbsp;</a>
                                                <a href="" class="prev-button-in-dummy">&nbsp;</a>
                                            </span>
                                        </div>
                                    </div>
                                    <!-- <span><img src="assets/img/inner-img/i.png" alt="test" /></span> -->
                                </div>
                                <div class="case-arrow">
                                    <img src="<?php echo base_url('assets/img/inner-img/case-arrow.png') ?>" alt="test" />
                                </div>
                                <div class="case-name">
                                    TOM HARDY (35)<br><span>Single Dad, IT shift worker</span>
                                </div>
                            </div>
                        </div>					
                        <div class="item">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                <div class="case-studies">
                                    <div class="tom-title">
                                        <img src="<?php echo base_url('assets/img/inner-img/t-img.png') ?>" />
                                        TOM BECOMES A SINGLE DAD 
                                        <div class="top-content">
                                            When Sarah died unexpected Tom had to learn how to be a single dad. Life insurance helped make things ea... 
                                        </div>
                                        <div class="full-story"><a href="">Full Story</a></div>
                                        <div>
                                            <span class="next-pre-button-in-dummy">
                                                <a href="" class="next-button-in-dummy">&nbsp;</a>
                                                <a href="" class="prev-button-in-dummy">&nbsp;</a>
                                            </span>
                                        </div>									
                                    </div>
                                    <!-- <span><img src="assets/img/inner-img/i.png" alt="test" /></span> -->
                                </div>
                                <div class="case-arrow">
                                    <img src="<?php echo base_url('assets/img/inner-img/case-arrow.png') ?>" alt="test" />
                                </div>
                                <div class="case-name">
                                    TOM HARDY (35)<br><span>Single Dad, IT shift worker</span>
                                </div>
                            </div>
                        </div>										
                    </div>
                    <div class="col-md-6 col-md-offset-2 text-center">
                        <span class="next-pre-button-in-t">
                            <a class="carousel-control01 next-button-in" href="#carousel-example-generic-t" role="button" data-slide="next">
                                &gt;
                            </a>
                            <a class="carousel-control01 prev-button-in" href="#carousel-example-generic-t" role="button" data-slide="prev">
                                &lt;
                            </a>
                        </span>
                    </div>
                </div>
            </div>	
            <!-- Case Studies Slider -->
        </div>
        <div class="row top-space02">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="renni-inner">
                    <span class="protection-icon">
                        <img src="<?php echo base_url('assets/img/inner-img/protec01.png') ?>" alt="test" />
                    </span>
                    <span class="protection-text">Renni Income Protection</span><br>
                    Learn how easy it is to protect your assets during periods of ill health
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="renni-inner">
                    <span class="protection-icon">
                        <img src="<?php echo base_url('assets/img/inner-img/user-icon.png') ?>" alt="test" />
                    </span>
                    <span class="protection-text">Hear what the experts say</span><br>
                    Discover how Australians are coping with serious illness. Read our selection of third-party publications. 
                </div>
            </div>
        </div>
    </div>
</section>

<!-- section 06 -->
<section class="section-06-inr">
    <div class="container-fluid">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
            <div class="first-box01">
                <div class="tool-title-inr">
                    <span>Understand the risks?</span><br>
                    Renni Facts & Figures
                </div>
                <div class="border-white02"></div>
                <div class="tool-content-inr">
                    Explore the statistics behind personal insurance and understand why getting covered should be at the top of your list...
                    <br/><br/>
                </div>
                <div class="discover-button">
                    <a href="">DISCOVER</a>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
            <div class="first-box02">
                <div class="tool-title-inr">
                    <span>Can I afford it?</span><br>
                    COST OF A LIFE INSURANCE
                </div>
                <div class="border-white02"></div>
                <div class="tool-content-inr">
                    A life insurance policy for 30 year old Australian male with no pre-existing risk factors and working in an occupation that is not deemed...
                </div>
                <div class="discover-button">
                    <a href="">REQUEST A QUOTE</a>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
            <div class="first-box03">
                <div class="tool-title-inr">
                    <span>Unsure about life insurance? </span><br>
                    Review our Renni Case Studies
                </div>
                <div class="border-white02"></div>
                <div class="tool-content-inr">
                    Unsure about life insurance? Review our Renni Case Studies for more examples for how life insurance can work for you and your loved ones.
                </div>
                <div class="discover-button">
                    <a href="<?php echo site_url('Casestudies') ?>">LEARN MORE</a>
                </div>
            </div>
        </div>
    </div>    
</section>
<!-- //////////// section 06 //////////// -->

<!-- section 05 -->
<section class="section-05">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="real-title"><strong>RENNI FAQ’s. LIFE INSURANCE</strong>
                    <div class="border-blue"></div>
                </div>
            </div>
        </div>
        <div class="row panel-group">
            <div class="col-md-6">
                <!-- Tabs --> 
                <div class="form-tabs" id="accordion-f" role="tablist" aria-multiselectable="true">
                    <?php get_faq_by_insurance(2); ?>					
                </div>
                <!-- Tabs ////////// -->
                <div class="more-button">
                    <a href="<?php echo site_url('faq') ?>">More FAQ‘s &nbsp; <img src="<?php echo base_url('assets/img/more-arrow.png') ?>" alt="" /></a>
                </div>
            </div>
            <div class="col-md-6">
                <?php $this->load->view('ask_form'); ?>				
            </div>
        </div>
        <div class="row">				
            <div id="carousel-example-generic03" class="carousel slide" data-ride="carousel">	
                <div class="faqs">
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="did-you">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <div class="did-title">Did you know?</div>
                                    <div class="getsefl-inr">In many cases life insurance will also be paid after your diagnosis of a terminal illness allowing you to spend quality time with your family and managing your illness.
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="get-inr"><a href="">GET INSURED</a></div>
                                </div>
                            </div>
                        </div>					
                        <div class="item">
                            <div class="did-you">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <div class="did-title">Did you know?</div>
                                    <div class="getsefl-inr">In many cases life insurance will also be paid after your diagnosis of a terminal illness allowing you to spend quality time with your family and managing your illness.
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="get-inr"><a href="">GET INSURED</a></div>
                                </div>
                            </div>
                        </div>										
                    </div>										
                </div>					
                <div class="col-md-12 clearfix">
                    <span class="next-pre-button-in-03">
                        <a class="carousel-control01 next-button-in-03" href="#carousel-example-generic03" role="button" data-slide="next">
                            &gt;
                        </a>
                        <a class="carousel-control01 prev-button-in-03" href="#carousel-example-generic03" role="button" data-slide="prev">
                            &lt;
                        </a>
                    </span>
                </div>				
            </div>
        </div>
    </div>
</section>
<!-- //////////// section 05 //////////// -->
<?php $this->load->view('casestudies_news_4more'); ?>
<!-- //////////// section 05 //////////// -->

<!-- //////////// section 05 //////////// -->
<section class="logo-section">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <?php $this->load->view('all_logo'); ?>
            </div>
        </div>
    </div>
</section>


<!-- section 08 -->
<section class="section-08-iner">
    <div class="section-img-inner">
        <div class="container">
            <div class="row">            
                <div class="col-lg-7 col-sm-6 discover-t_not">Not sure about something? </div>
                <div class="col-lg-5 col-sm-6 two-button-inr text-right">
                    <a href="javascript:void(0);" class="button-request-inr">REQUEST ASSISTANCE</a>
                </div>
            </div>    
        </div>
    </div>
</section> 
<!-- //////////// section 08 //////////// -->

