<!-- section 03 REAL LIFE STORIES-->
<section class="case_section">
    <div class="container">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <?php foreach ($result as $res): ?>
                    <?php if ($res->rlid == $this->uri->segment(3)) { ?>
                        <div class="item active">
                        <?php } else { ?>
                            <div class="item">
                            <?php } ?>

                            <!-- <div class="row">				
                                    <div class="border_button_case_faq">
                                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                            <a href="" class="previously_button"></a>
                                            <a href="" class="previously_button02"> < PREVIOUS </a>
                                            </div>
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-right">
                                                    <a href="" class="previously_button"> NEXT > </a>
                                            </div>
                                    </div>
                            </div> -->		
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">	
                                    <div class="case_studies_div">
                                        <div class="real-title-inr02">
                                            <img src="<?php echo base_url(); ?>/uploads/casestudies/<?php echo $res->imageurl; ?>" class="img-responsive">
                                        </div>							
                                        <div class="Video_CNT">
                                            <span><?php echo $res->title; ?></span>
                                            <div class="Date_TPD">
                                                <?php echo $res->sub_title_short; ?>
                                            </div>
                                        </div>
                                        <?php echo $res->sub_title_long; ?>
                                        <div class="mark_speech">
                                            <span class="double_qut"><img src="<?php echo base_url('assets/img/inner-img/double_qut.png');?>" alt="double_qut"></span>
                                            <div class="mark_text">
                                                Mark’s speech impediment made it difficult for him to communicate verbally		
                                            </div>
                                        </div>

                                        <?php echo $res->details; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="border_button_top">
                                        <span class="bowl_caner">
                                            <?php echo $res->short_reference; ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div  class="border_button">
                                        <span class="buttoN_share"><i class="fa fa-tag"></i></span>
                                        <div class="button_over02">
                                            <?php
                                            if ($res->tags != '') {
                                                $tags = explode(',', $res->tags);
                                                foreach ($tags as $tag):
                                                    echo '<a href="">' . $tag . '</a>';
                                                endforeach;
                                            }
                                            ?>  

                                        </div>
                                    </div>					
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div  class="border_button02">
                                        <span class="buttoN_share" style="background: #21c2f8;"><i class="fa fa-share-alt"></i></span>
                                        <div class="button_over03">
                                            <a href="">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-linkedin"></i>
                                            </a>
                                            <a href="">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                        </div>
                                    </div>					
                                </div>
                            </div>		
                        </div>
                    <?php endforeach; ?>


                </div>
                <div class="border_button_case">
                    <!-- Left and right controls -->
                    <a class="left carousel-control previously_button02" href="#myCarousel" data-slide="prev">Previous
                    </a>
                    <a class="right carousel-control previously_button02" href="#myCarousel" data-slide="next">Next
                    </a>
                </div>
            </div>

        </div>
</section>
<!-- //////////// section 03 //////////// -->

<!-- //////////// section 05 //////////// -->
<section class="logo-section" style="border-top: 1px solid #eee;">
    <div class="container text-center">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <?php $this->load->view('all_logo'); ?>
            </div>
        </div>
    </div>
</section>
