<h2>Edit Blog</h2>
<form class="col-md-10 col-md-offset-1" action="" method="POST" enctype="multipart/form-data" class="form-horizontal">
    <div class="col-md-12 Form_main_div">
        <div class="form-group">
            <div class=" btn-danger">
                <?php
                echo validation_errors();
                if (isset($msg)) {
                    echo $msg;
                }
                ?>
            </div>
        </div>	
        <div class="form-group">
            <label>Title</label>
            <input type="text" name="title" value="<?php echo $result['title'] ?>" class="form-control" required="">
        </div>
        <div class="form-group">
            <label for="sel1">Author</label>		   
            <?php
            $option_arr[] = 'Select Author';
            foreach ($authordata as $au) {
                $option_arr[$au->author_name] = $au->author_name;
            }
            ?>

<?php echo form_dropdown('author_name', $option_arr, $result['author_id'], ['class' => 'form-control', 'id' => 'author_name', 'required' => ""]); ?>
        </div>
        <div class="form-group">
            <label>Reference</label>
            <input type="text" name="news_reference" class="form-control" value="<?php echo $result['news_reference'] ?>" required="">
        </div>
        <div class="form-group">
            <label>Details</label>
            <textarea class="ckeditor form-control" id="answer" name="details" required=""><?php echo $result['details'] ?></textarea>
            <?php
            $ckeeee = ['id' => 'answer',
                'path' => 'assets/js/ckeditor',
                'config' => [
                    'filebrowserImageUploadUrl' => site_url('securearea/Ckeditorform/upload'),
                // add additional CKEditor properties here
                ],
            ];

            echo display_ckeditor($ckeeee);
            ?>
        </div>
        <div class="form-group">
            <label>Tags</label>
            <input type="text" name="tag" value="<?php echo $result['tags'] ?>" class="form-control" data-role="tagsinput" required="">
        </div>

        <div class="form-group">
            <label>Featured </label>
            <div class="form-control">
                <input class="radio-inline radio" onclick="$('#feature_imaged').show('slow');" <?php echo ($result['is_featured'] == 1) ? 'checked="checked"' : ''; ?> type="radio" value="1" name="optradio">Yes	
                <input class="radio-inline radio" onclick="$('#feature_imaged').hide('slow');" <?php echo ($result['is_featured'] == 0) ? 'checked="checked"' : ''; ?> type="radio" value="0" name="optradio">No
            </div>
        </div>
        <div <?php echo ($result['is_featured'] == 0) ? 'style="display:none;"' : ''; ?> id="feature_imaged" class="form-group">
            <label>Feature Image </label>
            <input type="file" name="feature_image" class="form-control upload_img" style="margin-top:10px; border:none; box-shadow:0px !important">
        </div>
        <div class="form-group">
            <label>Image</label>
            <input type="file" name="image" class="form-control upload_img" style="margin-top:10px; border:none; box-shadow:0px !important">
        </div>
        <div class="form-group">
            <label for="sel1">Select list:</label>

<?php echo form_dropdown('ins_get', $insurence_list, $result['insurance'], ['class' => 'form-control', 'id' => 'ins_change']); ?>

        </div>
        <div class="form-group">
            <label for="sel1">Select Sub list:</label>
            <div id="get_data">
            </div>
            <br/>
        </div>
        <div class="form-group">
<?php echo form_submit('submit', 'Submit', 'class="btn btn-success submit"'); ?>
            <a href="<?php echo site_url('SecureArea/blog'); ?>"<button class="btn btn-danger cancel">cancel</button></a>
        </div>
    </div>
</form>



<script type="text/javascript">
    jQuery(document).ready(function () {
        $('form').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });
        ///auto load sub cat                 
        getsubcats("<?php echo $result['insurance']; ?>");

        jQuery("#ins_change").change(function () {
            var ins_val = $(this).val();
            getsubcats(ins_val);
        });

    });

    function getchecked_subcat() {
        var dt_set = "<?php echo $result['ins_ids']; ?>";
        var dt_arr = dt_set.split(',');
        $.each(dt_arr, function (ni, nv) {
            $.each($('#get_data').find('input'), function (i, v) {
                if ($(v).val() == nv) {
                    $(v).click();
                }
            });
        });
    }

    function getsubcats(ins_val)
    {
        if (ins_val == '' || ins_val == '0') {
            $('#get_data').html('');
        } else {
            $.ajax({
                url: '<?php echo base_url(); ?>/index.php/SecureArea/Faqs/subinc_cat',
                data: {ins_val: ins_val},
                type: 'post',
                success: function (data) {
                    $('#get_data').replaceWith(data);
                    getchecked_subcat();
                }
            });
        }
    }
</script> 
