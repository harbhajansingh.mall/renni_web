<div class="row">
  <div class="col-md-8"> <h2>Insurances</h2></div>
 <div class="col-md-4 right"><a href="<?php echo site_url('securearea/Insurance/openAddForm'); ?>" class="btn btn-default pull-right">Add New</a></div>
</div>

<div class="table-responsive">  

  
  <!-- <p>The .table-striped class adds zebra-striping to any table row within tbody (not available in IE8):</p>                   -->
  <table class="table table-striped">
    <thead>
      <tr>
        
        <th>Insurance</th>
        <th>Sub Title</th>
        <th>Parent</th>
        <th>Image</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php 
foreach ($result as $res) {
 ?>
      <tr>
        
        <td><?php echo $res->ins_name; ?></td>
         <td><?php echo $res->ins_subtitle; ?></td>
         <td><?php echo $res->parent_name; ?></td>
          <td><?php if($res->image_url!=''){echo '<img src="'.base_url().'uploads/thumbnails/' . $res->image_url.'" height="100px" alt="No Image">';}else{ echo 'NA';}?></td>
          <td class="action">
            <a href="<?php echo site_url('/SecureArea/Insurance/Update/'.$res->ins_id); ?>">Edit</a>&nbsp;|
            <a href="<?php echo site_url('/SecureArea/Insurance/DeleteRec/' . $res->ins_id . '/' . $res->image_url); ?>" onclick="isconfirm('Are you sure you want to delete this record?');">Delete</a>
            
         </td>
      </tr>
<?php } ?>
 <tr>
<td colspan="4"><span class="alert danger"><?php echo $msg; ?></span></td>
 </tr>
      
    </tbody>
  </table>
</div>