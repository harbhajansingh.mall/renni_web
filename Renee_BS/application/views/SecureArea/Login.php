<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('css/public/bootstrap.min.css')?>" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('/css/SecureArea/custom.css') ; ?>">
  <script src="<?php echo base_url('js/jquery.js');?>"></script>
  <script src="<?php echo base_url('js/bootstrap.min.js')?>"></script> 


</head>

<body>

<div class="container vertical-center">
 

  	<div class="col-md-6 col-md-offset-3">


 <h2 class="col-md-10 col-md-offset-2">Login <?php //echo base_url(); ?></h2>

 <?php if(validation_errors() || $msg <> '')
 {
 	?>
<div class="col-md-10 col-md-offset-2 alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong><?php echo validation_errors(); ?></strong>
  <p><?php echo $msg; ?></p>
</div>
<?php
 } 
 
 echo form_open('login/Validate_Crendentials','class="form-horizontal"');
?>
    <div class="form-group">
      <label class="control-label col-sm-2" for="username">Username:</label>
      <div class="col-sm-10">
        <!-- <input type="text" class="form-control" name="username" id="username" placeholder="Enter Username"> -->
        <?php echo form_input('username','','class="form-control" placeholder="Enter Username"'); ?>
         
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Password:</label>
      <div class="col-sm-10">          
        <?php echo form_password('password','','class="form-control" placeholder="Enter Password"'); ?>
      </div>
    </div>
    
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <!-- <button type="submit" name="submit" class="btn btn-default">Submit</button> -->
        <?php echo form_submit('submit','Login'); ?>
      </div>
    </div>


  <!-- </form> -->
  <?php echo form_close(); ?>
</div>
 



</div>

</body>
</html>

