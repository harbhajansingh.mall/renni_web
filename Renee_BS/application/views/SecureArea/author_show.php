<style>
.col-md-12.message-div {
    margin: 10px 0px;
    font-size: 18px;
    color: #289a28;
}	
</style>

<div class="row" id="contactlist">
  <div class="col-md-8"> <h1 style="color:blue">Authors</h1></div>
  <div class="col-md-4 text-right"> <a href="<?php echo site_url('SecureArea/AuthorsController/authors_add'); ?>" class="btn btn-default">Add New</a> </div>
 <div class="col-md-12 message-div">
	 <?php if($this->session->flashdata('success'));{?>
	 <div class="form-message">
		<?php echo $this->session->flashdata('success');?>
	 </div>
	 <?php }?>
	 
	 <?php if($this->session->flashdata('deletesuccess'));{?>
	 <div class="form-message">
		<?php echo $this->session->flashdata('deletesuccess');?>
	 </div>
	 <?php }?>
	 <?php if($this->session->flashdata('updatesuccess'));{?>
	 <div class="form-message">
		<?php echo $this->session->flashdata('updatesuccess');?>
	 </div>
	 <?php }?>
</div>
 <div class="col-md-12">
<div class="table-responsive">  
  <table class="table table-default">
    <thead>
      <tr>
        <th>Author</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
 <?php 
      
foreach ($result as $res) {

  
 ?>
      <tr>
        <td><?php echo $res->author_name; ?></td>
        
      
        <td>
             <a href="<?php echo site_url('SecureArea/AuthorsController/authors_delete/'.$res->author_id); ?>" onclick="return confirm('Are you sure?')" class="delete">Delete</a>
             |
             <a href="<?php echo site_url('SecureArea/AuthorsController/authors_update/'.$res->author_id); ?>">Edit</a>
        </td>
      </tr> 
<?php } ?>
</tbody>
</table>
</div>
</div>


</div>