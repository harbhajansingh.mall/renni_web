<div class="row" id="contactlist">
  <div class="col-md-8"> <h1 style="color:blue">Life Story</h1></div>
  <div class="col-md-4 text-right"> <a href="<?php echo site_url('securearea/LifestoryCont/add'); ?>" class="btn btn-default">Add New</a> </div>
 
<div class="col-md-12">
<div class="table-responsive">  
  <table class="table table-default">
    <thead>
      <tr>
        <th> Title  </th>
       <th>Youtube link</th>
       <th>Author name</th>
       <th>Company</th>
       <th>Designation</th>
        <th>Details</th>
        <th>ins type</th>
       <th>Image</th>
       <th>Action</th>
      </tr>
    </thead>
    
    <tbody>
   <?php 

foreach($result as $k) {

  ?>
  <tr>
   
    
       <td> <?php echo $k->title; ?></td>
       <td> <?php echo $k->youtubeurl; ?></td>
       <td> <?php echo $k->authorname; ?></td>
       <td> <?php echo $k->company; ?></td>
       <td> <?php echo $k->designation; ?></td>
       <td> <?php echo $k->details; ?></td>
       <td> <?php echo $k->Ins_Name; ?></td>
       
        <td><img src="<?php echo base_url('uploads/'.$k->imgurl); ?>" width="50px"></img> </td>
        <td><a href="<?php echo site_url('securearea/LifestoryCont/delete/') .$k->rlid  ?>">Delete</a>/
        <a href="<?php echo site_url('securearea/LifestoryCont/showedit/') .$k->rlid  ?>">Edit</a></td> 
      
     </tr>
 <?php 
}
 ?>
</tbody>
</table>
</div>
</div>