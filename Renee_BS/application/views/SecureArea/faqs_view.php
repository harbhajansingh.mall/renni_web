
<div class="row" id="contactlist">
  <div class="col-md-8"> <h1 style="color:blue">FAQs</h1></div>
  <div class="col-md-4 text-right"> <a href="<?php echo site_url('securearea/Faqs'); ?>" class="btn btn-default">Add New</a> </div>
  
 
<div class="col-md-12">
<div class="table-responsive">  
  <table class="table table-default">
    <thead>
      <tr>
        <th>Questions</th>
        <th>Answer</th>
        <th>Status</th>
        <th>Insurane Type</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
 <?php 
      
foreach ($result as $res) {
	//print_r($res);
	
  
 ?>
      <tr>
        <td><?php echo $res->question; ?></td>
        <td><?php echo substr(strip_tags($res->answer),0,100);echo (strlen(strip_tags($res->answer))>100)?'...':''; ?></td>
        <td><?php  if($res->fstatus==1) echo 'Active'; else echo 'Inactive'; ?></td>
        <td><?php echo ($res->ins_name=='')?'NA':$res->ins_name; ?></td>
      
        <td>
             <a href="<?php echo site_url('securearea/Faqs/deletedata/' . $res->faqid );?>" onclick="isconfirm('Are you sure you want to delete this record?');" class="delete">Delete</a>
             |
             <a href="<?php echo site_url('securearea/Faqs/showedit/' . $res->faqid );?>">Edit</a>
        </td>
      </tr> 
<?php } ?>
</tbody>
</table>
</div>
</div>