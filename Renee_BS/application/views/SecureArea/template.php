<?php 
// Include header area
$this->load->view('securearea/header');

// include body area
$this->load->view('securearea/'. $pagename);

// Footer area
$this->load->view('securearea/footer'); 

?>