
  <style>
.col-md-12.message-div {
    margin: 10px 0px;
    font-size: 18px;
    color: #289a28;
}	
</style>

<div class="row" id="contactlist">
  <div class="col-md-8"> <h1 style="color:blue">Subscriber</h1></div>
  
 <div class="col-md-12 message-div"> 
	 <?php if($this->session->flashdata('deletesuccess'));{?>
	 <div class="form-message">
		<?php echo $this->session->flashdata('deletesuccess');?>
	 </div>
	 <?php }?>	 
</div>
 <div class="col-md-12">
<div class="table-responsive">  
  <table class="table table-default">
    <thead>
      <tr>        
        <th>Email</th>        
        <th>Created at</th>
        <th>Ip-address</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
 <?php 
  if(empty($result)){
      echo '<tr><td colspan="10">No record found.</td></tr>';
  } else{   
foreach ($result as $res) {
 ?>
      <tr>
        <tr>
        <td><?php echo $res->email; ?></td>        
        <td><?php echo date('F j, Y h:i A',strtotime($res->created_at)); ?></td>
        <td><?php echo $res->ip_address; ?></td>     
        <td>
             <a href="<?php echo site_url('securearea/Subscriber/delete/' . $res->id );?>" onclick="return confirm('Are you sure?')" class="delete">Delete</a>
            
        </td>
      </tr> 
      </tr> 
  <?php }} ?>
</tbody>
</table>
</div>
</div>


</div>
 