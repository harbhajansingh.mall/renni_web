<?php
echo form_open('SecureArea/Faqs/updaterec', 'class="form-horizontal"');
foreach ($result as $res) {
    $faqid = $res->faqid;
    $question = $res->question;
    $answer = $res->answer;
    $insurance = $res->insurance;
    $fstatus = $res->fstatus;
    $id = $res->Ins_Ids;
}
?>
<h2>Edit FAQ</h2>
<div class="form-group">
    <div class="col-md-12">
        <label>Questions</label>
        <input type="hidden" value="<?php echo $faqid; ?>"name="faqid" id="faqid"> 
        <?php echo form_input('question', $question, 'class="form-control"'); ?>
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <label>Answers</label>
        <textarea class="ckeditor form-control" id="answer" name="answer"><?php echo $answer; ?></textarea>
        <?php
        $ckeeee = ['id' => 'answer',
            'path' => 'assets/js/ckeditor',
            'config' => [
                // path to submit image upload form to. i.e., upload() above
                'filebrowserImageUploadUrl' => site_url('securearea/Ckeditorform/upload'),
            // add additional CKEditor properties here
            ],
        ];

        echo display_ckeditor($ckeeee);
        ?>
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <label>Status</label><br>
        <input type="radio" name="fstatus" value=1 <?php echo $c = ($fstatus == '1' ? 'checked' : ''); ?>> Active
        <input type="radio" name="fstatus" value=0 <?php echo $c = ($fstatus == '0' ? 'checked' : ''); ?>> Inactive<br>
    </div>
</div>


<div class="form-group">
    <label for="sel1">Select list:</label>
<?php
$parentlist1[''] = 'Select insurance type';
foreach ($result1 as $res) {
    $parentlist1[$res->ins_id] = $res->ins_name;
}
?>

    <?php echo form_dropdown('insurance_get', $parentlist1, $insurance, ['class' => 'form-control', 'id' => 'ins_change']); ?>

</div>

<div class="form-group">
    <label for="sel1">Select Sub list:</label>
    <div class="col-md-12" id="get_data">
    </div>
</div>



<div class="form-group">
    <div class="col-md-12"><br>
<?php echo form_submit('submit', 'Submit', 'class="btn btn-success"'); ?>

        <a href="<?php echo site_url('SecureArea/Faqs/show'); ?>"<button class="btn btn-danger">cancel</button></a>
    </div>
</div>

<?php echo form_close(); ?>
<div>
<?php echo validation_errors(); ?>


</div>

<script type="text/javascript">
    jQuery(document).ready(function () {
        ///auto load sub cat                 
        getsubcats("<?php echo $insurance; ?>");

        jQuery("#ins_change").change(function () {
            var ins_val = $(this).val();
            getsubcats(ins_val);
        });

    });

    function getchecked_subcat() {
        var dt_set = "<?php echo $id; ?>";
        var dt_arr = dt_set.split(',');
        $.each(dt_arr, function (ni, nv) {
            $.each($('#get_data').find('input'), function (i, v) {
                if ($(v).val() == nv) {
                    $(v).click();
                }
            });
        });
    }

    function getsubcats(ins_val)
    {
        if(ins_val=='' || ins_val==0){
            $('#get_data').html('');            
        }else{
        $.ajax({
            url: '<?php echo base_url(); ?>/index.php/SecureArea/Faqs/subinc_cat',
            data: {ins_val: ins_val},
            type: 'post',
            success: function (data) {
                $('#get_data').replaceWith(data);
                getchecked_subcat();
            }
        });
        }
    }
</script> 
