<h2>Add News</h2>
<form class="col-md-10 col-md-offset-1" action="" method="POST" enctype="multipart/form-data" class="form-horizontal">
    <div class="col-md-12 Form_main_div">
        <div class="form-group">
            <div class=" btn-danger">
                <?php
                echo validation_errors();
                if (isset($msg)) {
                    echo $msg;
                }
                ?>
            </div>
        </div>	
        <div class="form-group">
            <label>Title</label>
            <input type="text" name="title" class="form-control" required="">
        </div>
        <div class="form-group">
            <label for="sel1">Author</label>
            <select class="form-control" name="author_name" required="">
                <option value="">Select Author</option>
                <?php
                foreach ($authordata as $au) {
                    ?>
                    <option  value="<?php echo $au->author_name ?>"><?php echo $au->author_name; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label>News Reference</label>
            <input type="text" name="news_reference" class="form-control" required="">
        </div>
        <div class="form-group">
            <label>Details</label>
            <textarea class="ckeditor form-control" id="answer" name="details" required=""></textarea>
            <?php
            $ckeeee = ['id' => 'answer',
                'path' => 'assets/js/ckeditor',
                'config' => [
                    'filebrowserImageUploadUrl' => site_url('securearea/Ckeditorform/upload'),
                // add additional CKEditor properties here
                ],
            ];

            echo display_ckeditor($ckeeee);
            ?>
        </div>
        <div class="form-group">
            <label>Tags</label>
            <input type="text" value="" name="tag" class="form-control" data-role="tagsinput" required="" />

<!--            <input type="text" name="tag" class="form-control" required="">-->
        </div>
        <div class="form-group">
            <label>Redirection Url</label>
            <input type="url" name="redirection_url" class="form-control" required="">
        </div>
        <div class="form-group">
            <label>Featured</label>
            <div class="form-control">
                <input class="radio-inline radio" onclick="$('#feature_imaged').show('slow');" type="radio" value="1" name="optradio">Yes	
                <input class="radio-inline radio" onclick="$('#feature_imaged').hide('slow');" checked="checked" type="radio" value="0" name="optradio">No
            </div>
        </div>
        <div id="feature_imaged" class="form-group" style="display: none;">
            <label>Feature Image</label>
            <input type="file" name="feature_image" class="form-control upload_img" style="margin-top:10px; border:none; box-shadow:0px !important">
        </div>
        <div class="form-group">
            <label>Image</label>
            <input type="file" name="image" class="form-control upload_img" style="margin-top:10px; border:none; box-shadow:0px !important">
        </div>
        <div class="form-group">
            <label for="sel1">Select list:</label>

            <?php echo form_dropdown('ins_get', $insurence_list, '', ['class' => 'form-control', 'id' => 'ins_change', 'required' => ""]); ?>

        </div>
        <div class="form-group">
            <label for="sel1">Select Sub list:</label>
            <div id="get_data">
            </div>
        </div>
        <div class="form-group">
            <?php echo form_submit('submit', 'Submit', 'class="btn btn-success submit"'); ?>
            <a href="<?php echo site_url('SecureArea/news'); ?>"<button class="btn btn-danger cancel">cancel</button></a>
        </div>
    </div>
</form>


<script type="text/javascript">
    jQuery(document).ready(function () {
        $('form').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });
        jQuery("#ins_change").change(function () {
            var ins_val = $(this).val();
            if (ins_val == '' || ins_val == '0') {
                $('#get_data').html('');
            } else {
                $.ajax({
                    url: '<?php echo base_url(); ?>/index.php/SecureArea/Faqs/subinc_cat',
                    data: {ins_val: ins_val},
                    type: 'post',
                    success: function (data) {
                        $('#get_data').replaceWith(data)
                    }
                });
            }
        });
    });
</script> 
