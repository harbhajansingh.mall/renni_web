
<?php echo form_open('SecureArea/Faqs/process', 'class="form-horizontal"'); ?>
<h2>Add FAQ</h2>
<div class="form-group">
    <div class="col-md-12">
        <div class=" btn-danger">
            <?php echo validation_errors();
            if (isset($msg)) {
                echo $msg;
            }
            ?>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <label>Questions</label>
<?php echo form_input('question', '', 'class="form-control"'); ?>
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <label>Answers</label>
        <textarea class="ckeditor form-control" id="answer" name="answer" ></textarea>
        <?php
        $ckeeee = ['id' => 'answer',
            'path' => 'assets/js/ckeditor',
            'config' => [
                'filebrowserImageUploadUrl' => site_url('securearea/Ckeditorform/upload'),
            // add additional CKEditor properties here
            ],
        ];

        echo display_ckeditor($ckeeee);
        ?>
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <label>Status</label><br>
        <input type="radio" name="fstatus" value="1"  checked> Active
        <input type="radio" name="fstatus" value="0" > Inactive<br>
    </div>
</div>


<div class="form-group">
    <label for="sel1">Select list:</label>
    <select class="form-control" name="ins_get"  id="ins_change">
        <option value="">Select insurance type</option>
        <?php
        foreach ($result as $res) {
            ?>
            <option  value="<?php echo $res->ins_id; ?>"><?php echo $res->ins_name; ?></option>
        <?php } ?>
    </select>
</div>
<div class="form-group">
    <label for="sel1">Select Sub list:</label>
    <div id="get_data">
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <?php echo form_submit('submit', 'Submit', 'class="btn btn-success"'); ?>

        <a href="<?php echo site_url('SecureArea/Faqs/show'); ?>"<button class="btn btn-danger">cancel</button></a>
    </div>
</div>

<?php echo form_close(); ?>


<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery("#ins_change").change(function () {
            var ins_val = $(this).val();
            if(ins_val==''){
                $('#get_data').html('');
            }else{
            getsubcats(ins_val);
        }
        });
    });

    function getsubcats(ins_val)
    {
        $.ajax({
            url: '<?php echo base_url(); ?>/index.php/SecureArea/Faqs/subinc_cat',
            data: {ins_val: ins_val},
            type: 'post',
            success: function (data) {
                $('#get_data').replaceWith(data)
            }
        });
    }
</script> 
