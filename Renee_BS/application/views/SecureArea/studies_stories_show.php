  <style>
.col-md-12.message-div {
    margin: 10px 0px;
    font-size: 18px;
    color: #289a28;
}	
</style>

<div class="row" id="contactlist">
  <div class="col-md-8"> <h1 style="color:blue">Case Studies & Stories</h1></div>
  <div class="col-md-4 text-right"> <a href="<?php echo site_url('SecureArea/studies_stories/studies_stories_add'); ?>" class="btn btn-default">Add New</a> </div>
 <div class="col-md-12 message-div">
	 <?php if($this->session->flashdata('success'));{?>
	 <div class="form-message">
		<?php echo $this->session->flashdata('success');?>
	 </div>
	 <?php }?>
	 
	 <?php if($this->session->flashdata('deletesuccess'));{?>
	 <div class="form-message">
		<?php echo $this->session->flashdata('deletesuccess');?>
	 </div>
	 <?php }?>
	 <?php if($this->session->flashdata('updatesuccess'));{?>
	 <div class="form-message">
		<?php echo $this->session->flashdata('updatesuccess');?>
	 </div>
	 <?php }?>
</div>
 <div class="col-md-12">
<div class="table-responsive">  
  <table class="table table-default">
    <thead>
      <tr>
        <th>Title</th>
        <th>Sub Title Short</th>
        <th>Sub Title Long</th>
        <th>Details</th>
        <th>Short Rreference</th>
        <th>Insurance Sub Cat</th>
        <th>Tags</th>
        <th>Images</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
 <?php 
foreach ($result as $res) {
 ?>
      <tr>
        
        <td><?php echo $res->title; ?></td>
        <td><?php echo $res->sub_title_short; ?></td>
        <td><?php echo $res->sub_title_long; ?></td>
        <td><?php echo substr(strip_tags($res->details),0,100);echo (strlen(strip_tags($res->details))>100)?'...':''; ?></td>
        <td><?php echo $res->short_reference; ?></td>
        <td><?php echo $res->ins_name; ?></td>
        <td><?php echo $res->tags; ?></td>
        <td><?php if($res->imageurl==''){?> NA
        <?php }else{?>
            <img style="width:50px" src="<?php echo base_url(); ?>/uploads/casestudies/<?php echo $res->imageurl; ?>">
        <?php }?>
        </td>
      
        <td>
             <a href="<?php echo site_url('securearea/studies_stories/studies_stories_delete/' . $res->rlid );?>" onclick="return confirm('Are you sure?')" class="delete">Delete</a>
             |
             <a href="<?php echo site_url('securearea/studies_stories/studies_stories_edit/' . $res->rlid );?>">Edit</a>
        </td>
      
      </tr> 
<?php } ?>
</tbody>
</table>
</div>
</div>


</div>
 