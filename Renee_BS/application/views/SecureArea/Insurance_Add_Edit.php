<?php 
 $insid="0";
 $title="";
 $subtitle="";
 $parent_id='';
 //print_r($result);
 if(isset($result))
 {
foreach ($result as $res) 
{
  $insid =$res->ins_id;
  $title=$res->ins_name;
  $subtitle=$res->ins_subtitle;
  $parent_id=$res->parent_id;
  //return;
 }
}
 ?>
<div class="col-md-6 col-md-offset-3">

 <h2 class="col-md-10 col-md-offset-2"><?php if($insid==0){echo 'Add';}else{ echo 'Edit';}?></h2>

 <?php

 if(validation_errors() || $msg <> '')
 {
 	?>
<div class="col-md-10 col-md-offset-2 alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong><?php echo validation_errors(); ?></strong>
  <p><?php echo $msg; ?></p>
</div>
<?php

 } 
 
 echo form_open_multipart('SecureArea/Insurance/Add_Update_Record','class="form-horizontal"');
?>
 <div class="form-group">
      <label class="control-label col-sm-3" for="title">Parent:</label>
      <div class="col-sm-9">      
        <?php
        $parentlist1['']='Select insurance type';
          foreach($parentlist as $res) {              
              $parentlist1[$res->ins_id]=$res->ins_name;
        } ?>      
         <?php echo form_dropdown('parent_id', $parentlist1, $parent_id,['class'=>'form-control']);?>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-3" for="title">Insurance:</label>
      <div class="col-sm-9">
        <!-- <input type="text" class="form-control" name="username" id="username" placeholder="Enter Username"> -->
        <?php echo form_input('insid',$insid,'class="form-control" placeholder="Enter your name" style="
    display: none"
"'); ?>
        <?php echo form_input('title',$title,'class="form-control" placeholder="Enter Insurance Title"'); ?>
         
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3" for="subtitle">Sub Title:</label>
      <div class="col-sm-9">          
        <?php echo form_input('subtitle',$subtitle,'class="form-control" placeholder="Enter Sub Title"'); ?>
      </div>
    </div>

     <div class="form-group">
      <label class="control-label col-sm-3" for="subtitle">Image:</label>
      <div class="col-sm-9">   
        <input type="file" name="userfile" class="form-control">
       <!--  <?php echo form_input('subtitle',$subtitle,'class="form-control" placeholder="Enter Sub Title"'); ?> -->
      </div>
    </div>

    <div class="form-group">        
      <div class="col-sm-offset-3 col-sm-9">
        <!-- <button type="submit" name="submit" class="btn btn-default">Submit</button> -->
        <?php echo form_submit('submit','Submit','class="btn btn-default"'); ?>
      <a href="<?php echo site_url().'/SecureArea/Insurance'; ?>" class="btn btn-default">Cancel</a>
      </div>
    </div>


  <!-- </form> -->
  <?php echo form_close(); ?>
</div>
 