
  <style>
.col-md-12.message-div {
    margin: 10px 0px;
    font-size: 18px;
    color: #289a28;
}	
</style>

<div class="row" id="contactlist">
  <div class="col-md-8"> <h1 style="color:blue">News</h1></div>
  <div class="col-md-4 text-right"> <a href="<?php echo site_url('SecureArea/news/news_add'); ?>" class="btn btn-default">Add New</a> </div>
 <div class="col-md-12 message-div">
	 <?php if($this->session->flashdata('success'));{?>
	 <div class="form-message">
		<?php echo $this->session->flashdata('success');?>
	 </div>
	 <?php }?>
	 
	 <?php if($this->session->flashdata('deletesuccess'));{?>
	 <div class="form-message">
		<?php echo $this->session->flashdata('deletesuccess');?>
	 </div>
	 <?php }?>
	 <?php if($this->session->flashdata('updatesuccess'));{?>
	 <div class="form-message">
		<?php echo $this->session->flashdata('updatesuccess');?>
	 </div>
	 <?php }?>
</div>
 <div class="col-md-12">
<div class="table-responsive">  
  <table class="table table-default">
    <thead>
      <tr>
        <th>Title</th>
        <th>Author</th>
        <th>News Reference</th>
        <th>Details</th>
        <th>Tags</th>
        <th>Insurance Sub Cat</th>
        <th>Redirection Url</th>
        <th>Featured</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
 <?php 
  if(empty($result)){
      echo '<tr><td colspan="10">No record found.</td></tr>';
  } else{   
foreach ($result as $res) {
 ?>
      <tr>
        
        <td><?php echo $res->title; ?></td>
        <td><?php echo $res->author_id	; ?></td>
        <td><?php echo $res->news_reference; ?></td>
        <td><?php echo substr(strip_tags($res->details),0,100);echo (strlen(strip_tags($res->details))>100)?'...':''; ?></td>
        <td><?php echo $res->tags; ?></td>
        <td><?php echo $res->ins_name; ?></td>
        <td><?php echo $res->redirection_url; ?></td>
        <td><?php if($res->featured_image==''){?> <img style="width:50px" src="<?php echo base_url(); ?>/uploads/newsblog/<?php echo $res->image_url; ?>">
        <?php }else{?>
            <img style="width:50px" src="<?php echo base_url(); ?>/uploads/newsblog/<?php echo $res->featured_image; ?>">
        <?php }?>
        </td>
        <td><?php  if($res->is_featured==1) echo 'Active'; else echo 'Inactive'; ?></td>
      
        <td>
             <a href="<?php echo site_url('securearea/news/news_delete/' . $res->nb_id );?>" onclick="return confirm('Are you sure?')" class="delete">Delete</a>
             |
             <a href="<?php echo site_url('securearea/news/news_edit/' . $res->nb_id );?>">Edit</a>
        </td>
       
      </tr> 
  <?php }} ?>
</tbody>
</table>
</div>
</div>


</div>
 