<h2>Edit</h2>
<?php echo form_open('', 'class="form-horizontal col-md-10 col-md-offset-1" enctype="multipart/form-data" '); ?>

<div class="form-group">
    <div class="col-md-12">
        <div class=" btn-danger">
            <?php
            echo validation_errors();
            if (isset($msg)) {
                echo $msg;
            }
            ?>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <label>Title</label>
<?php echo form_input('title', $result['title'], ['class' => "form-control", 'required' => 'required']); ?>
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <label>Sub Title Short</label>
<?php echo form_input('sub_title_short', $result['sub_title_short'], ['class' => "form-control", 'required' => 'required']); ?>
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <label>Sub Title Long</label>
<?php echo form_input('sub_title_long', $result['sub_title_long'], ['class' => "form-control", 'required' => 'required']); ?>
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <label>Details</label>
        <textarea class="ckeditor form-control" id="answer" name="details" required="required"><?php echo $result['details']; ?></textarea>
        <?php
        $ckeeee = ['id' => 'answer',
            'path' => 'assets/js/ckeditor',
            'config' => [
                'filebrowserImageUploadUrl' => site_url('securearea/Ckeditorform/upload'),
            // add additional CKEditor properties here
            ],
        ];

        echo display_ckeditor($ckeeee);
        ?>
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <label>Short Reference</label>
<?php echo form_input('short_reference', $result['short_reference'], ['class' => "form-control", 'required' => 'required']); ?>
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <label>Tag</label>
<?php echo form_input('tag', $result['tags'], ['class' => "form-control",'data-role'=>'tagsinput', 'required' => 'required']); ?>
    </div>
</div>

<div class="form-group">
    <div class="col-md-12">
        <label for="sel1">Select list:</label>
<?php echo form_dropdown('ins_get', $insurence_list, $result['insurance'], ['class' => 'form-control', 'id' => 'ins_change', 'required' => ""]); ?>

    </div>
</div>
<div class="form-group">
    <div class="col-md-12">
        <label for="sel1">Select Sub list:</label>
        <div id="get_data">
        </div>

        <div class="form-group">
            <div class="col-md-12">
                <label for="sel1">Select Image:</label>
                <input type="file" name="file" id="file"/>
            </div>
        </div>

    </div>
</div>




<div class="form-group">
    <div class="col-md-12">
<?php echo form_submit('submit', 'Submit', 'class="btn btn-success"'); ?>

        <a href="<?php echo site_url('SecureArea/studies_stories'); ?>"<button class="btn btn-danger">cancel</button></a>
    </div>
</div>

<?php echo form_close(); ?>


<script type="text/javascript">
    jQuery(document).ready(function () {
        $('form').on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });
        ///auto load sub cat                 
        getsubcats("<?php echo $result['insurance']; ?>");

        jQuery("#ins_change").change(function () {
            var ins_val = $(this).val();
            getsubcats(ins_val);
        });

    });

    function getchecked_subcat() {
        var dt_set = "<?php echo $result['ins_ids']; ?>";
        var dt_arr = dt_set.split(',');
        $.each(dt_arr, function (ni, nv) {
            $.each($('#get_data').find('input'), function (i, v) {
                if ($(v).val() == nv) {
                    $(v).click();
                }
            });
        });
    }

    function getsubcats(ins_val)
    {
        if (ins_val == '' || ins_val == '0') {
            $('#get_data').html('');
        } else {
            $.ajax({
                url: '<?php echo base_url(); ?>/index.php/SecureArea/Faqs/subinc_cat',
                data: {ins_val: ins_val},
                type: 'post',
                success: function (data) {
                    $('#get_data').replaceWith(data);
                    getchecked_subcat();
                }
            });
        }
    }
</script> 
