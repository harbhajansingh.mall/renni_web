<?php $this->load->view('header'); ?>

<div id="viewContainer">
	<h1>View / Upload Images</h1>

	<div id="body">
		<form action="<?php echo site_url(); ?>/site/uploadImage/<?php echo $this->uri->segment(3); ?>" method="post" enctype="multipart/form-data">
			<div class="row"><div class="col1"><p>Name : </p></div><div class="col2">
				<select name='nm' id="nm" class='sel-form' onchange='location.replace("<?php echo base_url(); ?>site/images/"+$("#nm").val())'>
					<option value='-'>---Select---</option>
				<?php foreach ($seldata as $selected) { ?>	
					<option value='<?php echo $selected->id ?>' <?php if($selected->id == $this->uri->segment(3)) { ?> selected="selected" <?php } ?>><?php echo $selected->fname.' '.$selected->lname ?></option>
				<?php } ?>
				</select>
			</div></div>
			<div class="row"><div class="col1"><p>Image Title : </p></div><div class="col2"><input type="text" name="imgtitle" class="inp-form" /></div></div>
			<input type="file" name="file" id="file"/><input type="submit" class="submitFile" name="form_submit" /><a href='<?php echo base_url(); ?>site/viewAllContent'><input type="button" class="submitFile" name="form_submit" value="Cancel"/></a>
		</form> 
		<div class="image-gallery">
			<div class="row">
				<?php foreach ($info as $imgdata) { ?>
				<div class="col-gallery"><img src='<?php echo base_url().$imgdata->thumbnailpath; ?>' /></div>
				<a href="<?php echo base_url(); ?>site/deleteImage/<?php echo $this->uri->segment(3); ?>/<?php echo $imgdata->id; ?>"class="close">X</a>
				<?php } ?>
			</div>
			<div class="clear"></div>
		</div>
	</div>

	<p class="footer"></p>
</div>

<?php $this->load->view('footer'); ?>

