<div class="row" id="contactlist">
  <div class="col-md-8"> <h1 style="color:blue">FAQs</h1></div>
  <div class="col-md-4 text-right"> <a href="<?php echo site_url('securearea/FaqsController'); ?>" class="btn btn-default">Add New</a> </div>
 
<div class="col-md-12">
<div class="table-responsive">  
  <table class="table table-default">
    <thead>
      <tr>
        <th>Title</th>
        <th>Detail</th>
        <th>Status</th>
        <th>Ins Type</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
 <?php 
      
foreach ($result as $res) {
  
 ?>
      <tr>
        <td><?php echo $res->title; ?></td>
        <td><?php echo $res->details; ?></td>
        <td><?php  if($res->kpstatus==1) echo 'Active'; else 'Inactive'; ?></td>
        <td><?php echo $res->Ins_Name; ?></td>
      
        <td>
             <a href="<?php echo site_url('securearea/FaqsController/deletedata/' . $res->faqid );?>" class="delete">Delete</a>
             ||
             <a href="<?php echo site_url('securearea/FaqsController/showedit/' . $res->faqid );?>">Edit</a>
        </td>
      </tr> 
<?php } ?>
</tbody>
</table>
</div>
</div>