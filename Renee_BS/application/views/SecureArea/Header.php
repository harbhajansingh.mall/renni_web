<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo isset($title) ? $title : 'Renni'; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

<!--   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->


<!--    <script src="//cdn.ckeditor.com/4.6.1/standard/ckeditor.js"></script>-->

        <link href="<?php echo base_url('css/public/bootstrap.min.css') ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url('css/public/bootstrap-tagsinput.css')?>">
        <link rel="stylesheet" href="<?php echo base_url('css/SecureArea/custom.css'); ?>">
        <script src="<?php echo base_url('js/jquery.js'); ?>"></script>
        <script src="<?php echo base_url('js/bootstrap.min.js') ?>"></script>
        <script src="<?php echo base_url('js/bootstrap-tagsinput.min.js') ?>"></script>


        <script>
            function isconfirm(msg) {

                if (!confirm(msg)) {
                    event.preventDefault();
                    return;
                }
                return true;
            }
        </script>
    </head>

    <body>
<?php $this->load->helper('tabSelect');?>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="<?php echo base_url('imgs/logo.png'); ?>" height="28px"></a>

                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">

                        <li <?php echo selected_tab('active','dashboard',$this->uri->segment(2));?>><a href="<?php echo site_url('securearea/dashboard') ?>">Dashboard <span class="sr-only">(current)</span></a></li>
                        <li <?php echo selected_tab('active','Insurance',$this->uri->segment(2));?>><a href="<?php echo site_url('securearea/Insurance') ?>">Insuarance</a></li>
                        <li <?php echo selected_tab('active','Faqs',$this->uri->segment(2));?>><a href="<?php echo site_url('securearea/Faqs/show') ?>">Faq's</a></li>
                      <!-- <li><a href="<?php echo site_url('securearea/KeypointController') ?>">Key Points</a></li>
                       <li><a href="<?php echo site_url('securearea/LifestoryCont') ?>">Real Stories</a></li>-->

                        <li <?php echo selected_tab('active','AuthorsController',$this->uri->segment(2));?>><a href="<?php echo site_url('securearea/AuthorsController/authors_show') ?>">Authors</a></li>
                        <li <?php echo selected_tab('active','News',$this->uri->segment(2));?>><a href="<?php echo site_url('securearea/News') ?>">News</a></li>
                        <li <?php echo selected_tab('active','Blog',$this->uri->segment(2));?>><a href="<?php echo site_url('securearea/Blog') ?>">Blog</a></li>
                        <li <?php echo selected_tab('active','Studies_Stories',$this->uri->segment(2));?>><a href="<?php echo site_url('securearea/Studies_Stories') ?>">Case studies & Stories</a></li>
                        <li <?php echo selected_tab('active','Contactus',$this->uri->segment(2));?>><a href="<?php echo site_url('securearea/Contactus') ?>">Contact us query</a></li>
                        <li <?php echo selected_tab('active','Subscriber',$this->uri->segment(2));?>><a href="<?php echo site_url('securearea/Subscriber') ?>">Subscriber</a></li>
                        <li <?php echo selected_tab('active','Questions',$this->uri->segment(2));?>><a href="<?php echo site_url('securearea/Questions') ?>">Questions</a></li>

                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">Welcome <?php echo $_SESSION['username'] ?></a></li>
                        <li><a href="<?php echo site_url('Login/logout'); ?>">Logout</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        <div class="container">
            <?php if ($this->session->flashdata('msg') != '') { ?>
                <div class="row">    
                    <div class="col-md-12 alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <p><?php echo $this->session->flashdata('msg'); ?></p>  
                    </div>
                </div>
                <?php
            }?>