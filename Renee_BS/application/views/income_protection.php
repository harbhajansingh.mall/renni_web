
	<!-- Banner Part -->
	<section class="income-top-section">
		<div class="container">			
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
					<div class="banner-text-inner">
						<div class="take-to">
							TAKE THE PATH TO
						</div>
						<div class="today_icome">
							PROTECTION <span>TODAY.</span>
						</div>
						<div class="request-now">
							<a href="<?php echo site_url('contactus') ?>">REQUEST Now</a>
						</div>
					</div>					
				</div>
			</div> 
		</div>
	</section>
	<!-- //////////// Banner Part //////////// -->
		
	<!-- Section 02 -->
	<section class="section-02">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
					<?php $this->load->view('left_panel_inner_ins');  ?>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
					<div class="real-title01">WHAT IS INCOME PROTECTION?</div>
					<div class="border-blueinr"></div>
					<div class="tab-content">Income protection helps protect your assets and standard of living should you be unable to work due to accident or illness. It pays you up to 75% of your salary as a monthly income for a fixed period of time. </div>
				</div>
			</div>
			<div class="row top-space">
				<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">
					<div class="watch"><img src="<?php echo base_url('assets/img/inner-img/income_60_per.png')?>" alt="" /></div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-7 col-xs-12">
					<div class="real-title01">HOW DOES INCOME PROTECTION HELP?</div>
					<div class="border-blueinr"></div>
					<div class="tab-content01">Income protection provides you with a replacement income to replace your  primary income, regardless of what your injury or illness is or how long you are out of work (as determined by your policy).					
					</div>
					<div class="protection_Tiele">Income protection helps you:</div>
					<div class="icn_space_Icome">
						<ul class="banner-list_Icome">
							<li><i class="fa fa-check-circle" aria-hidden="true"></i>Meet immediate financial obligations</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i>Fund mortgage repayments & protect your assets </li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i>Pay living expenses while reamining solvent</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i>Keep your money in the bank – where it belongs!</li>
						</ul>
					</div>
					<div class="box-future text-center">
						<div class="future-title">PROTECT YOUR WEALTH </div>
						<div class="getsefl">Protect your wealth and take the time you need to recover.<br>
Cover yourself for as little as <span>$15 per month.</span>
						</div>
						<div class="get-button"><a href="">GET INSURED</a></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- //////////// section 02 //////////// -->
		
	<!-- section 03 REAL LIFE STORIES-->
	<section class="section-03">
		<div class="container">			
			<div class="row top-space">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
						<div class="real-title">
							Income Protection <br><strong>Keith is diagnosed with a brain tumor </strong>
							<div class="border-blue"></div>
						</div>
					</div>
					<div class="testimonial-inner02"> 						 
						<div class="t-box-space">							
							<span class="mau-text">Keith (Mr. Wellness), collapsed unexpectedly at work – he was later diagnosed with a brain tumor. Hear how income protection kept Keith’s business solvent while he recuperated from neurosurgery. 
								<br><br></span>
							<div class="row mau-img tp-sp">
								<span class="col-md-9 user-text">
									<!-- <img src="assets/img/inner-img/img03.png" alt=""> -->
									<span class="frog-desi">
									<strong>Keith (47)</strong><br>
									Physiotherapist<br>Self employed
									</span>
								</span>
							</div>
						</div>
				   </div>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">                    
					<div class="testimonial-inner">  
						<div class="quote-img">
							<div class="quate-icon"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
						</div>
						<div class="t-img_icome">
							<a href="javascript:void(0);" class='click' value="QH03CgaGk3I">
								<img src="<?php echo base_url('assets/img/inner-img/Icome_T_v.png')?>" alt="" class="img-responsive" />
							</a>
						</div>						
				   </div>
				</div>
			</div>			
		</div>
	</section>
	<!-- //////////// section 03 //////////// -->
	<!-- section 03 REAL LIFE STORIES-->
	<section class="section-04">
		<div class="container">
			<!-- Section 02 -->
			<div id="carousel-example-generic01" class="carousel slide" data-ride="carousel">				
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="chances text-center">
									HOW WOULD YOU MAINTAIN YOUR STANDARD OF LIVING?
									<div class="border-white"></div>
								</div>
								<div class="text-center what-content">The weekly household income is reduced by half where the main financial provider is temporarily unable to work, or the supporting partner becomes disabled or dies.1</div>
							</div>
						</div>
					</div>
					
					<div class="item">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="chances text-center">
									What are your chances?
									<div class="border-white"></div>
								</div>
								<div class="text-center what-content">Cardiovascular disease (CVD) is a major cause of death in Australia. There were  43,603 deaths attributed to CVD in Australia in 2013.  On average, one Australian dies as a result of CVD every 12 minutes.2  </div>							
							</div>
						</div>
					</div>										
				</div>
				<div class="col-md-8 col-md-offset-2 text-center">
					<span class="next-pre-button-in">
						<a class="carousel-control01 next-button-in" href="#carousel-example-generic01" role="button" data-slide="next">
							&gt;
						</a>
						<a class="carousel-control01 prev-button-in" href="#carousel-example-generic01" role="button" data-slide="prev">
							&lt;
						</a>
					</span>
				</div>
			</div>	
		</div>
	</section>
	<div class="text-center down-arrow">
		<img src="<?php echo base_url('assets/img/inner-img/down-arrow.png')?>" alt="" />
	</div>
	
	<section class="references-section">
		<div class="container">
			<div class="col-md-12">
				<span class="more-plus">
					<a href="" class="">References
						<span class="plus"><i class="fa fa-plus"></i></span>
					</a>
				</span>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="male-female">
						SPENDING TWICE AS MUCH ON HEALTH? 
					</div>
					<div class="male-img">
						<img src="<?php echo base_url('assets/img/inner-img/Icome_chart.png')?>" class="img-responsive" alt="" />
					</div>
				</div>
				<!-- Case Studies Slider -->
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">	
					<div class="real-title-inr">
						Case Studies<br><strong>HOW  INCOME PROTECTION HELPS</strong>
					</div>
					<div id="carousel-example-generic-t" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner" role="listbox">
							<div class="item active">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
									<div class="case-studies">
										<div class="tom-title">
											<img src="<?php echo base_url('assets/img/inner-img/Icome_Help_01.png')?>" />
											MARY SUFFERS A SERIES OF STROKES
											<div class="top-content">
											Mary suffered a stroke as a result of a blood clot becoming lodged in a vein inside her brain. She spent... 
											</div>
											<div class="full-story"><a href="">Full Story</a></div>
											<div>
												<span class="next-pre-button-in-dummy">
													<a href="" class="next-button-in-dummy">&nbsp;</a>
													<a href="" class="prev-button-in-dummy">&nbsp;</a>
												</span>
											</div>
										</div>
										<!-- <span><img src="assets/img/inner-img/i.png" alt="test" /></span> -->
									</div>
									<div class="case-arrow">
										<img src="<?php echo base_url('assets/img/inner-img/case-arrow.png')?>" alt="test" />
									</div>
									<div class="case-name">
										MARY (37)<br><span>, Nurse</span>
									</div>
								</div>
							</div>					
							<div class="item">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
									<div class="case-studies">
										<div class="tom-title">
											<img src="<?php echo base_url('assets/img/inner-img/Icome_Help_01.png')?>" />
											MARY SUFFERS A SERIES OF STROKES
											<div class="top-content">
											Mary suffered a stroke as a result of a blood clot becoming lodged in a vein inside her brain. She spent... 
											</div>
											<div class="full-story"><a href="">Full Story</a></div>
											<div>
												<span class="next-pre-button-in-dummy">
													<a href="" class="next-button-in-dummy">&nbsp;</a>
													<a href="" class="prev-button-in-dummy">&nbsp;</a>
												</span>
											</div>
										</div>
										<!-- <span><img src="assets/img/inner-img/i.png" alt="test" /></span> -->
									</div>
									<div class="case-arrow">
										<img src="<?php echo base_url('assets/img/inner-img/case-arrow.png')?>" alt="test" />
									</div>
									<div class="case-name">
										MARY (37)<br><span>, Nurse</span>
									</div>
								</div>
							</div>										
						</div>
						<div class="col-md-6 col-md-offset-2 text-center">
							<span class="next-pre-button-in-t">
								<a class="carousel-control01 next-button-in" href="#carousel-example-generic-t" role="button" data-slide="next">
									&gt;
								</a>
								<a class="carousel-control01 prev-button-in" href="#carousel-example-generic-t" role="button" data-slide="prev">
									&lt;
								</a>
							</span>
						</div>
					</div>
				</div>	
				<!-- Case Studies Slider -->
			</div>
			<div class="row top-space02">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="renni-inner">
						<span class="protection-icon">
							<img src="<?php echo base_url('assets/img/inner-img/Icome_Icon01.png')?>" alt="test" />
						</span>
						<span class="protection-text">RENNI LIFE INSURANCE</span><br>
						Protect your family when they need you most. Get life Insurance today.
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="renni-inner">
						<span class="protection-icon">
							<img src="<?php echo base_url('assets/img/inner-img/Icome_Icon02.png')?>" alt="test" />
						</span>
						<span class="protection-text">SAVE WITH INCOME PROTECTION </span><br>
						Cover yourself with a Renni income protection policy and receive a 10% discount on your first year’s premium.
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!-- section 06 -->
    <section class="section-06-inr">
        <div class="container-fluid">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
				<div class="first-box01_Icome">
					<div class="tool-title-inr">
						<span>Understand the risks?</span><br>
						Renni Facts & Figures
					</div>
					<div class="border-white02"></div>
					<div class="tool-content-inr_Icome">
						18% of all strokes in Australia happen to young people for whom strokes are particularly damaging. Patients are often unable to resume independent living or return to the workforce. 5 
					</div>
					<div class="discover-button">
					   <a href="">LEARN MORE </a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
				<div class="first-box02_Icome">
					<div class="tool-title-inr">
						<span>How would you manage?</span><br>
						THIRD-PARTY PUBLCATIONS
					</div>
					<div class="border-white02"></div>
					<div class="tool-content-inr_Icome">
						Discover different perspectives of how industry, government and social organizations are tackling critical health and welfare issues in Australia. 
					</div>
					<div class="discover-button">
					   <a href="">UNDERSTAND</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
				<div class="first-box03_Icome">
					<div class="tool-title-inr">
						<span>How much will you pay?</span><br>
						THE COST OF INCOME PROTECTION
					</div>
					<div class="border-white02"></div>
					<div class="tool-content-inr_Icome">
						A standard policy (waiting period of 90 days) for a 30 year-old non-smoker (with no lifestyle or work-related risk factors) earning $80,000 p.a. can cost as little as $50 to $80 per month.<a href="" title="Based on a standard non-smoker, income protection policy for a 30 year male or female with a waiting period of 90 days and a benefit of $5,000 p.m. payable to age 65. The calculation is based on policy holders having a white-collar occupation. It also includes males employed in certain light, blue-collar occupations. The premium is affected by occupation, sex, age and smoking status.">*</a>
					</div>
					<div class="discover-button">
					   <a href="">REQUEST A QUOTE</a>
					</div>
				</div>
			</div>
        </div>    
    </section>
	<!-- //////////// section 06 //////////// -->
	
	<!-- section 05 -->
	<section class="section-05">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="real-title"><strong>RENNI FAQ’s. InCOME PROTECTION</strong>
						<div class="border-blue"></div>
					</div>
				</div>
			</div>
			<div class="row panel-group">
				<div class="col-md-6">
					<!-- Tabs --> 
					<div class="form-tabs" id="accordion-f" role="tablist" aria-multiselectable="true">
						<?php get_faq_by_insurance(5); ?>
					</div>
					<!-- Tabs ////////// -->
					<div class="more-button">
						<a href="<?php echo site_url('faq') ?>">More FAQ‘s &nbsp; <img src="<?php echo base_url('assets/img/more-arrow.png')?>" alt="" /></a>
					</div>
				</div>
				<div class="col-md-6">
					<?php $this->load->view('ask_form'); ?>				
				</div>
			</div>
			<div class="row">				
				<div id="carousel-example-generic03" class="carousel slide" data-ride="carousel">	
					<div class="faqs">
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="did-you">
								<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
									<div class="did-title">Did you know?</div>
									<div class="getsefl-inr">Income protection is designed to supplement trauma insurance and total and permanent disability (TPD) insurance. Its focus is providing you with a replacement income. 
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="get-inr"><a href="">GET INSURED</a></div>
								</div>
							</div>
						</div>					
						<div class="item">
							<div class="did-you">
								<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
									<div class="did-title">Did you know?</div>
									<div class="getsefl-inr">Income protection is designed to supplement trauma insurance and total and permanent disability (TPD) insurance. Its focus is providing you with a replacement income. 
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="get-inr"><a href="">GET INSURED</a></div>
								</div>
							</div>
						</div>										
						</div>										
					</div>					
					<div class="col-md-12 clearfix">
						<span class="next-pre-button-in-03">
							<a class="carousel-control01 next-button-in-03" href="#carousel-example-generic03" role="button" data-slide="next">
								&gt;
							</a>
							<a class="carousel-control01 prev-button-in-03" href="#carousel-example-generic03" role="button" data-slide="prev">
								&lt;
							</a>
						</span>
					</div>				
				</div>
			</div>
		</div>
	</section>
	<!-- //////////// section 05 //////////// -->
	
	<!-- //////////// section 05 //////////// -->
	<?php $this->load->view('casestudies_news_4more');  ?>
	<!-- //////////// section 05 //////////// -->
	<section class="logo-section">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
					<?php $this->load->view('all_logo');  ?>
				</div>
			</div>
		</div>
	</section>
	
	
    <!-- section 08 -->
    <section class="section-08-iner">
    	<div class="section-img-inner">
			<div class="container">
				<div class="row">            
					<div class="col-lg-7 col-sm-6 discover-t_not">Not sure about something? </div>
					<div class="col-lg-5 col-sm-6 two-button-inr text-right">
						<a href="javascript:void(0);" class="button-request-inr">REQUEST ASSISTANCE</a>
					</div>
				</div>    
			</div>
        </div>
    </section> 
    <!-- //////////// section 08 //////////// -->
        