<!-- Banner Part -->
	<section class="disability_top_section">
		<div class="container">			
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
					<div class="banner-text-inner">
						<div class="take-to">
							TAKE THE PATH TO
						</div>
						<div class="today_Trauma">
							PROTECTION <span>TODAY.</span>
						</div>
						<div class="request-now">
							<a href="<?php echo site_url('contactus') ?>">REQUEST Now</a>
						</div>
					</div>					
				</div>
			</div> 
		</div>
	</section>
	<!-- //////////// Banner Part //////////// -->
		
	<!-- Section 02 -->
	<section class="section-02">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
					<?php $this->load->view('left_panel_inner_ins');  ?>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
					<div class="real-title01">WHAT IS PERMANENT DISABILITY INSURANCE?</div>
					<div class="border-blueinr"></div>
					<div class="tab-content">Permanent disability insurance pays you a lump sum should you become totally or permanently disabled. It provides you with a source of revenue when you are unlikely ever to work again, either in your field of expertise or at all. It is also referred to as total and permanent disability insurance (TPD)</div>
				</div>
			</div>
			<div class="row top-space">
				<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">
					<div class="watch"><img src="<?php echo base_url('assets/img/inner-img/Disability_img.png')?>" alt="" class="img-responsive" /></div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-7 col-xs-12">
					<div class="real-title01">HOW DOES INCOME PROTECTION HELP?</div>
					<div class="border-blueinr"></div>
					<div class="tab-content01">Coping with the onset of a permanent disability is traumatic and living with one is a life-long challenge. A disability payout can be used to:  
					</div>
					<div class="icn_space_Icome">
						<ul class="banner-list_Trauma">
							<li style="line-height:20px;"><i class="fa fa-check-circle" aria-hidden="true"></i>Implement major lifestyle changes such as modifying your living environment for reduced mobility</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i>Help pay for medical expenses and the cost of care</li>
							<li><i class="fa fa-check-circle" aria-hidden="true"></i>Help compensate for loss of income </li>
						</ul>
					</div>
					<div class="box-future text-center">
						<div class="future-title">MAKE THE MOST OF LIFE!</div>
						<div class="getsefl">Permanent disability cover can cost<br>as little as <span>$20 per month.</span>
						</div>
						<div class="get-button"><a href="">GET INSURED</a></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- //////////// section 02 //////////// -->
		
	<!-- section 03 Case Studies-->
	<section class="section-03">
		<div class="container">			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="real-title-inr">
						Case Studies<br><strong>HOW PERMANENT DISABILITY INSURANCE HELPS.</strong>
					</div>
				</div>
				<div id="carousel-example-generic-d" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="col-md-10 col-sm-10 col-xs-10 col-md-offset-1">				
								<div class="case-studies_Disability">
									<div class="tom-title02">
										<div class="col-md-2"><img src="<?php echo base_url('assets/img/inner-img/Trauma_tM.png')?>" /></div>
										<div class="col-md-10 real-title-inr02">MARK SUFFERS A SERIOUS FACIAL INJURY 
											<div class="border-blue02"></div>
										</div>										
									</div>	
									<div class="top-content02">
										<i>Mark, a 35 year old sales executive, sold mining and excavating equipment throughout Australia. He was involved in a car accident coming back from a site visit that resulted in serious facial injuries and was left with a permanent speech impediment.</i>
									</div>
									<div class="full-story"><a href="">Full Story</a></div>
									<div class="case-name02 text-right">
										Mark (35)<br><span>Mining sales executive</span>
									</div>
										<div>
											<span class="next-pre-button-in-dummy">
												<a href="" class="next-button-in-dummy">&nbsp;</a>
												<a href="" class="prev-button-in-dummy">&nbsp;</a>
											</span>
										</div>
									
									<!-- <span><img src="assets/img/inner-img/i.png" alt="test" /></span> -->
								</div>								
								
							</div>
						</div>
						<div class="item">
							<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 col-md-offset-1">								
								<div class="case-studies_Disability">
									<div class="tom-title02">
										<div class="col-md-2"><img src="<?php echo base_url('assets/img/inner-img/Trauma_tM.png')?>" /></div>
										<div class="col-md-10 real-title-inr02">MARK SUFFERS A SERIOUS FACIAL INJURY 
											<div class="border-blue02"></div>
										</div>										
									</div>	
									<div class="top-content02">
										<i>Mark, a 35 year old sales executive, sold mining and excavating equipment throughout Australia. He was involved in a car accident coming back from a site visit that resulted in serious facial injuries and was left with a permanent speech impediment.</i>
									</div>
									<div class="full-story"><a href="">Full Story</a></div>
									<div class="case-name02 text-right">
										Mark (35)<br><span>Mining sales executive</span>
									</div>
										<div>
											<span class="next-pre-button-in-dummy">
												<a href="" class="next-button-in-dummy">&nbsp;</a>
												<a href="" class="prev-button-in-dummy">&nbsp;</a>
											</span>
										</div>
								</div>								
								
							</div>
						</div>				
												
					</div>
					<div class="col-md-12 text-center">
						<span class="next-pre-button-in-d">
							<a class="carousel-control01 next-button-in_d" href="#carousel-example-generic-d" role="button" data-slide="next">
								&gt;
							</a>
							<a class="carousel-control01 prev-button-in_d" href="#carousel-example-generic-d" role="button" data-slide="prev">
								&lt;
							</a>
						</span>
					</div>
				</div>
				<!-- Case Studies Slider -->
			</div>			
		</div>
	</section>
	<!-- //////////// section 03 //////////// -->
	<!-- section 03 REAL LIFE STORIES-->
	<section class="section-04">
		<div class="container">
			<!-- Section 02 -->
			<div id="carousel-example-generic01" class="carousel slide" data-ride="carousel">				
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="chances_Trauma text-center">
									IT COULDN’T HAPPEN TO ME, RIGHT?
									<div class="border-white"></div>
								</div>
								<div class="text-center what-content">The majority of trauma claims over the five year period 2010 – 2014 were the result of unexpected medical issues.1</div>
							</div>
						</div>
					</div>
					
					<div class="item">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="chances_Trauma text-center">
									IT COULDN’T HAPPEN TO ME, RIGHT?
									<div class="border-white"></div>
								</div>
								<div class="text-center what-content">The majority of trauma claims over the five year period 2010 – 2014 were the result of unexpected medical issues.1</div>
							</div>
						</div>
					</div>										
				</div>
				<div class="col-md-8 col-md-offset-2 text-center">
					<span class="next-pre-button-in">
						<a class="carousel-control01 next-button-in" href="#carousel-example-generic01" role="button" data-slide="next">
							&gt;
						</a>
						<a class="carousel-control01 prev-button-in" href="#carousel-example-generic01" role="button" data-slide="prev">
							&lt;
						</a>
					</span>
				</div>
			</div>	
		</div>
	</section>
	<div class="text-center down-arrow">
		<img src="<?php echo base_url('assets/img/inner-img/down-arrow.png')?>" alt="" />
	</div>
	
	<section class="references-section">
		<div class="container">
			<div class="col-md-12">
				<span class="more-plus">
					<a href="" class="">References
						<span class="plus"><i class="fa fa-plus"></i></span>
					</a>
				</span>
			</div>
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">
					<div class="male_female_Disability text-center">
						Australians <span>affected</span> by a <br><span>disability</span> in 2012 
					</div>
					<div class="male-img">
						<img src="<?php echo base_url('assets/img/inner-img/Disability_2012.png')?>" class="img-responsive" alt="" />
					</div>
				</div>
				<!-- Case Studies Slider -->
				<div class="col-lg-6 col-md-6 col-sm-7 col-xs-12">	
					<div class="real-title-inr">
						<img src="<?php echo base_url('assets/img/inner-img/Disability_video.png')?>" class="img-responsive" />
					</div>
					<div class="Video_CNT">
						<span>Lorem Ipsum is simply dummy text of  the <br>printing and typesetting industry.</span>
						<div class="Date_TPD">
							24 January, 2016  |   TPD   |   24 Comments
						</div>
					</div>
					<div class="tab-content01">In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, in ornare massa enim at lorem. Etiam risus diam, porttitor vitae ultrices quis, dapibus id dolor. Morbi venenatis lacinia rhoncus.
					</div>
				</div>	
				<!-- Case Studies Slider -->
			</div>
			<div class="row top-space02">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="renni-inner">
						<span class="protection-icon">
							<img src="<?php echo base_url('assets/img/inner-img/Icome_Icon01.png')?>" alt="test" />
						</span>
						<span class="protection-text">RENNI LIFE INSURANCE</span><br>
						Help your family when they need it most.
					</div>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<div class="renni-inner">
						<span class="protection-icon">
							<img src="<?php echo base_url('assets/img/inner-img/Disability_icon.png')?>" alt="test" />
						</span>
						<span class="protection-text">SAVE WITH DISABILITY INSURANCE </span><br>
						Cover yourself with Renni permanent disability Insurance and receive a 10% discount on your first year’s premium.
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!-- section 06 -->
    <section class="section-06-inr">
        <div class="container-fluid">
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
				<div class="first-box01_Icome">
					<div class="tool-title-inr">
						<span>Why is it relevant for you?</span><br>
						CASE STUDIES
					</div>
					<div class="border-white02"></div>
					<div class="tool-content-inr_Icome">
						While you normally recover from a serious  illness  a permanent disability will affect you for life. Everybody should consider some form of disability insurance.
					</div>
					<div class="discover-button">
					   <a href="<?php echo site_url('Casestudies') ?>">DISCOVER</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
				<div class="first-box02_Icome">
					<div class="tool-title-inr">
						<span>Have you considered?</span><br>
						FACTS & FIGURES
					</div>
					<div class="border-white02"></div>
					<div class="tool-content-inr_Icome">
						60% of people with a disability need assistance with at least one daily activity.
					</div>
					<div class="discover-button">
					   <a href="">LEARN MORE</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
				<div class="first-box03_Icome">
					<div class="tool-title-inr">
						<span>Can I afford it?</span><br>
						THE COST OF DISABILITY INSURANCE
					</div>
					<div class="border-white02"></div>
					<div class="tool-content-inr_Icome">
						A permanent disability policy for a 30 year-old  Australian male, with no pre-existing risk factors & working in an occupation that is not deemed as high risk could cost as little as $25 per month.
					</div>
					<div class="discover-button">
					   <a href="">REQUEST A QUOTE</a>
					</div>
				</div>
			</div>
        </div>    
    </section>
	<!-- //////////// section 06 //////////// -->
	
	<!-- section 05 -->
	<section class="section-05">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="real-title"><strong>RENNI FAQ’s. <br>PERMANENT DISABILITY INSURANCE </strong>
						<div class="border-blue"></div>
					</div>
				</div>
			</div>
			<div class="row panel-group">
				<div class="col-md-6">
					<!-- Tabs --> 
					<div class="form-tabs" id="accordion-f" role="tablist" aria-multiselectable="true">
						<?php get_faq_by_insurance(4); ?>						
					</div>
					<!-- Tabs ////////// -->
					<div class="more-button">
						<a href="<?php echo site_url('faq') ?>">More FAQ‘s &nbsp; <img src="<?php echo base_url('assets/img/more-arrow.png')?>" alt="" /></a>
					</div>
				</div>
				<div class="col-md-6">
					<?php $this->load->view('ask_form'); ?>				
				</div>
			</div>
			<div class="row">				
				<div id="carousel-example-generic03" class="carousel slide" data-ride="carousel">	
					<div class="faqs">
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="did-you">
								<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
									<div class="did-title">Did you know?</div>
									<div class="getsefl-inr">The average 30 year old Australian making $75,000 will earn approximately $2.6 million between now and when they retire at 65.5
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="get-inr"><a href="">GET INSURED</a></div>
								</div>
							</div>
						</div>					
						<div class="item">
							<div class="did-you">
								<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
									<div class="did-title">Did you know?</div>
									<div class="getsefl-inr">The average 30 year old Australian making $75,000 will earn approximately $2.6 million between now and when they retire at 65.5
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<div class="get-inr"><a href="">GET INSURED</a></div>
								</div>
							</div>
						</div>										
						</div>										
					</div>					
					<div class="col-md-12 clearfix">
						<span class="next-pre-button-in-03">
							<a class="carousel-control01 next-button-in-03" href="#carousel-example-generic03" role="button" data-slide="next">
								&gt;
							</a>
							<a class="carousel-control01 prev-button-in-03" href="#carousel-example-generic03" role="button" data-slide="prev">
								&lt;
							</a>
						</span>
					</div>				
				</div>
			</div>
		</div>
	</section>
	<!-- //////////// section 05 //////////// -->
	
	<!-- //////////// section 05 //////////// -->
	<?php $this->load->view('casestudies_news_4more');  ?>
	<!-- //////////// section 05 //////////// -->
	<section class="logo-section">
		<div class="container text-center">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
				<?php $this->load->view('all_logo');  ?>
					
				</div>
			</div>
		</div>
	</section>
	
	
    <!-- section 08 -->
    <section class="section-08-iner">
    	<div class="section-img-inner">
			<div class="container">
				<div class="row">            
					<div class="col-lg-7 col-sm-6 discover-t_not">Not sure about something? </div>
					<div class="col-lg-5 col-sm-6 two-button-inr text-right">
						<a href="javascript:void(0);" class="button-request-inr">REQUEST ASSISTANCE</a>
					</div>
				</div>    
			</div>
        </div>
    </section> 
    <!-- //////////// section 08 //////////// -->
