<!-- Footer Section 09 -->
<footer class="footer-section">
    <div class="container">
        <div class="row">            
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="footer-logo">
                    <img src="<?php echo base_url('assets/img/footer-logo.png') ?>" alt="" /><br>
                    <span>Renni Insurance Pty Ltd </span>
                </div>
                <div class="footer-content">
                    This insurance is organised by Renni Insurance Pty Ltd trading as Renni Personal Insurance (ABN 31 002 317 608, AFSL 352 522). All content and information on this website has been prepared without taking into account any of your personal circumstances, objectives or personal needs. 
                    You should read the product disclosure statement (PDS) of the policy or policies you are interested in before making a decision to apply for insurance to ensure that the cover meet your needs. 
                    Any content on this website does not constitute independent: financial, legal, and/or taxation advice. 

                </div>
                <div class="download">
                    Click to view or download a copy of our <a href="#"><span>Product Disclosure Statements</span></a> or <a href="#"><span>Financial Services Guide</span></a>.
                </div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-3 col-xs-12">
                <div class="insr">
                    INSURANCES
                </div>
                <div class="footer-link-one">
                    <ul class="footer-link">
                        <li><a href="<?php echo site_url('Lifeinsurance') ?>" >Life Insurance</a></li>
                        <li><a href="<?php echo site_url('Incomeprotection') ?>">Income Protection</a></li>
                        <li><a href="<?php echo site_url('Disabilityinsurance') ?>">Disability Insurance</a></li>
                        <li><a href="<?php echo site_url('Traumainsurance') ?>">Trauma Insurance</a></li>
                        <!--  <li><a href="#">Terms & Policies</a></li> -->
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-3 col-xs-12">
                <div class="insr">
                    useful links
                </div>
                <div class="footer-link-one">
                    <ul class="footer-link">
                        <li><a href="#">About Us</a></li>
                        <li><a href="<?php echo site_url('contactus') ?>">Contact Us</a></li>                        
                        <li><a href="<?php echo site_url('faq') ?>">FAQ’s</a></li>

                        <li><a href="<?php echo site_url('Casestudies') ?>">Real Stories</a></li>
                        <li><a href="<?php echo site_url('blogs') ?>">Renni Blog</a></li>                        
                        <li><a href="#">Third-party Publications</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-md-6 col-sm-6 col-xs-12">
                <div class="insr">
                    SUBSCRIBE TO OUR NEWSLETTER
                </div>
                <div class="news-link">
                    <form>
                        <input required="required" type="email" placeholder="Enter your email here" name="sub_mail" id="sub_mail" class="news-input" />
                        <input id="submit" type="submit" value="SUBSCRIBE NOW" name="save" onclick="return subscribe()" class="button-input" />
                    </form>
                    <div id="responsedv" style="font-size: 14px;color: #21c2f8;font-weight: 600;width: 100%;padding-top: 10px;"></div>
                </div>
            </div>
        </div>
    </div>    
</div>
<div class="footer-icon">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-9 col-sm-6 col-xs-12">
                <div class="all-right01">
                    © 2016 Renni Insurance Pty Ltd. ACN 002 519 208. All rights reserved.<br>
                    <a href="#"> <span>Privacy Policy<span></a> &nbsp;&nbsp; <a href="#"><span> Website Terms of Use</span></a>
                                </div>
                                <div class="all-right02">

                                </div>
                                </div>
                                <div class="col-lg-4 col-md-3 col-sm-6 col-xs-12 text-right">
                                    <div class="footer-icon-link">
                                        <a href="">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                        <a href="">
                                            <i class="fa fa-google-plus"></i>
                                        </a>                            
                                    </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                </footer>
                                <!-- //////////// Footer Section 09 //////////// -->


                                
                                <!-- Bootstrap Core JavaScript -->
                                <script src="<?php echo base_url('js/bootstrap.min.js') ?>"></script>
                                <script type="text/javascript" src="<?php echo base_url('js/youtubevideo.js') ?>"></script>
                                <script src="<?php echo base_url('js/jquery.flexisel.js') ?>"></script>
                                <!-- Collapse Tabs -->
                                <script type="text/javascript">
                            function toggleIcon(e) {
                                $(e.target).prev('.panel-heading').find(".more-less").toggleClass('glyphicon-plus glyphicon-minus');
                            }
                            $('.panel-group').on('hidden.bs.collapse', toggleIcon);
                            $('.panel-group').on('shown.bs.collapse', toggleIcon);
                                </script>
                                <!-- /////////// Collapse Tabs /////////// -->

                                <!-- Mobile Slider -->
                                <script>
                                    $(window).load(function () {
                                        $("#services-scroll").flexisel({
                                            visibleItems: 1,
                                            animationSpeed: 350,
                                            autoPlay: true,
                                            autoPlaySpeed: 3000,
                                            pauseOnHover: false,
                                            enableResponsiveBreakpoints: true,
                                            responsiveBreakpoints: {
                                                portrait: {
                                                    changePoint: 320,
                                                    visibleItems: 1
                                                },
                                                landscape: {
                                                    changePoint: 640,
                                                    visibleItems: 1
                                                },
                                                tablet: {
                                                    changePoint: 768,
                                                    visibleItems: 2
                                                }
                                            }
                                        });
                                    });
                                    function subscribe() {
                                        var emailid = $('#sub_mail').val();
                                        if(emailid==''){
                                            alert('Please enter your email id.');
                                            return false;
                                        }
                                        $.ajax({
                                            url: '<?php echo base_url(); ?>index.php/Home/subscribe',
                                            data: {'emailid': emailid},
                                            type: 'post',
                                            success: function (data) {
                                                $('#sub_mail').val('');
                                                $('#responsedv').html(data)
                                            }
                                        });
                                        return false;
                                    }
                                </script>
                                <!-- ///////////// Mobile Slider ///////////// -->

                                <!-- Top Navigation Menu -->
                                <script>
                                    $(function () {

                                        $('li.dropdown > a').on('click', function (event) {
                                            event.preventDefault()
                                            $(this).parent().find('ul').first().toggle(300);
                                            $(this).parent().siblings().find('ul').hide(200);
                                            //Hide menu when clicked outside
                                            $(this).parent().find('ul').mouseleave(function () {
                                                var thisUI = $(this);
                                                $('html').click(function () {
                                                    thisUI.hide();
                                                    $('html').unbind('click');
                                                });
                                            });

                                        });

                                    });
                                </script>
                                <!-- /////////////////  Top Navigation Menu ///////////////// -->



                                </body>
                                </html>