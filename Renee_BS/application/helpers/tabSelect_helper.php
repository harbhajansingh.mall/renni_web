<?php

function selected_tab($class, $select, $current_controller) {
    if (strtolower($select) == strtolower($current_controller)) {
        return 'class="' . $class . '"';
    }
}

function front_selected_tab($class, $select_arr, $current_controller) {
    if (in_array(strtolower($current_controller), $select_arr)) {
        return 'class="' . $class . '"';
    }
}

function faq_selected_subtab($class, $select, $current_controller) {
    if (strtolower($select) == strtolower($current_controller)) {
        return 'class="' . $class . '"';
    }
}

///get faq for all insurance pages
function get_faq_by_insurance($id) {
    $CI = & get_instance();
    $CI->db->select('*');
    $CI->db->from('faqs');
    $CI->db->where('find_in_set(' . $id . ',Ins_Ids)!=', 0);
    $CI->db->order_by('faqid', 'desc');
    $query = $CI->db->get();
    if ($query->num_rows() > 0) {
        $rows = $query->result();
        foreach ($rows as $key => $row):
            if ($key == 0) {
                $in = 'in';
                $sign = 'glyphicon-minus';
            } else {
                $in = '';
                $sign = 'glyphicon-plus';
            }
            ?>
            <div class="panel panel-default">
                <div class="panel-heading tab-bg-clr01" role="tab" id="heading<?php echo $row->faqid; ?>-f">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion-f" href="#collapse<?php echo $row->faqid; ?>-f" aria-expanded="true" aria-controls="collapse<?php echo $row->faqid; ?>-f" class="collapsed">
                            <i class="more-less glyphicon <?php echo $sign; ?> bg-clr01"></i>
                            <?php echo $row->question; ?>
                        </a>
                    </h4>
                </div>
                <div id="collapse<?php echo $row->faqid; ?>-f" class="panel-collapse collapse <?php echo $in; ?>" role="tabpanel" aria-labelledby="heading<?php echo $row->faqid; ?>-f" aria-expanded="false" >
                    <div class="panel-body f-text">
                        <?php echo $row->answer; ?>
                    </div>
                </div>
            </div>
        <?php
        endforeach;
    }

    //print_r($result);exit;
}
