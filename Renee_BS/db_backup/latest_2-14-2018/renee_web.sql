-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 14, 2018 at 04:36 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `renee_web`
--

-- --------------------------------------------------------

--
-- Table structure for table `admindet`
--

CREATE TABLE IF NOT EXISTS `admindet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usrnm` varchar(100) NOT NULL,
  `pswd` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admindet`
--

INSERT INTO `admindet` (`id`, `usrnm`, `pswd`) VALUES
(1, 'admin', '100');

-- --------------------------------------------------------

--
-- Table structure for table `author_master`
--

CREATE TABLE IF NOT EXISTS `author_master` (
  `author_id` int(11) NOT NULL,
  `author_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `author_master`
--

INSERT INTO `author_master` (`author_id`, `author_name`) VALUES
(1, 'aaaa'),
(2, 'makaka jkk'),
(3, 'Aman'),
(4, 'dd'),
(0, 'hhhhh');

-- --------------------------------------------------------

--
-- Table structure for table `casestudies_stories`
--

CREATE TABLE IF NOT EXISTS `casestudies_stories` (
  `rlid` int(12) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `sub_title_short` varchar(150) DEFAULT NULL,
  `sub_title_long` varchar(250) DEFAULT NULL,
  `details` text NOT NULL,
  `short_reference` varchar(250) DEFAULT NULL,
  `imageurl` varchar(200) DEFAULT NULL,
  `insurance` int(11) NOT NULL,
  `ins_ids` varchar(200) NOT NULL,
  `tags` varchar(250) NOT NULL,
  `edate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`rlid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `casestudies_stories`
--

INSERT INTO `casestudies_stories` (`rlid`, `title`, `sub_title_short`, `sub_title_long`, `details`, `short_reference`, `imageurl`, `insurance`, `ins_ids`, `tags`, `edate`) VALUES
(6, 'Tom develops skin cancer', 'Trauma insurance payout of $120,000', 'Tom, a 42 year old self-employed tradesman, noticed a spot on his left leg that would not heal. His doctor ran some tests and informed Tom he had stage two skin cancer (basal cell carcinoma). Morbi tristique, mauris eu imperdiet bibendum, velit diam', '<div class="tab-content02">Fortunately, Mark had taken out permanent disability cover (TPD) for his  own occupation two years previously.  Mark lodged a successful TPD claim with his insurance company on the basis that he could no longer work within his own  occupation.</div>\r\n                                <div class="tab-content02">Mark is still working for the same company but has transitioned to a sales support role.  The insurance company paid Mark a lump sum of $2 million (the maximum policy amount) which compensated Mark for his loss of specialist income until age 65 (the policy expiry date).</div>\r\n                                <div class="tab-content02"><span>According to a data from the Australian Institute of Health and Welfare, bowel cancer rates amongst young people have increased dramatically since 1996. 2 </span></div>							\r\n                                <div class="tab-content02">Maecenas suscipit vestibulum nunc nec placerat. Phasellus blandit augue nunc, consequat consectetur augue placerat sed. Aenean fermentum scelerisque lectus, sit amet ultricies ex interdum bibendum. Maecenas suscipit vestibulum nunc nec placerat. Phasellus blandit augue nunc, consequat consectetur augue placerat sed. Aenean fermentum scelerisque lectus, sit amet ultricies ex interdum bibendum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libero venenatis sodales. Sed efficitur eget risus sed molestie.</div>\r\n                                <div class="tab-content02">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libero venenatis sodales. Sed efficitur eget risus sed molestie. Lorem tdolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libero venenatis sodales. Sed efficitur eget risus sed molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libeLorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libero venenatis sodales. Sed efficitur eget risus sed molestie.ro venenatis sodales. Sed efficitur eget risus sed molestie.</div>', '2. Bowl Caner Rates Amongst Australians, 2015. Australian Institute of Health and Welfare. EP. 234-4365.', '1517154152case_open_banner.jpg', 20, '5', 'INCOME,TRAUMA,INSURANCE TAG', '2018-02-02 18:30:00'),
(7, 'Sally copes with breast cancer', 'Trauma insurance payout of $105,000 (attached to life insurance policy)', 'Sally (41) was undergoing chemotherapy for breast cancer. She and her family were put under additional pressure when her self-employed husband lost his major business client.Morbi venenatis lacinia rhoncus. Pellentesque non bibendum tellus, vitae sem', '<div class="tab-content02">Fortunately, Mark had taken out permanent disability cover (TPD) for his own occupation two years previously. Mark lodged a successful TPD claim with his insurance company on the basis that he could no longer work within his own occupation.</div>\r\n\r\n<div class="tab-content02">Mark is still working for the same company but has transitioned to a sales support role. The insurance company paid Mark a lump sum of $2 million (the maximum policy amount) which compensated Mark for his loss of specialist income until age 65 (the policy expiry date).</div>\r\n\r\n<div class="tab-content02">According to a data from the Australian Institute of Health and Welfare, bowel cancer rates amongst young people have increased dramatically since 1996. 2</div>\r\n\r\n<div class="tab-content02">Maecenas suscipit vestibulum nunc nec placerat. Phasellus blandit augue nunc, consequat consectetur augue placerat sed. Aenean fermentum scelerisque lectus, sit amet ultricies ex interdum bibendum. Maecenas suscipit vestibulum nunc nec placerat. Phasellus blandit augue nunc, consequat consectetur augue placerat sed. Aenean fermentum scelerisque lectus, sit amet ultricies ex interdum bibendum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libero venenatis sodales. Sed efficitur eget risus sed molestie.</div>\r\n\r\n<div class="tab-content02">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libero venenatis sodales. Sed efficitur eget risus sed molestie. Lorem tdolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libero venenatis sodales. Sed efficitur eget risus sed molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libeLorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libero venenatis sodales. Sed efficitur eget risus sed molestie.ro venenatis sodales. Sed efficitur eget risus sed molestie.</div>', 'dfdgsdsd', '1517653182case01.jpg', 20, '2,4', 'TRAUMA,Life', '2018-02-02 18:30:00'),
(8, 'Mary had a stroke', 'Trauma insurance : $20K lump sum     Income protection : 75% of previous income', 'Mary (37) suffered a stroke as a result of a blood clot becoming lodged in a vein inside her brain. In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, In varius varius ju', '<div class="tab-content02">Fortunately, Mark had taken out permanent disability cover (TPD) for his own occupation two years previously. Mark lodged a successful TPD claim with his insurance company on the basis that he could no longer work within his own occupation.</div>\r\n\r\n<div class="tab-content02">Mark is still working for the same company but has transitioned to a sales support role. The insurance company paid Mark a lump sum of $2 million (the maximum policy amount) which compensated Mark for his loss of specialist income until age 65 (the policy expiry date).</div>\r\n\r\n<div class="tab-content02">According to a data from the Australian Institute of Health and Welfare, bowel cancer rates amongst young people have increased dramatically since 1996. 2</div>\r\n\r\n<div class="tab-content02">Maecenas suscipit vestibulum nunc nec placerat. Phasellus blandit augue nunc, consequat consectetur augue placerat sed. Aenean fermentum scelerisque lectus, sit amet ultricies ex interdum bibendum. Maecenas suscipit vestibulum nunc nec placerat. Phasellus blandit augue nunc, consequat consectetur augue placerat sed. Aenean fermentum scelerisque lectus, sit amet ultricies ex interdum bibendum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libero venenatis sodales. Sed efficitur eget risus sed molestie.</div>\r\n\r\n<div class="tab-content02">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libero venenatis sodales. Sed efficitur eget risus sed molestie. Lorem tdolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libero venenatis sodales. Sed efficitur eget risus sed molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libeLorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libero venenatis sodales. Sed efficitur eget risus sed molestie.ro venenatis sodales. Sed efficitur eget risus sed molestie.</div>', 'jhkhkhkhkhkjh', '1517653777case_mobile.png', 20, '2', 'INCOME,TRAUMA,INSURANCE TAG', '2018-02-02 18:30:00'),
(9, 'Tom develops skin cancer', 'Trauma insurance payout of $120,000', 'Tom, a 42 year old self-employed tradesman, noticed a spot on his left leg that would not heal. His doctor ran some tests and informed Tom he had stage two skin cancer (basal cell carcinoma). Morbi tristique, mauris eu imperdiet bibendum, velit diam', '<div class="tab-content02">Fortunately, Mark had taken out permanent disability cover (TPD) for his own occupation two years previously. Mark lodged a successful TPD claim with his insurance company on the basis that he could no longer work within his own occupation.</div>\r\n\r\n<div class="tab-content02">Mark is still working for the same company but has transitioned to a sales support role. The insurance company paid Mark a lump sum of $2 million (the maximum policy amount) which compensated Mark for his loss of specialist income until age 65 (the policy expiry date).</div>\r\n\r\n<div class="tab-content02">According to a data from the Australian Institute of Health and Welfare, bowel cancer rates amongst young people have increased dramatically since 1996. 2</div>\r\n\r\n<div class="tab-content02">Maecenas suscipit vestibulum nunc nec placerat. Phasellus blandit augue nunc, consequat consectetur augue placerat sed. Aenean fermentum scelerisque lectus, sit amet ultricies ex interdum bibendum. Maecenas suscipit vestibulum nunc nec placerat. Phasellus blandit augue nunc, consequat consectetur augue placerat sed. Aenean fermentum scelerisque lectus, sit amet ultricies ex interdum bibendum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libero venenatis sodales. Sed efficitur eget risus sed molestie.</div>\r\n\r\n<div class="tab-content02">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libero venenatis sodales. Sed efficitur eget risus sed molestie. Lorem tdolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libero venenatis sodales. Sed efficitur eget risus sed molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libeLorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libero venenatis sodales. Sed efficitur eget risus sed molestie.ro venenatis sodales. Sed efficitur eget risus sed molestie.</div>', 'gdgd', '1517658080Icome_Help_08.png', 20, '6', 'INCOME,TRAUMA,INSURANCE TAG', '2018-02-02 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE IF NOT EXISTS `contactus` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `email` varchar(250) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `best_time` varchar(100) NOT NULL,
  `insurance` varchar(100) NOT NULL,
  `subject` varchar(250) NOT NULL,
  `query` text NOT NULL,
  `ip_address` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `contactus`
--

INSERT INTO `contactus` (`id`, `name`, `email`, `phone`, `best_time`, `insurance`, `subject`, `query`, `ip_address`, `created_at`) VALUES
(1, 'harbhajan', 'harbhajansingh.mall@gmail.com', '454545454545', '2:30pm', 'LIFE INSURANCE', 'Test', 'Testing', '::1', '2018-02-13 13:33:55'),
(2, 'harbhajan', 'harbhajansingh.mall@gmail.com', '454545454545', '2:30pm', 'LIFE INSURANCE', 'Test', 'dfsfgsdf', '::1', '2018-02-13 13:39:24'),
(3, 'dfdfddf', 'jhjkhkh@vvv.com', '454545454545', '3:50pm', 'LIFE INSURANCE', 'Test', 'xzcvxzvv', '::1', '2018-02-13 13:50:14');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE IF NOT EXISTS `faqs` (
  `faqid` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(200) NOT NULL,
  `answer` text NOT NULL,
  `insurance` int(11) NOT NULL,
  `Ins_Ids` varchar(100) NOT NULL,
  `fstatus` bit(1) NOT NULL,
  PRIMARY KEY (`faqid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`faqid`, `question`, `answer`, `insurance`, `Ins_Ids`, `fstatus`) VALUES
(1, 'Why choose Renni?', '<h2>Insurance made easier.</h2>', 0, '', b'1'),
(2, 'Why do I need personal insurance and how does it work?', '<div class="doi_need">You are your most valuable asset, so it makes sense to insure yourself. Personal insurance lets you insure yourself against unforeseen life events that significantly impact your ability to earn an income and provide for your family.</div>\r\n\r\n<div class="you_pay">You pay a monthly or annual premium (just as you do with car or home contents insurance). On making a successful personal insurance claim, the insurance company pays you the stipulated policy amount. Depending on the type of policy, you will be paid either a lump sum or a monthly income.</div>\r\n\r\n<div class="personal_ins">Personal insurance provides you and your family with a source of revenue during what are often extremely stressful emotional and financial times. There are four key forms of personal insurance that include: life, total and permanent disability (TPD), trauma, and income protection.</div>\r\n\r\n<div class="How_can">\r\n<h3>How can I get great value from my personal insurance cover?</h3>\r\n\r\n<div class="faq_ul">\r\n<ul>\r\n	<li>Apply young (when you&rsquo;re likely to be healthier). Once your cover is in place it&#39;s guaranteed by your insurer to be renewable, regardless of any future health issues you face. When you apply later you run the risk of being unhealthier, which may result in you paying a higher premium or accepting policy exclusions regarding serious health issues that you may have or are at risk of acquiring.</li>\r\n	<li>Apply young (when you&rsquo;re likely to be healthier). Once your cover is in place it&#39;s guaranLook to see if you have any cover within your superannuation fund. Depending on the type and amount of cover you have, you may only need to top up your personal insurance cover. You&#39;ll also save money because premiums are paid out of your pre-tax income</li>\r\n	<li>Claim your premiums as a tax deduction. Some insurance premiums are tax deductible (depending on your circumstances).</li>\r\n	<li>Don&rsquo;t pay for cover you do not need. You should be adequately insured but not over insured (which will see you paying a higher premium than necessary).</li>\r\n	<li>Carefully review the Product Disclosure Statement (PDS) of any policies you are interested in. A PDS fully describes: the product, what it covers, benefits, conditions, and any limitations.</li>\r\n</ul>\r\n</div>\r\n</div>\r\n\r\n<div class="How_can">\r\n<h3>How can Renni help me get more value out of personal insurance cover?</h3>\r\n\r\n<div class="you_pay">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s\r\n<p><img alt="" src="http://localhost/renee_bs/assets/upload/images/faq_img01.jpg" /></p>\r\n</div>\r\n\r\n<h3>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text</h3>\r\n\r\n<div class="you_pay">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</div>\r\n\r\n<div class="personal_ins">It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</div>\r\n</div>', 0, '', b'1'),
(3, 'Question 3', '<p>Apply young (when you&rsquo;re likely to be healthier). Once your cover is in place it&#39;s guaranteed by your insurer to be renewable, regardless of any future health issues you face. When you apply later you run the risk of being unhealthier. This may result in you paying a higher premium or accepting policy exclusions regarding serious health issues that you may have or are at risk of acquiring.</p>', 20, '2', b'1'),
(4, 'What is life cover buy back and do I need it?', '<p>You are your most valuable asset. Personal insurance lets you insure yourself against unforeseen life events that significantly impact your ability to earn an income and provide for your family. You pay a monthly or annual premium (just as you do with car or home contents insurance). On making a successful personal insurance claim the insurance company pays you the stipulated policy amount. Depending on the type of policy you will be paid either a lump sum or a monthly income.</p>', 20, '2', b'1'),
(5, 'Why life cover?', '<p>Life insurance is probably the most valuable piece of insurance you will ever purchase if you have dependants &ndash; this is a statistical fact. A report &lsquo;Picking up the Pieces&rsquo; from ING (2010) claimed that within two years following the death of a parent, one in three families were forced to move home due to financial pressure.</p>', 20, '2', b'1'),
(6, 'What is trauma reinstatement and why do I need it?', '<p>Trauma reinstatement lets you reinstate your trauma benefit amount if you make a successful claim, without requiring further underwriting. Without the trauma reinstatement option, an insurance provider would cancel your policy after full payment of the first claim. You usually have the option to include trauma reinstatement when initially purchasing trauma cover.</p>', 20, '6', b'1'),
(7, 'How do I know how much I will receive for a successful?', '<p>You are your most valuable asset. Personal insurance lets you insure yourself against unforeseen life events that significantly impact your ability to earn an income and provide for your family.</p>', 20, '6', b'1'),
(8, 'Question 3?', '<p>Apply young (when you&rsquo;re likely</p>', 20, '6', b'1'),
(9, 'What is a permanent disability?', '<p>A permanent disability (when we are talking about insurance) can be described as a cognitive, physiological, or psychological impairment that reduces your ability to function for the foreseeable future. Your impairment can be anything that prevents you from working within your field of expertise or at all (depending on the type of cover you choose).</p>', 20, '4', b'1'),
(10, 'How do insurance companies decide if my disability...?', '<p>You are your most valuable asset...</p>', 20, '4', b'1'),
(11, 'What are the different types of disability insurance?', '<p>Apply young (when you&rsquo;re likely...</p>', 20, '4', b'1'),
(12, 'How long can I cover myself with income protection?', '<p>Income protection usually provides cover for a range of predetermined time increments. Some common options are: 1 year, 2 years, 5 years, until age 65, or until age 70.</p>', 20, '5', b'1'),
(13, 'How can I reduce my insurance premium?', '<p>You are your most valuable asset. Personal insurance lets you insure yourself against unforeseen life events that significantly impact your ability to earn an income and provide for your family. You pay a monthly or annual premium (just as you do with car or home contents insurance). On making a successful personal insurance claim the insurance company pays you the stipulated policy amount. Depending on the type of policy you will be paid either a lump sum or a monthly income.</p>', 20, '5', b'1'),
(14, 'Should I choose Agreed Value or Indemnity Cover?', '<p>Apply young (when you&rsquo;re likely to be healthier). Once your cover is in place it&#39;s guaranteed by your insurer to be renewable, regardless of any future health issues you face. When you apply later you run the risk of being unhealthier. This may result in you paying a higher premium or accepting policy exclusions regarding serious health issues that you may have or are at risk of acquiring.</p>', 20, '5', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `faq_insurance_mapping`
--

CREATE TABLE IF NOT EXISTS `faq_insurance_mapping` (
  `faqid` int(11) NOT NULL,
  `Ins_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq_insurance_mapping`
--

INSERT INTO `faq_insurance_mapping` (`faqid`, `Ins_ID`) VALUES
(43, 1),
(43, 17),
(43, 27),
(2, 1),
(35, 1),
(35, 17),
(35, 26);

-- --------------------------------------------------------

--
-- Table structure for table `imgdata`
--

CREATE TABLE IF NOT EXISTS `imgdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imgtitle` varchar(100) NOT NULL,
  `imgpath` varchar(50) NOT NULL,
  `infoid` int(11) NOT NULL,
  `thumbnailpath` varchar(70) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `imgdata`
--

INSERT INTO `imgdata` (`id`, `imgtitle`, `imgpath`, `infoid`, `thumbnailpath`) VALUES
(12, '', 'images/main2f11b73e26b2589c97a22e34103b339f.png', 18, 'images/thumbnails/main2f11b73e26b2589c97a22e34103b339f_thumb.png'),
(14, '', 'images/main3891e4a30d587485b3b3db289256295f.jpg', 19, 'images/thumbnails/main3891e4a30d587485b3b3db289256295f_thumb.jpg'),
(15, '', 'images/main5e86eeb79575b8df218bf1ba96d45c44.png', 17, 'images/thumbnails/main5e86eeb79575b8df218bf1ba96d45c44_thumb.png'),
(16, '', 'images/mainbac3227efb25be1d4660eb85542d1478.jpg', 18, 'images/thumbnails/mainbac3227efb25be1d4660eb85542d1478_thumb.jpg'),
(17, '', 'images/main93d99c25932f8cd375ef43e3c33eb709.jpg', 17, 'images/thumbnails/main93d99c25932f8cd375ef43e3c33eb709_thumb.jpg'),
(18, 'sdfsf', 'images/mainbf6fc34a7b29542ccff284f6b813422e.png', 21, 'images/thumbnails/mainbf6fc34a7b29542ccff284f6b813422e_thumb.png'),
(19, 'sds', 'images/main68d60293d54682c3cea79d2016cb6a78.JPG', 17, 'images/thumbnails/main68d60293d54682c3cea79d2016cb6a78_thumb.JPG'),
(20, 'ss', 'images/main52db1125c653f4a9a5c8810d21cdc6c8.JPG', 17, 'images/thumbnails/main52db1125c653f4a9a5c8810d21cdc6c8_thumb.JPG'),
(21, 'dsd', 'images/main72f5cc0990af4afc493009c9474c7a2a.JPG', 17, 'images/thumbnails/main72f5cc0990af4afc493009c9474c7a2a_thumb.JPG'),
(22, 'dsd', 'images/main70df29f9bc44cd24a2bc4d85e06c7137.JPG', 17, 'images/thumbnails/main70df29f9bc44cd24a2bc4d85e06c7137_thumb.JPG'),
(23, 'sfsd', 'images/main730ab88cd0744e4339ce41e39445ff96.JPG', 17, 'images/thumbnails/main730ab88cd0744e4339ce41e39445ff96_thumb.JPG'),
(24, 'sdfsf', 'images/maina3621e4615755fe27470db1a2589538b.JPG', 17, 'images/thumbnails/maina3621e4615755fe27470db1a2589538b_thumb.JPG'),
(25, 'sdfsf', 'images/mainf8170bc7161fea0569fc6f2d9c58d608.JPG', 17, 'images/thumbnails/mainf8170bc7161fea0569fc6f2d9c58d608_thumb.JPG'),
(26, 'sdfs', 'images/mainfd73733550874313a5b82401ebc17bb3.JPG', 17, 'images/thumbnails/mainfd73733550874313a5b82401ebc17bb3_thumb.JPG'),
(27, 'sdfs', 'images/main676fe800cbe3958e5230e1eac7998b9c.JPG', 17, 'images/thumbnails/main676fe800cbe3958e5230e1eac7998b9c_thumb.JPG'),
(28, 'sss', 'images/maine679fe607b9cf51dddd6b46817f1f298.JPG', 17, 'images/thumbnails/maine679fe607b9cf51dddd6b46817f1f298_thumb.JPG'),
(29, 'sdfds', 'images/main8b85dc0183a8432e8cb4f97a331c7573.JPG', 20, 'images/thumbnails/main8b85dc0183a8432e8cb4f97a331c7573_thumb.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `infodata`
--

CREATE TABLE IF NOT EXISTS `infodata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `gender` int(11) NOT NULL COMMENT '0=Male; 1=Female',
  `qualid` varchar(11) NOT NULL,
  `profid` int(11) NOT NULL,
  `rem` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `infodata`
--

INSERT INTO `infodata` (`id`, `fname`, `lname`, `gender`, `qualid`, `profid`, `rem`) VALUES
(17, 'Harnish', 'Kaur', 1, '1,2', 2, 'Testing I'),
(19, 'Amandeep', 'Kaur', 1, '1,2', 1, 'Testing III Editing'),
(20, 'sdf', 'ssss', 0, '1,2', 1, 'sdf'),
(21, 'sdf', 'ssss', 0, '1,2', 1, 'sdf'),
(22, 's', 'Sdf', 0, '1', 1, 'dsf');

-- --------------------------------------------------------

--
-- Table structure for table `insurances`
--

CREATE TABLE IF NOT EXISTS `insurances` (
  `ins_id` int(11) NOT NULL AUTO_INCREMENT,
  `ins_name` varchar(50) NOT NULL,
  `ins_subtitle` varchar(250) NOT NULL,
  `image_url` varchar(100) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ins_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `insurances`
--

INSERT INTO `insurances` (`ins_id`, `ins_name`, `ins_subtitle`, `image_url`, `parent_id`) VALUES
(2, 'Life Insurance', 'Provides your loved ones with a lump sum payment should you be diagnosed with a terminal illness or die.', '', 20),
(4, 'Disability Insurance', 'Provides you with a lump sum payment should you become permanently or total disabled and cannot work.', '', 20),
(5, 'Income Protection', 'Provides you with a replacement monthly income should you be temporarily unable to work due to illness or injury.', '', 20),
(6, 'Trauma Insurance', 'Provides you with a lump sum payment should you suffer a serious physical or emotional trauma.', '', 20),
(20, 'Personal Insurance', 'Personal Insurance', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `insurance_keypoints`
--

CREATE TABLE IF NOT EXISTS `insurance_keypoints` (
  `ikid` int(11) NOT NULL,
  `title` varchar(55) NOT NULL,
  `details` text NOT NULL,
  `kpstatus` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `news_blogs`
--

CREATE TABLE IF NOT EXISTS `news_blogs` (
  `nb_id` int(12) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `author_id` varchar(55) DEFAULT NULL,
  `nb_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `news_reference` varchar(200) DEFAULT NULL,
  `details` text NOT NULL,
  `tags` varchar(250) DEFAULT NULL,
  `insurance` int(11) NOT NULL,
  `ins_ids` varchar(150) NOT NULL,
  `image_url` varchar(250) DEFAULT NULL,
  `featured_image` varchar(250) DEFAULT NULL,
  `redirection_url` varchar(250) DEFAULT NULL,
  `is_featured` tinyint(1) NOT NULL,
  `type` varchar(50) NOT NULL DEFAULT 'blog',
  PRIMARY KEY (`nb_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `news_blogs`
--

INSERT INTO `news_blogs` (`nb_id`, `title`, `author_id`, `nb_date`, `news_reference`, `details`, `tags`, `insurance`, `ins_ids`, `image_url`, `featured_image`, `redirection_url`, `is_featured`, `type`) VALUES
(4, 'No mobrbis mauris eu bibendum veit iacultis lorem', 'aaaa', '2018-02-03 00:00:00', 'In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, in ornare massa enim at lorem. Etiam risus diam, porttitor vitae', '<p>In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, in ornare massa enim at lorem. Etiam risus diam, porttitor vitae</p>\r\n\r\n<p>In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, in ornare massa enim at lorem. Etiam risus diam, porttitor vitae</p>\r\n\r\n<p>In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, in ornare massa enim at lorem. Etiam risus diam, porttitor vitae</p>', 'jkjk,gfgffgf', 20, '6', NULL, '15166198011512907268Jellyfish.jpg', 'http://google.com', 1, 'news'),
(6, 'Eget ultricies mauris rhonucus no mobrbis mauris eu bibendum veit iacultis lorem', 'makaka jkk', '2018-02-03 00:00:00', 'Caring for a disabled child takes its toll mentally and financially on family and carers. The out-of pocket costs associated in raising a child far out way any support that the government currently of', '<p>Caring for a disabled child takes its toll mentally and financially on family and carers. The out-of pocket costs associated in raising a child far out way any support that the government currently offers.</p>\r\n\r\n<p>Caring for a disabled child takes its toll mentally and financially on family and carers. The out-of pocket costs associated in raising a child far out way any support that the government currently offers.</p>\r\n\r\n<p>Caring for a disabled child takes its toll mentally and financially on family and carers. The out-of pocket costs associated in raising a child far out way any support that the government currently offers.</p>', 'tag,new', 20, '4', '15166111091512907227Desert.jpg', NULL, 'http://google.com', 1, 'news'),
(7, 'dsgds fff', 'aaaa', '2018-01-25 00:00:00', 'dsfsd', '<p><img alt="" src="http://localhost/renee_bs/assets/upload/images/mypic.jpg" style="height:80px; width:60px" />safsa</p>', 'sadas', 20, '5,30', '15166113481512907268Jellyfish.jpg', '15166113491512907315Chrysanthemum.jpg', '', 1, 'blog'),
(8, 'dfffdf', 'hhhhh', '2018-01-22 00:00:00', 'sadaa', 'wffdfsfsf', 'sadasad', 20, '6', '15166179961513182187Koala.jpg', '15166179961512907268Jellyfish.jpg', NULL, 1, 'blog'),
(9, 'This article title has is has three lines, three lines will be the max limit', 'Aman', '2018-02-03 00:00:00', 'In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, in ornare massa enim at lorem. Etiam risus diam, porttitor vitae', '<p>In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, in ornare massa enim at lorem. Etiam risus diam, porttitor vitae</p>\r\n\r\n<p>In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, in ornare massa enim at lorem. Etiam risus diam, porttitor vitae</p>\r\n\r\n<p>In varius varius justo, eget ultrices mauris rhoncus non. Morbi tristique, mauris eu imperdiet bibendum, velit diam iaculis velit, in ornare massa enim at lorem. Etiam risus diam, porttitor vitae</p>', 'fggffd,ggfg', 20, '5', NULL, NULL, 'http://google.com', 0, 'news'),
(10, 'It costs a lot to care for disbaled children', 'dd', '2018-02-03 00:00:00', 'sddfdsCaring for a disabled child takes its toll mentally and financially on family and carers.  The out-of pocket costs associated in raising a child far out way any support that the government curre', '<p>Caring for a disabled child takes its toll mentally and financially on family and carers. &nbsp;The out-of pocket costs associated in raising a child far out way any support that the government currently offers.</p>\r\n\r\n<p>Caring for a disabled child takes its toll mentally and financially on family and carers. &nbsp;The out-of pocket costs associated in raising a child far out way any support that the government currently offers.</p>\r\n\r\n<p>Caring for a disabled child takes its toll mentally and financially on family and carers. &nbsp;The out-of pocket costs associated in raising a child far out way any support that the government currently offers.</p>', 'dffdd,dfddd,dfdfd,dffdf', 20, '2', NULL, NULL, 'http://google.com', 1, 'news'),
(11, 'asasA', 'dd', '2018-01-28 00:00:00', 'Q', '<p>EQWEQE</p>', 'KHKHKHKH,JHHK', 0, '', NULL, NULL, NULL, 0, 'blog');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `email` varchar(250) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `question` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_address` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `name`, `email`, `phone`, `question`, `created_at`, `ip_address`) VALUES
(1, 'harbhajan', '', '9434344344', 'dfdsfdsff df dfd dsfdf dfdsf', '2018-02-08 16:55:10', '::1'),
(3, 'harbhajan', 'harbhajansingh.mall@gmail.com', '9434344344', 'ewttewtewt', '2018-02-09 16:44:20', '::1'),
(4, 'harbhajan', 'harbhajansingh.mall@gmail.com', '9434344344', 'dgfddfgfgdgdgdg fg fgfgfg g', '2018-02-09 16:44:43', '::1'),
(5, 'harbhajan', 'harbhajansingh.mall@gmail.com', 'd54545', 'fffffffffffffff', '2018-02-09 17:01:39', '::1'),
(6, 'dddg', 'jhjkhkh@vvv.com', '9434344344', 'afsffsa', '2018-02-09 17:04:51', '::1'),
(7, 'fgfgf', 'jhhjhjh@dfdd.com', 'jhjhjhjh', 'hhhhhhhhhhhhhh', '2018-02-09 17:38:01', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `subscribe`
--

CREATE TABLE IF NOT EXISTS `subscribe` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `email` varchar(250) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip_address` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `subscribe`
--

INSERT INTO `subscribe` (`id`, `email`, `created_at`, `ip_address`) VALUES
(1, 'harbhajan@dss.com', '2018-02-08 16:12:52', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `user_logins`
--

CREATE TABLE IF NOT EXISTS `user_logins` (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(30) NOT NULL,
  `Passwords` varchar(32) NOT NULL,
  `Firstname` varchar(25) NOT NULL,
  `Lastname` varchar(25) NOT NULL,
  `EmailID` varchar(35) NOT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_logins`
--

INSERT INTO `user_logins` (`mid`, `Username`, `Passwords`, `Firstname`, `Lastname`, `EmailID`) VALUES
(1, 'sukh', 'f899139df5e1059396431415e770c6dd', 'Sukhchain', 'Singh', 'sukh@gmail.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
