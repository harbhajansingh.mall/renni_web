-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2018 at 07:10 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `renee_web`
--

-- --------------------------------------------------------

--
-- Table structure for table `admindet`
--

CREATE TABLE IF NOT EXISTS `admindet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usrnm` varchar(100) NOT NULL,
  `pswd` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admindet`
--

INSERT INTO `admindet` (`id`, `usrnm`, `pswd`) VALUES
(1, 'admin', '100');

-- --------------------------------------------------------

--
-- Table structure for table `author_master`
--

CREATE TABLE IF NOT EXISTS `author_master` (
  `author_id` int(11) NOT NULL,
  `author_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `author_master`
--

INSERT INTO `author_master` (`author_id`, `author_name`) VALUES
(1, 'aaaa'),
(2, 'makaka jkk'),
(3, 'Aman'),
(4, 'dd');

-- --------------------------------------------------------

--
-- Table structure for table `casestudies_stories`
--

CREATE TABLE IF NOT EXISTS `casestudies_stories` (
  `rlid` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `sub_title_short` varchar(150) DEFAULT NULL,
  `sub_title_long` varchar(250) DEFAULT NULL,
  `details` text NOT NULL,
  `short_reference` varchar(250) DEFAULT NULL,
  `imageurl` varchar(200) DEFAULT NULL,
  `insurance` int(11) NOT NULL,
  `ins_ids` varchar(200) NOT NULL,
  `tags` varchar(250) NOT NULL,
  `edate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `casestudies_stories`
--

INSERT INTO `casestudies_stories` (`rlid`, `title`, `sub_title_short`, `sub_title_long`, `details`, `short_reference`, `imageurl`, `insurance`, `ins_ids`, `tags`, `edate`) VALUES
(1, 'nn', 'nn', 'nn', '', 'nn', NULL, 27, 'ggg,gggg', 'nn', '2017-12-05 18:30:00'),
(0, 'Title', 'Sub title short', 'sub title long', '<p>details</p>\r\n', 'short reference', NULL, 1, '2,4', 'tags', '2018-01-04 18:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE IF NOT EXISTS `faqs` (
  `faqid` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(200) NOT NULL,
  `answer` text NOT NULL,
  `insurance` int(11) NOT NULL,
  `Ins_Ids` varchar(100) NOT NULL,
  `fstatus` bit(1) NOT NULL,
  PRIMARY KEY (`faqid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`faqid`, `question`, `answer`, `insurance`, `Ins_Ids`, `fstatus`) VALUES
(2, 'new questions', '<p>sdfds</p>', 1, '6', b'1'),
(5, 'checking', '<p>sdfs df</p>', 18, '2,4', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `faq_insurance_mapping`
--

CREATE TABLE IF NOT EXISTS `faq_insurance_mapping` (
  `faqid` int(11) NOT NULL,
  `Ins_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq_insurance_mapping`
--

INSERT INTO `faq_insurance_mapping` (`faqid`, `Ins_ID`) VALUES
(43, 1),
(43, 17),
(43, 27),
(2, 1),
(35, 1),
(35, 17),
(35, 26);

-- --------------------------------------------------------

--
-- Table structure for table `imgdata`
--

CREATE TABLE IF NOT EXISTS `imgdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imgtitle` varchar(100) NOT NULL,
  `imgpath` varchar(50) NOT NULL,
  `infoid` int(11) NOT NULL,
  `thumbnailpath` varchar(70) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `imgdata`
--

INSERT INTO `imgdata` (`id`, `imgtitle`, `imgpath`, `infoid`, `thumbnailpath`) VALUES
(12, '', 'images/main2f11b73e26b2589c97a22e34103b339f.png', 18, 'images/thumbnails/main2f11b73e26b2589c97a22e34103b339f_thumb.png'),
(14, '', 'images/main3891e4a30d587485b3b3db289256295f.jpg', 19, 'images/thumbnails/main3891e4a30d587485b3b3db289256295f_thumb.jpg'),
(15, '', 'images/main5e86eeb79575b8df218bf1ba96d45c44.png', 17, 'images/thumbnails/main5e86eeb79575b8df218bf1ba96d45c44_thumb.png'),
(16, '', 'images/mainbac3227efb25be1d4660eb85542d1478.jpg', 18, 'images/thumbnails/mainbac3227efb25be1d4660eb85542d1478_thumb.jpg'),
(17, '', 'images/main93d99c25932f8cd375ef43e3c33eb709.jpg', 17, 'images/thumbnails/main93d99c25932f8cd375ef43e3c33eb709_thumb.jpg'),
(18, 'sdfsf', 'images/mainbf6fc34a7b29542ccff284f6b813422e.png', 21, 'images/thumbnails/mainbf6fc34a7b29542ccff284f6b813422e_thumb.png'),
(19, 'sds', 'images/main68d60293d54682c3cea79d2016cb6a78.JPG', 17, 'images/thumbnails/main68d60293d54682c3cea79d2016cb6a78_thumb.JPG'),
(20, 'ss', 'images/main52db1125c653f4a9a5c8810d21cdc6c8.JPG', 17, 'images/thumbnails/main52db1125c653f4a9a5c8810d21cdc6c8_thumb.JPG'),
(21, 'dsd', 'images/main72f5cc0990af4afc493009c9474c7a2a.JPG', 17, 'images/thumbnails/main72f5cc0990af4afc493009c9474c7a2a_thumb.JPG'),
(22, 'dsd', 'images/main70df29f9bc44cd24a2bc4d85e06c7137.JPG', 17, 'images/thumbnails/main70df29f9bc44cd24a2bc4d85e06c7137_thumb.JPG'),
(23, 'sfsd', 'images/main730ab88cd0744e4339ce41e39445ff96.JPG', 17, 'images/thumbnails/main730ab88cd0744e4339ce41e39445ff96_thumb.JPG'),
(24, 'sdfsf', 'images/maina3621e4615755fe27470db1a2589538b.JPG', 17, 'images/thumbnails/maina3621e4615755fe27470db1a2589538b_thumb.JPG'),
(25, 'sdfsf', 'images/mainf8170bc7161fea0569fc6f2d9c58d608.JPG', 17, 'images/thumbnails/mainf8170bc7161fea0569fc6f2d9c58d608_thumb.JPG'),
(26, 'sdfs', 'images/mainfd73733550874313a5b82401ebc17bb3.JPG', 17, 'images/thumbnails/mainfd73733550874313a5b82401ebc17bb3_thumb.JPG'),
(27, 'sdfs', 'images/main676fe800cbe3958e5230e1eac7998b9c.JPG', 17, 'images/thumbnails/main676fe800cbe3958e5230e1eac7998b9c_thumb.JPG'),
(28, 'sss', 'images/maine679fe607b9cf51dddd6b46817f1f298.JPG', 17, 'images/thumbnails/maine679fe607b9cf51dddd6b46817f1f298_thumb.JPG'),
(29, 'sdfds', 'images/main8b85dc0183a8432e8cb4f97a331c7573.JPG', 20, 'images/thumbnails/main8b85dc0183a8432e8cb4f97a331c7573_thumb.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `infodata`
--

CREATE TABLE IF NOT EXISTS `infodata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `gender` int(11) NOT NULL COMMENT '0=Male; 1=Female',
  `qualid` varchar(11) NOT NULL,
  `profid` int(11) NOT NULL,
  `rem` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `infodata`
--

INSERT INTO `infodata` (`id`, `fname`, `lname`, `gender`, `qualid`, `profid`, `rem`) VALUES
(17, 'Harnish', 'Kaur', 1, '1,2', 2, 'Testing I'),
(19, 'Amandeep', 'Kaur', 1, '1,2', 1, 'Testing III Editing'),
(20, 'sdf', 'ssss', 0, '1,2', 1, 'sdf'),
(21, 'sdf', 'ssss', 0, '1,2', 1, 'sdf'),
(22, 's', 'Sdf', 0, '1', 1, 'dsf');

-- --------------------------------------------------------

--
-- Table structure for table `insurances`
--

CREATE TABLE IF NOT EXISTS `insurances` (
  `ins_id` int(11) NOT NULL AUTO_INCREMENT,
  `ins_name` varchar(50) NOT NULL,
  `ins_subtitle` varchar(250) NOT NULL,
  `image_url` varchar(100) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ins_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `insurances`
--

INSERT INTO `insurances` (`ins_id`, `ins_name`, `ins_subtitle`, `image_url`, `parent_id`) VALUES
(2, 'Life Insurance', 'Provides your loved ones with a lump sum payment should you be diagnosed with a terminal illness or die.', '', 20),
(4, 'Disability Insurance', 'Provides you with a lump sum payment should you become permanently or total disabled and cannot work.', '', 20),
(5, 'Income Protection', 'Provides you with a replacement monthly income should you be temporarily unable to work due to illness or injury.', '', 20),
(6, 'Trauma Insurance', 'Provides you with a lump sum payment should you suffer a serious physical or emotional trauma.', '', 20),
(19, 'ssddd', 'ddd', '87d48947b39d3c7574863f395864ad02.png', 18),
(20, 'Personal Insurance', 'Personal Insurance', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `insurance_keypoints`
--

CREATE TABLE IF NOT EXISTS `insurance_keypoints` (
  `ikid` int(11) NOT NULL,
  `title` varchar(55) NOT NULL,
  `details` text NOT NULL,
  `kpstatus` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `news_blogs`
--

CREATE TABLE IF NOT EXISTS `news_blogs` (
  `nb_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `author_id` varchar(55) DEFAULT NULL,
  `nb_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `news_reference` varchar(200) DEFAULT NULL,
  `details` text NOT NULL,
  `tags` varchar(250) DEFAULT NULL,
  `insurance` int(11) NOT NULL,
  `ins_ids` varchar(150) NOT NULL,
  `image_url` varchar(250) DEFAULT NULL,
  `featured_image` varchar(250) DEFAULT NULL,
  `redirection_url` varchar(250) DEFAULT NULL,
  `is_featured` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `polloption`
--

CREATE TABLE IF NOT EXISTS `polloption` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quest_id` int(11) NOT NULL,
  `opt_id` int(11) NOT NULL,
  `opt_val` varchar(200) NOT NULL,
  `postedon` date NOT NULL,
  `postedby` varchar(30) NOT NULL,
  `result` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `polloption`
--

INSERT INTO `polloption` (`id`, `quest_id`, `opt_id`, `opt_val`, `postedon`, `postedby`, `result`) VALUES
(1, 1, 1, '1', '2015-10-01', 'harnish', 0),
(2, 1, 2, '2', '2015-10-01', 'harnish', 0),
(3, 1, 3, '3', '2015-10-01', 'harnish', 0),
(4, 1, 4, '4', '2015-10-01', 'harnish', 0),
(5, 14, 1, '1', '2015-10-01', 'harnish', 0),
(6, 14, 2, '2', '2015-10-01', 'harnish', 0),
(7, 14, 3, '3', '2015-10-01', 'harnish', 0),
(8, 14, 4, '4', '2015-10-01', 'harnish', 0),
(9, 15, 1, 'sss', '2015-10-01', 'harnish', 2),
(10, 15, 2, 'aaaa', '2015-10-01', 'harnish', 0),
(11, 15, 3, 'dffff', '2015-10-01', 'harnish', 0),
(12, 15, 4, 'hjhjhjhj', '2015-10-01', 'harnish', 3);

-- --------------------------------------------------------

--
-- Table structure for table `pollquestion`
--

CREATE TABLE IF NOT EXISTS `pollquestion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(200) NOT NULL,
  `validtill` date NOT NULL,
  `postedon` date NOT NULL,
  `postedby` varchar(30) NOT NULL,
  `status` varchar(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `pollquestion`
--

INSERT INTO `pollquestion` (`id`, `question`, `validtill`, `postedon`, `postedby`, `status`) VALUES
(6, 'Popularity Stars', '1970-01-01', '2015-09-30', 'harnish', 'N'),
(7, 'Popularity Stars', '2015-10-05', '2015-09-30', 'harnish', 'N'),
(11, 'Music Rating', '2015-10-16', '2015-10-01', 'harnish', 'N'),
(13, 'Music Rating', '2015-10-17', '2015-10-01', 'harnish', 'N'),
(14, 'Music Rating', '2015-10-17', '2015-10-01', 'harnish', 'N'),
(15, 'Popularity Stars', '2015-10-31', '2015-10-01', 'harnish', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `profession`
--

CREATE TABLE IF NOT EXISTS `profession` (
  `profid` int(11) NOT NULL AUTO_INCREMENT,
  `profession` varchar(50) NOT NULL,
  PRIMARY KEY (`profid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `profession`
--

INSERT INTO `profession` (`profid`, `profession`) VALUES
(1, 'Administration'),
(2, 'Information Technology');

-- --------------------------------------------------------

--
-- Table structure for table `qualification`
--

CREATE TABLE IF NOT EXISTS `qualification` (
  `qualid` int(11) NOT NULL AUTO_INCREMENT,
  `course` varchar(10) NOT NULL,
  PRIMARY KEY (`qualid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `qualification`
--

INSERT INTO `qualification` (`qualid`, `course`) VALUES
(1, 'Bca'),
(2, 'Mca');

-- --------------------------------------------------------

--
-- Table structure for table `user_logins`
--

CREATE TABLE IF NOT EXISTS `user_logins` (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(30) NOT NULL,
  `Passwords` varchar(32) NOT NULL,
  `Firstname` varchar(25) NOT NULL,
  `Lastname` varchar(25) NOT NULL,
  `EmailID` varchar(35) NOT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_logins`
--

INSERT INTO `user_logins` (`mid`, `Username`, `Passwords`, `Firstname`, `Lastname`, `EmailID`) VALUES
(1, 'sukh', 'f899139df5e1059396431415e770c6dd', 'Sukhchain', 'Singh', 'sukh@gmail.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
